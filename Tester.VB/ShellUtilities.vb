'
 '* [File purpose]
 '* Author: Phillip Piper
 '* Date: 1 May 2007 7:44 PM
 '*
 '* CHANGE LOG:
 '* when who what
 '* 1 May 2007 JPP  Initial Version
 '


Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Namespace ObjectListViewDemo
	''' <summary>
	''' This helper class allows listviews and tree views to use image from the system image list.
	''' </summary>
	''' <remarks>Instances of this helper class know how to retrieve icon from the Windows shell for
	''' a given file path. These icons are then added to the imagelist on the given control. ListViews need 
	''' special handling since they have two image lists which need to be kept in sync.</remarks>
	Public Class SysImageListHelper
		Private Sub New()
		End Sub

		''' <summary>
		''' Create a SysImageListHelper that will fetch images for the given tree control
		''' </summary>
		''' <param name="treeView">The tree view that will use the images</param>
		Public Sub New(ByVal treeView As TreeView)
			If treeView.ImageList Is Nothing Then
				treeView.ImageList = New ImageList()
				treeView.ImageList.ImageSize = New Size(16, 16)
			End If
			Me.smallImageCollection = treeView.ImageList.Images
		End Sub

		''' <summary>
		''' Create a SysImageListHelper that will fetch images for the given listview control.
		''' </summary>
		''' <param name="listView">The listview that will use the images</param>
		''' <remarks>Listviews manage two image lists, but each item can only have one image index.
		''' This means that the image for an item must occur at the same index in the two lists. 
		''' SysImageListHelper instances handle this requirement. However, if the listview already
		''' has image lists installed, they <b>must</b> be of the same length.</remarks>
		Public Sub New(ByVal listView As ListView)
			If listView.SmallImageList Is Nothing Then
				listView.SmallImageList = New ImageList()
				listView.SmallImageList.ImageSize = New Size(16, 16)
			End If

			If listView.LargeImageList Is Nothing Then
				listView.LargeImageList = New ImageList()
				listView.LargeImageList.ImageSize = New Size(32, 32)
			End If

			'if (listView.SmallImageList.Images.Count != listView.LargeImageList.Images.Count)
			'    throw new ArgumentException("Small and large image lists must have the same number of items.");

			Me.smallImageCollection = listView.SmallImageList.Images
			Me.largeImageCollection = listView.LargeImageList.Images
		End Sub

		''' <summary>
		''' Return the index of the image that has the Shell Icon for the given file/directory.
		''' </summary>
		''' <param name="path">The full path to the file/directory</param>
		''' <returns>The index of the image or -1 if something goes wrong.</returns>
		Public Function GetImageIndex(ByVal path As String) As Integer
			If System.IO.Directory.Exists(path) Then
				path = System.Environment.SystemDirectory ' optimization! give all directories the same image
			Else
				If System.IO.Path.HasExtension(path) Then
					path = System.IO.Path.GetExtension(path)
				End If
			End If

			If Me.smallImageCollection.ContainsKey(path) Then
				Return Me.smallImageCollection.IndexOfKey(path)
			End If

			Try
				Me.smallImageCollection.Add(path, ShellUtilities.GetFileIcon(path, True, True))
				If Not Me.largeImageCollection Is Nothing Then
					Me.largeImageCollection.Add(path, ShellUtilities.GetFileIcon(path, False, True))
				End If
			Catch e1 As ArgumentNullException
				Return -1
			End Try

			Return Me.smallImageCollection.IndexOfKey(path)
		End Function

		Private smallImageCollection As ImageList.ImageCollection = Nothing
		Private largeImageCollection As ImageList.ImageCollection = Nothing
	End Class

	''' <summary>
	''' ShellUtilities contains routines to interact with the Windows Shell.
	''' </summary>
	Public Class ShellUtilities
		''' <summary>
		''' Execute the default verb on the file or directory identified by the given path.
		''' For documents, this will open them with their normal application. For executables,
		''' this will cause them to run.
		''' </summary>
		''' <param name="path">The file or directory to be executed</param>
		''' <returns>Values &lt; 31 indicate some sort of error. See ShellExecute() documentation for specifics.</returns>
		''' <remarks>The same effect can be achieved by <code>System.Diagnostics.Process.Start(path)</code>.</remarks>
		Private Sub New()
		End Sub
		Public Shared Function Execute(ByVal path As String) As Integer
			Return ShellUtilities.Execute(path, "")
		End Function

		''' <summary>
		''' Execute the given operation on the file or directory identified by the given path.
		''' Example operations are "edit", "print", "explore".
		''' </summary>
		''' <param name="path">The file or directory to be operated on</param>
		''' <param name="operation">What operation should be performed</param>
		''' <returns>Values &lt; 31 indicate some sort of error. See ShellExecute() documentation for specifics.</returns>
		Public Shared Function Execute(ByVal path As String, ByVal operation As String) As Integer
			Dim result As IntPtr = ShellUtilities.ShellExecute(0, operation, path, "", "", SW_SHOWNORMAL)
			Return result.ToInt32()
		End Function

		''' <summary>
		''' Get the string that describes the file's type.
		''' </summary>
		''' <param name="path">The file or directory whose type is to be fetched</param>
		''' <returns>A string describing the type of the file, or an empty string if something goes wrong.</returns>
		Public Shared Function GetFileType(ByVal path As String) As String
			Dim shfi As SHFILEINFO = New SHFILEINFO()
			Dim flags As Integer = SHGFI_TYPENAME
			Dim result As IntPtr = ShellUtilities.SHGetFileInfo(path, 0, shfi, Marshal.SizeOf(shfi), flags)
			If result.ToInt32() = 0 Then
				Return String.Empty
			Else
				Return shfi.szTypeName
			End If
		End Function

		''' <summary>
		''' Return the icon for the given file/directory.
		''' </summary>
		''' <param name="path">The full path to the file whose icon is to be returned</param>
		''' <param name="isSmallImage">True if the small (16x16) icon is required, otherwise the 32x32 icon will be returned</param>
		''' <param name="useFileType">If this is true, only the file extension will be considered</param>
		''' <returns>The icon of the given file, or null if something goes wrong</returns>
		Public Shared Function GetFileIcon(ByVal path As String, ByVal isSmallImage As Boolean, ByVal useFileType As Boolean) As Icon
			Dim flags As Integer = SHGFI_ICON
			If isSmallImage Then
				flags = flags Or SHGFI_SMALLICON
			End If

			Dim fileAttributes As Integer = 0
			If useFileType Then
				flags = flags Or SHGFI_USEFILEATTRIBUTES
				If System.IO.Directory.Exists(path) Then
					fileAttributes = FILE_ATTRIBUTE_DIRECTORY
				Else
					fileAttributes = FILE_ATTRIBUTE_NORMAL
				End If
			End If

			Dim shfi As SHFILEINFO = New SHFILEINFO()
			Dim result As IntPtr = ShellUtilities.SHGetFileInfo(path, fileAttributes, shfi, Marshal.SizeOf(shfi), flags)
			If result.ToInt32() = 0 Then
				Return Nothing
			Else
				Return Icon.FromHandle(shfi.hIcon)
			End If
		End Function

		''' <summary>
		''' Return the index into the system image list of the image that represents the given file.
		''' </summary>
		''' <param name="path">The full path to the file or directory whose icon is required</param>
		''' <returns>The index of the icon, or -1 if something goes wrong</returns>
		''' <remarks>This is only useful if you are using the system image lists directly. Since there is
		''' no way to do that in .NET, it isn't a very useful.</remarks>
		Public Shared Function GetSysImageIndex(ByVal path As String) As Integer
			Dim shfi As SHFILEINFO = New SHFILEINFO()
			Dim flags As Integer = SHGFI_ICON Or SHGFI_SYSICONINDEX
			Dim result As IntPtr = ShellUtilities.SHGetFileInfo(path, 0, shfi, Marshal.SizeOf(shfi), flags)
			If result.ToInt32() = 0 Then
				Return -1
			Else
				Return shfi.iIcon
			End If
		End Function

		#Region "Native methods"

		Private Const SHGFI_ICON As Integer = &H00100 ' get icon
		Private Const SHGFI_DISPLAYNAME As Integer = &H00200 ' get display name
		Private Const SHGFI_TYPENAME As Integer = &H00400 ' get type name
		Private Const SHGFI_ATTRIBUTES As Integer = &H00800 ' get attributes
		Private Const SHGFI_ICONLOCATION As Integer = &H01000 ' get icon location
		Private Const SHGFI_EXETYPE As Integer = &H02000 ' return exe type
		Private Const SHGFI_SYSICONINDEX As Integer = &H04000 ' get system icon index
		Private Const SHGFI_LINKOVERLAY As Integer = &H08000 ' put a link overlay on icon
		Private Const SHGFI_SELECTED As Integer = &H10000 ' show icon in selected state
		Private Const SHGFI_ATTR_SPECIFIED As Integer = &H20000 ' get only specified attributes
		Private Const SHGFI_LARGEICON As Integer = &H00000 ' get large icon
		Private Const SHGFI_SMALLICON As Integer = &H00001 ' get small icon
		Private Const SHGFI_OPENICON As Integer = &H00002 ' get open icon
		Private Const SHGFI_SHELLICONSIZE As Integer = &H00004 ' get shell size icon
		Private Const SHGFI_PIDL As Integer = &H00008 ' pszPath is a pidl
		Private Const SHGFI_USEFILEATTRIBUTES As Integer = &H00010 ' use passed dwFileAttribute
		'if (Me._WIN32_IE >= 0x0500)
		Private Const SHGFI_ADDOVERLAYS As Integer = &H00020 ' apply the appropriate overlays
		Private Const SHGFI_OVERLAYINDEX As Integer = &H00040 ' Get the index of the overlay

		Private Const FILE_ATTRIBUTE_NORMAL As Integer = &H00080 ' Normal file
		Private Const FILE_ATTRIBUTE_DIRECTORY As Integer = &H00010 ' Directory

		Private Const MAX_PATH As Integer = 260

		<StructLayout(LayoutKind.Sequential, CharSet := CharSet.Auto)> 
		Private Structure SHFILEINFO
			Public hIcon As IntPtr
			Public iIcon As Integer
			Public dwAttributes As Integer
			<MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH)> 
			Public szDisplayName As String
			<MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)> 
			Public szTypeName As String
		End Structure

		Private Const SW_SHOWNORMAL As Integer = 1

		<DllImport("shell32.dll", CharSet := CharSet.Auto)> 
		Private Shared Function ShellExecute(ByVal hwnd As Integer, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Integer) As IntPtr
		End Function

		<DllImport("shell32.dll", CharSet := CharSet.Auto)> 
		Private Shared Function SHGetFileInfo(ByVal pszPath As String, ByVal dwFileAttributes As Integer, <System.Runtime.InteropServices.Out()> ByRef psfi As SHFILEINFO, ByVal cbFileInfo As Integer, ByVal uFlags As Integer) As IntPtr
		End Function

		#End Region
	End Class
End Namespace
