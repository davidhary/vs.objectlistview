Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' Information about this assembly is defined by the following
' attributes.
'
' change them to the information which is associated with the assembly
' you compile.

<Assembly: AssemblyTitle("ObjectListViewDemo")>
<Assembly: AssemblyDescription("A simple demonstration of how easy an ObjectListView is to use.")>
<Assembly: AssemblyConfiguration("")>
<Assembly: AssemblyCompany("Bright Ideas Software")>
<Assembly: AssemblyProduct("ObjectListViewDemo")>
<Assembly: AssemblyCopyright("2006-2008 All Rights Reserved")>
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' This sets the default COM visibility of types in the assembly to invisible.
' If you need to expose a type to COM, use [ComVisible(true)] on that type.
<Assembly: ComVisible(False)>

' The assembly version has following format :
'
' Major.Minor.Build.Revision
'
' You can specify all values by your own or you can build default build and revision
' numbers with the '*' character (the default):

<Assembly: AssemblyVersion("1.11.*")>
' <Assembly: AssemblyFileVersion("1.11")>
<Assembly: System.CLSCompliant(True)>
