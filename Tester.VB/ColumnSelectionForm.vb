Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms

Imports BrightIdeasSoftware

Namespace ObjectListViewDemo
	''' <summary>
	''' This form is an example of how an application could allows the user to select which columns 
	''' an ObjectListView will display, as well as select which order the columns are displayed in.
	''' </summary>
	''' <remarks>
	''' <para>In Tile view, ColumnHeader.DisplayIndex does nothing. To reorder the columns you have
	''' to change the order of objects in the Columns property.</para>
	''' <para>Remember that the first column is special!
	''' It has to remain the first column.</para>
	''' </remarks>
	Public Partial Class ColumnSelectionForm
		Inherits Form
		''' <summary>
		''' Make a new ColumnSelectionForm
		''' </summary>
		Public Sub New()
			InitializeComponent()
		End Sub

		''' <summary>
		''' Open this form so it will edit the columns that are available in the listview's current view
		''' </summary>
		''' <param name="olv">The ObjectListView whose columns are to be altered</param>
		Public Sub OpenOn(ByVal olv As ObjectListView)
			Me.OpenOn(olv, olv.View)
		End Sub

		''' <summary>
		''' Open this form so it will edit the columns that are available in the given listview
		''' when the listview is showing the given type of view.
		''' </summary>
		''' <remarks>RearrangableColumns are only visible in Details and Tile views, so view must be one
		''' of those values.</remarks>
		''' <param name="olv">The ObjectListView whose columns are to be altered</param>
		''' <param name="view">The view that is to be altered. Must be View.Details or View.Tile</param>
		Public Sub OpenOn(ByVal olv As ObjectListView, ByVal view As View)
			If view <> View.Details AndAlso view <> View.Tile Then
				Return
			End If

			Me.InitializeForm(olv, view)
			If Me.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
				Me.ProcessOK(olv, view)
			End If
		End Sub

		''' <summary>
		''' Initialize the form to show the columns of the given view
		''' </summary>
		''' <param name="olv"></param>
		''' <param name="view"></param>
		Protected Sub InitializeForm(ByVal olv As ObjectListView, ByVal view As View)
			Me.AllColumns = olv.AllColumns
			Me.RearrangableColumns = New List(Of OLVColumn)(Me.AllColumns)
			For Each col As OLVColumn In Me.RearrangableColumns
				If view = View.Details Then
					Me.MapColumnToVisible(col) = col.IsVisible
				Else
					Me.MapColumnToVisible(col) = col.IsTileViewColumn
				End If
			Next col
			Me.RearrangableColumns.Sort(New SortByDisplayOrder(Me))

			Me.objectListView1.BooleanCheckStateGetter = AddressOf AnonymousMethod1

			Me.objectListView1.BooleanCheckStatePutter = AddressOf AnonymousMethod2

			Me.objectListView1.SetObjects(Me.RearrangableColumns)
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod1(ByVal rowObject As Object) As Boolean
      Return Me.MapColumnToVisible(CType(rowObject, OLVColumn))
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod2(ByVal rowObject As Object, ByVal newValue As Boolean) As Boolean
      If (Not Me.IsPrimaryColumn(CType(rowObject, OLVColumn))) Then
        Me.MapColumnToVisible(CType(rowObject, OLVColumn)) = newValue
        EnableControls()
      End If
      Return newValue
    End Function
		Private AllColumns As List(Of OLVColumn) = Nothing
		Private RearrangableColumns As List(Of OLVColumn) = New List(Of OLVColumn)()
		Private MapColumnToVisible As Dictionary(Of OLVColumn, Boolean) = New Dictionary(Of OLVColumn, Boolean)()

		''' <summary>
		''' The user has pressed OK. Do what's requied.
		''' </summary>
		''' <param name="olv"></param>
		''' <param name="view"></param>
		Protected Sub ProcessOK(ByVal olv As ObjectListView, ByVal view As View)
			olv.Freeze()

			' Update the column definitions to reflect whether they have been hidden
			For Each col As OLVColumn In olv.AllColumns
				If view = View.Details Then
					col.IsVisible = Me.MapColumnToVisible(col)
				Else
					col.IsTileViewColumn = Me.MapColumnToVisible(col)
				End If
			Next col

			' Collect the columns are still visible
			Dim visibleColumns As List(Of OLVColumn) = Me.RearrangableColumns.FindAll(AddressOf AnonymousMethod3)

			' Detail view and Tile view have to be handled in different ways.
			If view = View.Details Then
				' Of the still visible columns, change DisplayIndex to reflect their position in the rearranged list
				olv.ChangeToFilteredColumns(view)
				For Each col As ColumnHeader In olv.Columns
					col.DisplayIndex = visibleColumns.IndexOf(CType(col, OLVColumn))
				Next col
			Else
				' In Tile view, DisplayOrder does nothing. So to change the display order, we have to change the 
				' order of the columns in the Columns property.
				' Remember, the primary column is special and has to remain first!
				Dim primaryColumn As OLVColumn = Me.AllColumns(0)
				visibleColumns.Remove(primaryColumn)

				olv.Columns.Clear()
				olv.Columns.Add(primaryColumn)
				olv.Columns.AddRange(visibleColumns.ToArray())
				olv.CalculateReasonableTileSize()
			End If

			olv.Unfreeze()
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod3(ByVal x As OLVColumn) As Boolean
      Return Me.MapColumnToVisible(x)
    End Function

		#Region "Event handlers"

		Private Sub ButtonMoveUp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonMoveUp.Click
			Dim selectedIndex As Integer = Me.objectListView1.SelectedIndices(0)
			Dim col As OLVColumn = Me.RearrangableColumns(selectedIndex)
			Me.RearrangableColumns.RemoveAt(selectedIndex)
			Me.RearrangableColumns.Insert(selectedIndex-1, col)

			Me.objectListView1.BuildList()

			EnableControls()
		End Sub

		Private Sub ButtonMoveDown_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonMoveDown.Click
			Dim selectedIndex As Integer = Me.objectListView1.SelectedIndices(0)
			Dim col As OLVColumn = Me.RearrangableColumns(selectedIndex)
			Me.RearrangableColumns.RemoveAt(selectedIndex)
			Me.RearrangableColumns.Insert(selectedIndex + 1, col)

			Me.objectListView1.BuildList()

			EnableControls()
		End Sub

		Private Sub ButtonShow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonShow.Click
			Me.objectListView1.SelectedItem.Checked = True
		End Sub

		Private Sub ButtonHide_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonHide.Click
			Me.objectListView1.SelectedItem.Checked = False
		End Sub

		Private Sub ButtonOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonOK.Click
			Me.DialogResult = System.Windows.Forms.DialogResult.OK
			Me.Close()
		End Sub

		Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonCancel.Click
			Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
			Me.Close()
		End Sub

		Private Sub ObjectListView1_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles objectListView1.SelectionChanged
			EnableControls()
		End Sub

		#End Region

		#Region "Control enabling"

		Private Function IsPrimaryColumn(ByVal col As OLVColumn) As Boolean
			Return (col Is Me.AllColumns(0))
		End Function

		''' <summary>
		''' Enable the controls on the dialog to match the current state
		''' </summary>
		Protected Sub EnableControls()
			If Me.objectListView1.SelectedIndices.Count = 0 Then
				Me.buttonMoveUp.Enabled = False
				Me.buttonMoveDown.Enabled = False
				Me.buttonShow.Enabled = False
				Me.buttonHide.Enabled = False
			Else
				' Can't move the first row up or the last row down
				Me.buttonMoveUp.Enabled = (Me.objectListView1.SelectedIndices(0) <> 0)
				Me.buttonMoveDown.Enabled = (Me.objectListView1.SelectedIndices(0) < (Me.objectListView1.GetItemCount() - 1))

				Dim selectedColumn As OLVColumn = CType(Me.objectListView1.SelectedObject, OLVColumn)

				' The primary column cannot be hidden (and hence cannot be Shown)
				Me.buttonShow.Enabled = (Not Me.MapColumnToVisible(selectedColumn)) AndAlso Not Me.IsPrimaryColumn(selectedColumn)
				Me.buttonHide.Enabled = Me.MapColumnToVisible(selectedColumn) AndAlso Not Me.IsPrimaryColumn(selectedColumn)
			End If
		End Sub
		#End Region

		''' <summary>
		''' A Comparer that will sort a list of columns so that visible ones come before hidden ones,
		''' and that are ordered by their display order.
		''' </summary>
		Private Class SortByDisplayOrder
			Implements IComparer(Of OLVColumn)
			Public Sub New(ByVal form As ColumnSelectionForm)
				Me.Form = form
			End Sub
			Private Form As ColumnSelectionForm

			#Region "IComparer<OLVColumn> Members"

			Private Function IComparer_Compare(ByVal x As OLVColumn, ByVal y As OLVColumn) As Integer Implements IComparer(Of OLVColumn).Compare
				If Me.Form.MapColumnToVisible(x) AndAlso (Not Me.Form.MapColumnToVisible(y)) Then
					Return -1
				End If

				If (Not Me.Form.MapColumnToVisible(x)) AndAlso Me.Form.MapColumnToVisible(y) Then
					Return 1
				End If

				If x.DisplayIndex = y.DisplayIndex Then
					Return x.Text.CompareTo(y.Text)
				Else
					Return x.DisplayIndex - y.DisplayIndex
				End If
			End Function

			#End Region
		End Class
	End Class
End Namespace
