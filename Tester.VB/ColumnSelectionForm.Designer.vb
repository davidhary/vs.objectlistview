Imports Microsoft.VisualBasic
Imports System
Namespace ObjectListViewDemo
	Public Partial Class ColumnSelectionForm
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (Not components Is Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.buttonMoveUp = New System.Windows.Forms.Button()
			Me.buttonMoveDown = New System.Windows.Forms.Button()
			Me.buttonShow = New System.Windows.Forms.Button()
			Me.buttonHide = New System.Windows.Forms.Button()
			Me.label1 = New System.Windows.Forms.Label()
			Me.buttonOK = New System.Windows.Forms.Button()
			Me.buttonCancel = New System.Windows.Forms.Button()
			Me.objectListView1 = New BrightIdeasSoftware.ObjectListView()
			Me.olvColumn1 = New BrightIdeasSoftware.OLVColumn()
			CType(Me.objectListView1, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' buttonMoveUp
			' 
			Me.buttonMoveUp.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.buttonMoveUp.Location = New System.Drawing.Point(295, 31)
			Me.buttonMoveUp.Name = "buttonMoveUp"
			Me.buttonMoveUp.Size = New System.Drawing.Size(87, 23)
			Me.buttonMoveUp.TabIndex = 1
			Me.buttonMoveUp.Text = "Move &Up"
			Me.buttonMoveUp.UseVisualStyleBackColor = True
'			Me.buttonMoveUp.Click += New System.EventHandler(Me.buttonMoveUp_Click);
			' 
			' buttonMoveDown
			' 
			Me.buttonMoveDown.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.buttonMoveDown.Location = New System.Drawing.Point(295, 60)
			Me.buttonMoveDown.Name = "buttonMoveDown"
			Me.buttonMoveDown.Size = New System.Drawing.Size(87, 23)
			Me.buttonMoveDown.TabIndex = 2
			Me.buttonMoveDown.Text = "Move &Down"
			Me.buttonMoveDown.UseVisualStyleBackColor = True
'			Me.buttonMoveDown.Click += New System.EventHandler(Me.buttonMoveDown_Click);
			' 
			' buttonShow
			' 
			Me.buttonShow.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.buttonShow.Location = New System.Drawing.Point(295, 89)
			Me.buttonShow.Name = "buttonShow"
			Me.buttonShow.Size = New System.Drawing.Size(87, 23)
			Me.buttonShow.TabIndex = 3
			Me.buttonShow.Text = "&Show"
			Me.buttonShow.UseVisualStyleBackColor = True
'			Me.buttonShow.Click += New System.EventHandler(Me.buttonShow_Click);
			' 
			' buttonHide
			' 
			Me.buttonHide.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.buttonHide.Location = New System.Drawing.Point(295, 118)
			Me.buttonHide.Name = "buttonHide"
			Me.buttonHide.Size = New System.Drawing.Size(87, 23)
			Me.buttonHide.TabIndex = 4
			Me.buttonHide.Text = "&Hide"
			Me.buttonHide.UseVisualStyleBackColor = True
'			Me.buttonHide.Click += New System.EventHandler(Me.buttonHide_Click);
			' 
			' label1
			' 
			Me.label1.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.label1.BackColor = System.Drawing.SystemColors.Control
			Me.label1.Location = New System.Drawing.Point(13, 9)
			Me.label1.Name = "label1"
			Me.label1.Size = New System.Drawing.Size(366, 19)
			Me.label1.TabIndex = 5
			Me.label1.Text = "Choose the columns you want to see in this list. "
			' 
			' buttonOK
			' 
			Me.buttonOK.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.buttonOK.Location = New System.Drawing.Point(198, 304)
			Me.buttonOK.Name = "buttonOK"
			Me.buttonOK.Size = New System.Drawing.Size(87, 23)
			Me.buttonOK.TabIndex = 6
			Me.buttonOK.Text = "&OK"
			Me.buttonOK.UseVisualStyleBackColor = True
'			Me.buttonOK.Click += New System.EventHandler(Me.buttonOK_Click);
			' 
			' buttonCancel
			' 
			Me.buttonCancel.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
			Me.buttonCancel.Location = New System.Drawing.Point(295, 304)
			Me.buttonCancel.Name = "buttonCancel"
			Me.buttonCancel.Size = New System.Drawing.Size(87, 23)
			Me.buttonCancel.TabIndex = 7
			Me.buttonCancel.Text = "&Cancel"
			Me.buttonCancel.UseVisualStyleBackColor = True
'			Me.buttonCancel.Click += New System.EventHandler(Me.buttonCancel_Click);
			' 
			' objectListView1
			' 
			Me.objectListView1.AllColumns.Add(Me.olvColumn1)
			Me.objectListView1.AlternateRowBackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(192))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(192))))))
			Me.objectListView1.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.objectListView1.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClick
			Me.objectListView1.CheckBoxes = True
			Me.objectListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() { Me.olvColumn1})
			Me.objectListView1.FullRowSelect = True
			Me.objectListView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
			Me.objectListView1.HideSelection = False
			Me.objectListView1.Location = New System.Drawing.Point(12, 31)
			Me.objectListView1.MultiSelect = False
			Me.objectListView1.Name = "objectListView1"
			Me.objectListView1.ShowGroups = False
			Me.objectListView1.ShowSortIndicators = False
			Me.objectListView1.Size = New System.Drawing.Size(273, 259)
			Me.objectListView1.TabIndex = 0
			Me.objectListView1.UseCompatibleStateImageBehavior = False
			Me.objectListView1.View = System.Windows.Forms.View.Details
'			Me.objectListView1.SelectionChanged += New System.EventHandler(Me.objectListView1_SelectionChanged);
			' 
			' olvColumn1
			' 
			Me.olvColumn1.AspectName = "Text"
			Me.olvColumn1.IsVisible = True
			Me.olvColumn1.Text = "Column"
			Me.olvColumn1.Width = 267
			' 
			' ColumnSelectionForm
			' 
			Me.AcceptButton = Me.buttonOK
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.CancelButton = Me.buttonCancel
			Me.ClientSize = New System.Drawing.Size(391, 339)
			Me.Controls.Add(Me.buttonCancel)
			Me.Controls.Add(Me.buttonOK)
			Me.Controls.Add(Me.label1)
			Me.Controls.Add(Me.buttonHide)
			Me.Controls.Add(Me.buttonShow)
			Me.Controls.Add(Me.buttonMoveDown)
			Me.Controls.Add(Me.buttonMoveUp)
			Me.Controls.Add(Me.objectListView1)
			Me.MaximizeBox = False
			Me.MinimizeBox = False
			Me.Name = "ColumnSelectionForm"
			Me.ShowIcon = False
			Me.ShowInTaskbar = False
			Me.Text = "Column Selection"
			CType(Me.objectListView1, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private WithEvents ObjectListView1 As BrightIdeasSoftware.ObjectListView
		Private WithEvents ButtonMoveUp As System.Windows.Forms.Button
		Private WithEvents ButtonMoveDown As System.Windows.Forms.Button
		Private WithEvents ButtonShow As System.Windows.Forms.Button
		Private WithEvents ButtonHide As System.Windows.Forms.Button
		Private olvColumn1 As BrightIdeasSoftware.OLVColumn
		Private label1 As System.Windows.Forms.Label
		Private WithEvents ButtonOK As System.Windows.Forms.Button
		Private WithEvents ButtonCancel As System.Windows.Forms.Button
	End Class
End Namespace