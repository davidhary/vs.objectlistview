'
 '* Created by SharpDevelop.
 '* User: Viney
 '* Date: 9/10/2006
 '* Time: 2:14 PM
 '*
 '* To change this template use Tools | Options | Coding | Edit Standard Headers.
 '


Imports Microsoft.VisualBasic
Imports System
Imports BrightIdeasSoftware
Imports System.Windows.Forms
Imports System.Windows.Forms.Design

Namespace ObjectListViewDemo
  Partial Public Class MainForm
    Inherits System.Windows.Forms.Form
    ''' <summary>
    ''' Designer variable used to keep track of non-visual components.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Disposes resources used by the form.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
      If disposing Then
        If Not components Is Nothing Then
          components.Dispose()
        End If
      End If
      MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' This method is required for Windows Forms designer support.
    ''' Do not change the method contents inside the source code editor. The Forms designer might
    ''' not be able to load this method if it was changed manually.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:DoNotPassLiteralsAsLocalizedParameters", MessageId:="System.Windows.Forms.Control.set_Text(System.String)")> 
    Private Sub InitializeComponent()
      Me.components = New System.ComponentModel.Container()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
      Me.imageList1 = New System.Windows.Forms.ImageList(Me.components)
      Me.tabControl1 = New System.Windows.Forms.TabControl()
      Me.tabPage1 = New System.Windows.Forms.TabPage()
      Me.checkBox18 = New System.Windows.Forms.CheckBox()
      Me.checkBox15 = New System.Windows.Forms.CheckBox()
      Me.comboBox6 = New System.Windows.Forms.ComboBox()
      Me.label21 = New System.Windows.Forms.Label()
      Me.button7 = New System.Windows.Forms.Button()
      Me.button6 = New System.Windows.Forms.Button()
      Me.button4 = New System.Windows.Forms.Button()
      Me.button1 = New System.Windows.Forms.Button()
      Me.checkBox4 = New System.Windows.Forms.CheckBox()
      Me.checkBox3 = New System.Windows.Forms.CheckBox()
      Me.label1 = New System.Windows.Forms.Label()
      Me.listViewSimple = New BrightIdeasSoftware.ObjectListView()
      Me.columnHeader11 = New BrightIdeasSoftware.OLVColumn()
      Me.columnHeader12 = New BrightIdeasSoftware.OLVColumn()
      Me.columnHeader13 = New BrightIdeasSoftware.OLVColumn()
      Me.columnHeader14 = New BrightIdeasSoftware.OLVColumn()
      Me.columnHeader15 = New BrightIdeasSoftware.OLVColumn()
      Me.columnHeader16 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn34 = New BrightIdeasSoftware.OLVColumn()
      Me.contextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
      Me.command1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
      Me.command2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
      Me.command3ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
      Me.appearOnTheColumnHeadersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
      Me.hotItemStyle1 = New BrightIdeasSoftware.HotItemStyle()
      Me.tabPage2 = New System.Windows.Forms.TabPage()
      Me.button16 = New System.Windows.Forms.Button()
      Me.button17 = New System.Windows.Forms.Button()
      Me.comboBox5 = New System.Windows.Forms.ComboBox()
      Me.label20 = New System.Windows.Forms.Label()
      Me.label6 = New System.Windows.Forms.Label()
      Me.comboBox1 = New System.Windows.Forms.ComboBox()
      Me.checkBox6 = New System.Windows.Forms.CheckBox()
      Me.button5 = New System.Windows.Forms.Button()
      Me.button2 = New System.Windows.Forms.Button()
      Me.label2 = New System.Windows.Forms.Label()
      Me.checkBox2 = New System.Windows.Forms.CheckBox()
      Me.checkBox1 = New System.Windows.Forms.CheckBox()
      Me.listViewComplex = New BrightIdeasSoftware.ObjectListView()
      Me.personColumn = New BrightIdeasSoftware.OLVColumn()
      Me.occupationColumn = New BrightIdeasSoftware.OLVColumn()
      Me.columnCookingSkill = New BrightIdeasSoftware.OLVColumn()
      Me.cookingSkillRenderer = New BrightIdeasSoftware.MultiImageRenderer()
      Me.yearOfBirthColumn = New BrightIdeasSoftware.OLVColumn()
      Me.birthdayColumn = New BrightIdeasSoftware.OLVColumn()
      Me.hourlyRateColumn = New BrightIdeasSoftware.OLVColumn()
      Me.moneyImageColumn = New BrightIdeasSoftware.OLVColumn()
      Me.daysSinceBirthColumn = New BrightIdeasSoftware.OLVColumn()
      Me.olvJokeColumn = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn41 = New BrightIdeasSoftware.OLVColumn()
      Me.imageList2 = New System.Windows.Forms.ImageList(Me.components)
      Me.tabPage3 = New System.Windows.Forms.TabPage()
      Me.groupBox3 = New System.Windows.Forms.GroupBox()
      Me.label22 = New System.Windows.Forms.Label()
      Me.comboBox7 = New System.Windows.Forms.ComboBox()
      Me.checkBoxPause = New System.Windows.Forms.CheckBox()
      Me.rowHeightUpDown = New System.Windows.Forms.NumericUpDown()
      Me.label11 = New System.Windows.Forms.Label()
      Me.label8 = New System.Windows.Forms.Label()
      Me.comboBox3 = New System.Windows.Forms.ComboBox()
      Me.checkBox5 = New System.Windows.Forms.CheckBox()
      Me.listViewDataSet = New BrightIdeasSoftware.DataListView()
      Me.olvColumn1 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn2 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn3 = New BrightIdeasSoftware.OLVColumn()
      Me.salaryColumn = New BrightIdeasSoftware.OLVColumn()
      Me.salaryRenderer = New BrightIdeasSoftware.MultiImageRenderer()
      Me.heightColumn = New BrightIdeasSoftware.OLVColumn()
      Me.heightRenderer = New BrightIdeasSoftware.BarRenderer()
      Me.olvColumn42 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumnGif = New BrightIdeasSoftware.OLVColumn()
      Me.imageRenderer1 = New BrightIdeasSoftware.ImageRenderer()
      Me.checkBox7 = New System.Windows.Forms.CheckBox()
      Me.checkBox8 = New System.Windows.Forms.CheckBox()
      Me.groupBox1 = New System.Windows.Forms.GroupBox()
      Me.dataGridView1 = New System.Windows.Forms.DataGridView()
      Me.button3 = New System.Windows.Forms.Button()
      Me.label3 = New System.Windows.Forms.Label()
      Me.tabPage4 = New System.Windows.Forms.TabPage()
      Me.comboBox8 = New System.Windows.Forms.ComboBox()
      Me.label23 = New System.Windows.Forms.Label()
      Me.button9 = New System.Windows.Forms.Button()
      Me.button8 = New System.Windows.Forms.Button()
      Me.label7 = New System.Windows.Forms.Label()
      Me.comboBox2 = New System.Windows.Forms.ComboBox()
      Me.checkBox9 = New System.Windows.Forms.CheckBox()
      Me.label4 = New System.Windows.Forms.Label()
      Me.listViewVirtual = New BrightIdeasSoftware.VirtualObjectListView()
      Me.olvColumn4 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn12 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn5 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn7 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn8 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn9 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn10 = New BrightIdeasSoftware.OLVColumn()
      Me.tabPage5 = New System.Windows.Forms.TabPage()
      Me.buttonSaveState = New System.Windows.Forms.Button()
      Me.buttonRestoreState = New System.Windows.Forms.Button()
      Me.button13 = New System.Windows.Forms.Button()
      Me.buttonUp = New System.Windows.Forms.Button()
      Me.buttonGo = New System.Windows.Forms.Button()
      Me.textBoxFolderPath = New System.Windows.Forms.TextBox()
      Me.label10 = New System.Windows.Forms.Label()
      Me.label9 = New System.Windows.Forms.Label()
      Me.comboBox4 = New System.Windows.Forms.ComboBox()
      Me.checkBox10 = New System.Windows.Forms.CheckBox()
      Me.checkBox11 = New System.Windows.Forms.CheckBox()
      Me.checkBox12 = New System.Windows.Forms.CheckBox()
      Me.label5 = New System.Windows.Forms.Label()
      Me.listViewFiles = New BrightIdeasSoftware.ObjectListView()
      Me.olvColumnFileName = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumnFileCreated = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumnFileModified = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumnSize = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumnFileType = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumnAttributes = New BrightIdeasSoftware.OLVColumn()
      Me.treeColumnFileExtension = New BrightIdeasSoftware.OLVColumn()
      Me.tabPage6 = New System.Windows.Forms.TabPage()
      Me.groupBox5 = New System.Windows.Forms.GroupBox()
      Me.numericUpDown2 = New System.Windows.Forms.NumericUpDown()
      Me.label19 = New System.Windows.Forms.Label()
      Me.numericUpDown1 = New System.Windows.Forms.NumericUpDown()
      Me.label18 = New System.Windows.Forms.Label()
      Me.cbCellGridLines = New System.Windows.Forms.CheckBox()
      Me.label17 = New System.Windows.Forms.Label()
      Me.rbStyleTooMuch = New System.Windows.Forms.RadioButton()
      Me.cbPrintOnlySelection = New System.Windows.Forms.CheckBox()
      Me.rbStyleModern = New System.Windows.Forms.RadioButton()
      Me.cbShrinkToFit = New System.Windows.Forms.CheckBox()
      Me.rbStyleMinimal = New System.Windows.Forms.RadioButton()
      Me.cbIncludeImages = New System.Windows.Forms.CheckBox()
      Me.groupBox4 = New System.Windows.Forms.GroupBox()
      Me.tbFooter = New System.Windows.Forms.TextBox()
      Me.label15 = New System.Windows.Forms.Label()
      Me.tbHeader = New System.Windows.Forms.TextBox()
      Me.label16 = New System.Windows.Forms.Label()
      Me.tbWatermark = New System.Windows.Forms.TextBox()
      Me.label14 = New System.Windows.Forms.Label()
      Me.tbTitle = New System.Windows.Forms.TextBox()
      Me.label13 = New System.Windows.Forms.Label()
      Me.groupBox2 = New System.Windows.Forms.GroupBox()
      Me.rbShowVirtual = New System.Windows.Forms.RadioButton()
      Me.rbShowFileExplorer = New System.Windows.Forms.RadioButton()
      Me.rbShowDataset = New System.Windows.Forms.RadioButton()
      Me.rbShowComplex = New System.Windows.Forms.RadioButton()
      Me.rbShowSimple = New System.Windows.Forms.RadioButton()
      Me.label12 = New System.Windows.Forms.Label()
      Me.button12 = New System.Windows.Forms.Button()
      Me.button11 = New System.Windows.Forms.Button()
      Me.button10 = New System.Windows.Forms.Button()
      Me.printPreviewControl1 = New System.Windows.Forms.PrintPreviewControl()
      Me.listViewPrinter1 = New BrightIdeasSoftware.ListViewPrinter()
      Me.tabPage7 = New System.Windows.Forms.TabPage()
      Me.button19 = New System.Windows.Forms.Button()
      Me.button18 = New System.Windows.Forms.Button()
      Me.label26 = New System.Windows.Forms.Label()
      Me.comboBox9 = New System.Windows.Forms.ComboBox()
      Me.label24 = New System.Windows.Forms.Label()
      Me.label25 = New System.Windows.Forms.Label()
      Me.comboBox10 = New System.Windows.Forms.ComboBox()
      Me.checkBox13 = New System.Windows.Forms.CheckBox()
      Me.button15 = New System.Windows.Forms.Button()
      Me.button14 = New System.Windows.Forms.Button()
      Me.olvFastList = New BrightIdeasSoftware.FastObjectListView()
      Me.olvColumn18 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn19 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn26 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn27 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn28 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn29 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn31 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn32 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn33 = New BrightIdeasSoftware.OLVColumn()
      Me.tabPage9 = New System.Windows.Forms.TabPage()
      Me.button25 = New System.Windows.Forms.Button()
      Me.button26 = New System.Windows.Forms.Button()
      Me.button27 = New System.Windows.Forms.Button()
      Me.label32 = New System.Windows.Forms.Label()
      Me.treeListView = New BrightIdeasSoftware.TreeListView()
      Me.treeColumnName = New BrightIdeasSoftware.OLVColumn()
      Me.treeColumnCreated = New BrightIdeasSoftware.OLVColumn()
      Me.treeColumnModified = New BrightIdeasSoftware.OLVColumn()
      Me.treeColumnSize = New BrightIdeasSoftware.OLVColumn()
      Me.treeColumnFileType = New BrightIdeasSoftware.OLVColumn()
      Me.treeColumnAttributes = New BrightIdeasSoftware.OLVColumn()
      Me.statusStrip1 = New System.Windows.Forms.StatusStrip()
      Me.toolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
      Me.button20 = New System.Windows.Forms.Button()
      Me.button21 = New System.Windows.Forms.Button()
      Me.button22 = New System.Windows.Forms.Button()
      Me.button23 = New System.Windows.Forms.Button()
      Me.button24 = New System.Windows.Forms.Button()
      Me.textBox1 = New System.Windows.Forms.TextBox()
      Me.label27 = New System.Windows.Forms.Label()
      Me.label28 = New System.Windows.Forms.Label()
      Me.comboBox11 = New System.Windows.Forms.ComboBox()
      Me.checkBox14 = New System.Windows.Forms.CheckBox()
      Me.checkBox16 = New System.Windows.Forms.CheckBox()
      Me.checkBox17 = New System.Windows.Forms.CheckBox()
      Me.label29 = New System.Windows.Forms.Label()
      Me.olvColumn21 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn22 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn23 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn30 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn24 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn25 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn20 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn17 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn13 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn14 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn15 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn6 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn11 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn16 = New BrightIdeasSoftware.OLVColumn()
      Me.objectListView1 = New BrightIdeasSoftware.ObjectListView()
      Me.olvColumn35 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn36 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn37 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn38 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn39 = New BrightIdeasSoftware.OLVColumn()
      Me.olvColumn40 = New BrightIdeasSoftware.OLVColumn()
      Me.blockFormat1 = New BrightIdeasSoftware.BlockFormat()
      Me.tabControl1.SuspendLayout()
      Me.tabPage1.SuspendLayout()
      CType(Me.listViewSimple, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.contextMenuStrip1.SuspendLayout()
      Me.tabPage2.SuspendLayout()
      CType(Me.listViewComplex, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.tabPage3.SuspendLayout()
      Me.groupBox3.SuspendLayout()
      CType(Me.rowHeightUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.listViewDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.groupBox1.SuspendLayout()
      CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.tabPage4.SuspendLayout()
      CType(Me.listViewVirtual, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.tabPage5.SuspendLayout()
      CType(Me.listViewFiles, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.tabPage6.SuspendLayout()
      Me.groupBox5.SuspendLayout()
      CType(Me.numericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.numericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.groupBox4.SuspendLayout()
      Me.groupBox2.SuspendLayout()
      Me.tabPage7.SuspendLayout()
      CType(Me.olvFastList, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.tabPage9.SuspendLayout()
      CType(Me.treeListView, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.statusStrip1.SuspendLayout()
      CType(Me.objectListView1, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.SuspendLayout()
      ' 
      ' imageList1
      ' 
      Me.imageList1.ImageStream = (CType(resources.GetObject("imageList1.ImageStream"), System.Windows.Forms.ImageListStreamer))
      Me.imageList1.TransparentColor = System.Drawing.Color.Transparent
      Me.imageList1.Images.SetKeyName(0, "compass")
      Me.imageList1.Images.SetKeyName(1, "down")
      Me.imageList1.Images.SetKeyName(2, "user")
      Me.imageList1.Images.SetKeyName(3, "find")
      Me.imageList1.Images.SetKeyName(4, "folder")
      Me.imageList1.Images.SetKeyName(5, "movie")
      Me.imageList1.Images.SetKeyName(6, "music")
      Me.imageList1.Images.SetKeyName(7, "no")
      Me.imageList1.Images.SetKeyName(8, "readonly")
      Me.imageList1.Images.SetKeyName(9, "public")
      Me.imageList1.Images.SetKeyName(10, "recycle")
      Me.imageList1.Images.SetKeyName(11, "spanner")
      Me.imageList1.Images.SetKeyName(12, "star")
      Me.imageList1.Images.SetKeyName(13, "tick")
      Me.imageList1.Images.SetKeyName(14, "archive")
      Me.imageList1.Images.SetKeyName(15, "system")
      Me.imageList1.Images.SetKeyName(16, "hidden")
      Me.imageList1.Images.SetKeyName(17, "temporary")
      ' 
      ' tabControl1
      ' 
      Me.tabControl1.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.tabControl1.Controls.Add(Me.tabPage1)
      Me.tabControl1.Controls.Add(Me.tabPage2)
      Me.tabControl1.Controls.Add(Me.tabPage3)
      Me.tabControl1.Controls.Add(Me.tabPage4)
      Me.tabControl1.Controls.Add(Me.tabPage5)
      Me.tabControl1.Controls.Add(Me.tabPage6)
      Me.tabControl1.Controls.Add(Me.tabPage7)
      Me.tabControl1.Controls.Add(Me.tabPage9)
      Me.tabControl1.Location = New System.Drawing.Point(13, 12)
      Me.tabControl1.Name = "tabControl1"
      Me.tabControl1.SelectedIndex = 0
      Me.tabControl1.Size = New System.Drawing.Size(794, 511)
      Me.tabControl1.TabIndex = 0
      '			Me.tabControl1.Selected += New System.Windows.Forms.TabControlEventHandler(Me.tabControl1_Selected);
      ' 
      ' tabPage1
      ' 
      Me.tabPage1.Controls.Add(Me.checkBox18)
      Me.tabPage1.Controls.Add(Me.checkBox15)
      Me.tabPage1.Controls.Add(Me.comboBox6)
      Me.tabPage1.Controls.Add(Me.label21)
      Me.tabPage1.Controls.Add(Me.button7)
      Me.tabPage1.Controls.Add(Me.button6)
      Me.tabPage1.Controls.Add(Me.button4)
      Me.tabPage1.Controls.Add(Me.button1)
      Me.tabPage1.Controls.Add(Me.checkBox4)
      Me.tabPage1.Controls.Add(Me.checkBox3)
      Me.tabPage1.Controls.Add(Me.label1)
      Me.tabPage1.Controls.Add(Me.listViewSimple)
      Me.tabPage1.Location = New System.Drawing.Point(4, 22)
      Me.tabPage1.Name = "tabPage1"
      Me.tabPage1.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage1.Size = New System.Drawing.Size(786, 485)
      Me.tabPage1.TabIndex = 0
      Me.tabPage1.Text = "Simple Example"
      Me.tabPage1.UseVisualStyleBackColor = True
      ' 
      ' checkBox18
      ' 
      Me.checkBox18.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox18.Checked = True
      Me.checkBox18.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBox18.Location = New System.Drawing.Point(260, 455)
      Me.checkBox18.Name = "checkBox18"
      Me.checkBox18.Size = New System.Drawing.Size(95, 24)
      Me.checkBox18.TabIndex = 10
      Me.checkBox18.Text = "Hot &Item"
      Me.checkBox18.UseVisualStyleBackColor = True
      '			Me.checkBox18.CheckedChanged += New System.EventHandler(Me.checkBox18_CheckedChanged);
      ' 
      ' checkBox15
      ' 
      Me.checkBox15.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox15.Location = New System.Drawing.Point(177, 455)
      Me.checkBox15.Name = "checkBox15"
      Me.checkBox15.Size = New System.Drawing.Size(95, 24)
      Me.checkBox15.TabIndex = 9
      Me.checkBox15.Text = "&Lock Group"
      Me.checkBox15.UseVisualStyleBackColor = True
      '			Me.checkBox15.CheckedChanged += New System.EventHandler(Me.checkBox15_CheckedChanged);
      ' 
      ' comboBox6
      ' 
      Me.comboBox6.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox6.FormattingEnabled = True
      Me.comboBox6.Items.AddRange(New Object() {"No", "Single Click", "Double Click", "F2 Only"})
      Me.comboBox6.Location = New System.Drawing.Point(407, 455)
      Me.comboBox6.Name = "comboBox6"
      Me.comboBox6.Size = New System.Drawing.Size(83, 21)
      Me.comboBox6.TabIndex = 4
      '			Me.comboBox6.SelectedIndexChanged += New System.EventHandler(Me.comboBox6_SelectedIndexChanged);
      ' 
      ' label21
      ' 
      Me.label21.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label21.AutoSize = True
      Me.label21.Location = New System.Drawing.Point(353, 460)
      Me.label21.Name = "label21"
      Me.label21.Size = New System.Drawing.Size(48, 13)
      Me.label21.TabIndex = 3
      Me.label21.Text = "&Editable:"
      ' 
      ' button7
      ' 
      Me.button7.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button7.Location = New System.Drawing.Point(496, 455)
      Me.button7.Name = "button7"
      Me.button7.Size = New System.Drawing.Size(46, 23)
      Me.button7.TabIndex = 5
      Me.button7.Text = "&Add"
      Me.button7.UseVisualStyleBackColor = True
      '			Me.button7.Click += New System.EventHandler(Me.button7_Click);
      ' 
      ' button6
      ' 
      Me.button6.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button6.Location = New System.Drawing.Point(548, 455)
      Me.button6.Name = "button6"
      Me.button6.Size = New System.Drawing.Size(57, 23)
      Me.button6.TabIndex = 6
      Me.button6.Text = "Re&move"
      Me.button6.UseVisualStyleBackColor = True
      '			Me.button6.Click += New System.EventHandler(Me.button6_Click);
      ' 
      ' button4
      ' 
      Me.button4.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button4.Location = New System.Drawing.Point(611, 455)
      Me.button4.Name = "button4"
      Me.button4.Size = New System.Drawing.Size(86, 23)
      Me.button4.TabIndex = 7
      Me.button4.Text = "Copy &Selection"
      Me.button4.UseVisualStyleBackColor = True
      '			Me.button4.Click += New System.EventHandler(Me.Button4Click);
      ' 
      ' button1
      ' 
      Me.button1.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button1.Location = New System.Drawing.Point(703, 455)
      Me.button1.Name = "button1"
      Me.button1.Size = New System.Drawing.Size(77, 23)
      Me.button1.TabIndex = 8
      Me.button1.Text = "&Rebuild List"
      Me.button1.UseVisualStyleBackColor = True
      '			Me.button1.Click += New System.EventHandler(Me.Button1Click);
      ' 
      ' checkBox4
      ' 
      Me.checkBox4.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox4.Location = New System.Drawing.Point(99, 455)
      Me.checkBox4.Name = "checkBox4"
      Me.checkBox4.Size = New System.Drawing.Size(83, 24)
      Me.checkBox4.TabIndex = 2
      Me.checkBox4.Text = "Item &Count"
      Me.checkBox4.UseVisualStyleBackColor = True
      '			Me.checkBox4.CheckedChanged += New System.EventHandler(Me.CheckBox4CheckedChanged);
      ' 
      ' checkBox3
      ' 
      Me.checkBox3.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox3.Location = New System.Drawing.Point(6, 455)
      Me.checkBox3.Name = "checkBox3"
      Me.checkBox3.Size = New System.Drawing.Size(104, 24)
      Me.checkBox3.TabIndex = 1
      Me.checkBox3.Text = "Show &Groups"
      Me.checkBox3.UseVisualStyleBackColor = True
      '			Me.checkBox3.CheckedChanged += New System.EventHandler(Me.CheckBox3CheckedChanged);
      ' 
      ' label1
      ' 
      Me.label1.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label1.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label1.Location = New System.Drawing.Point(6, 6)
      Me.label1.Name = "label1"
      Me.label1.Size = New System.Drawing.Size(774, 46)
      Me.label1.TabIndex = 1
      Me.label1.Text = resources.GetString("label1.Text")
      ' 
      ' listViewSimple
      ' 
      Me.listViewSimple.AllColumns.Add(Me.columnHeader11)
      Me.listViewSimple.AllColumns.Add(Me.columnHeader12)
      Me.listViewSimple.AllColumns.Add(Me.columnHeader13)
      Me.listViewSimple.AllColumns.Add(Me.columnHeader14)
      Me.listViewSimple.AllColumns.Add(Me.columnHeader15)
      Me.listViewSimple.AllColumns.Add(Me.columnHeader16)
      Me.listViewSimple.AllColumns.Add(Me.olvColumn34)
      Me.listViewSimple.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.listViewSimple.CheckBoxes = True
      Me.listViewSimple.CheckedAspectName = "CanTellJokes"
      Me.listViewSimple.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnHeader11, Me.columnHeader12, Me.columnHeader13, Me.columnHeader14, Me.columnHeader15, Me.columnHeader16, Me.olvColumn34})
      Me.listViewSimple.ContextMenuStrip = Me.contextMenuStrip1
      Me.listViewSimple.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.listViewSimple.HideSelection = False
      Me.listViewSimple.HotItemStyle = Me.hotItemStyle1
      Me.listViewSimple.ItemRenderer = Nothing
      Me.listViewSimple.Location = New System.Drawing.Point(6, 55)
      Me.listViewSimple.Name = "listViewSimple"
      Me.listViewSimple.ShowGroups = False
      Me.listViewSimple.ShowImagesOnSubItems = True
      Me.listViewSimple.Size = New System.Drawing.Size(774, 394)
      Me.listViewSimple.SortGroupItemsByPrimaryColumn = False
      Me.listViewSimple.TabIndex = 0
      Me.listViewSimple.UseAlternatingBackColors = True
      Me.listViewSimple.UseCompatibleStateImageBehavior = False
      Me.listViewSimple.UseHotItem = True
      Me.listViewSimple.View = System.Windows.Forms.View.Details
      '			Me.listViewSimple.ItemCheck += New System.Windows.Forms.ItemCheckEventHandler(Me.listViewSimple_ItemCheck);
      '			Me.listViewSimple.SelectedIndexChanged += New System.EventHandler(Me.ListViewSelectedIndexChanged);
      '			Me.listViewSimple.ItemChecked += New System.Windows.Forms.ItemCheckedEventHandler(Me.listViewSimple_ItemChecked);
      ' 
      ' columnHeader11
      ' 
      Me.columnHeader11.AspectName = "Name"
      Me.columnHeader11.MaximumWidth = 200
      Me.columnHeader11.MinimumWidth = 100
      Me.columnHeader11.Text = "Person"
      Me.columnHeader11.ToolTipText = "This is a long tooltip text that should appear when the mouse is over this column" & " header but contains absolutely no useful information :)"
      Me.columnHeader11.UseInitialLetterForGroup = True
      Me.columnHeader11.Width = 140
      ' 
      ' columnHeader12
      ' 
      Me.columnHeader12.AspectName = "Occupation"
      Me.columnHeader12.MaximumWidth = 180
      Me.columnHeader12.MinimumWidth = 50
      Me.columnHeader12.Text = "Occupation"
      Me.columnHeader12.Width = 112
      ' 
      ' columnHeader13
      ' 
      Me.columnHeader13.AspectName = "CulinaryRating"
      Me.columnHeader13.Text = "Cooking Skill"
      Me.columnHeader13.Width = 74
      ' 
      ' columnHeader14
      ' 
      Me.columnHeader14.AspectName = "YearOfBirth"
      Me.columnHeader14.MaximumWidth = 81
      Me.columnHeader14.MinimumWidth = 81
      Me.columnHeader14.Text = "Year of birth"
      Me.columnHeader14.Width = 81
      ' 
      ' columnHeader15
      ' 
      Me.columnHeader15.AspectName = "BirthDate"
      Me.columnHeader15.AspectToStringFormat = "{0:d}"
      Me.columnHeader15.Text = "Birthday"
      Me.columnHeader15.Width = 121
      ' 
      ' columnHeader16
      ' 
      Me.columnHeader16.AspectName = "GetRate"
      Me.columnHeader16.AspectToStringFormat = "{0:C}"
      Me.columnHeader16.Text = "Hourly Rate"
      Me.columnHeader16.Width = 93
      ' 
      ' olvColumn34
      ' 
      Me.olvColumn34.AspectName = "Comments"
      Me.olvColumn34.FillsFreeSpace = True
      Me.olvColumn34.IsTileViewColumn = True
      Me.olvColumn34.MinimumWidth = 30
      Me.olvColumn34.Text = "Comments"
      Me.olvColumn34.ToolTipText = "This is the tool tip for the Comments column. This is configured through the Tool" & "TipText property."
      Me.olvColumn34.UseInitialLetterForGroup = True
      ' 
      ' contextMenuStrip1
      ' 
      Me.contextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.command1ToolStripMenuItem, Me.command2ToolStripMenuItem, Me.command3ToolStripMenuItem, Me.appearOnTheColumnHeadersToolStripMenuItem})
      Me.contextMenuStrip1.Name = "contextMenuStrip1"
      Me.contextMenuStrip1.Size = New System.Drawing.Size(270, 92)
      ' 
      ' command1ToolStripMenuItem
      ' 
      Me.command1ToolStripMenuItem.Name = "command1ToolStripMenuItem"
      Me.command1ToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
      Me.command1ToolStripMenuItem.Text = "Dummy commands..."
      ' 
      ' command2ToolStripMenuItem
      ' 
      Me.command2ToolStripMenuItem.Name = "command2ToolStripMenuItem"
      Me.command2ToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
      Me.command2ToolStripMenuItem.Text = "...to test that a context menu..."
      ' 
      ' command3ToolStripMenuItem
      ' 
      Me.command3ToolStripMenuItem.Name = "command3ToolStripMenuItem"
      Me.command3ToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
      Me.command3ToolStripMenuItem.Text = "...appears here and a different one..."
      ' 
      ' appearOnTheColumnHeadersToolStripMenuItem
      ' 
      Me.appearOnTheColumnHeadersToolStripMenuItem.Name = "appearOnTheColumnHeadersToolStripMenuItem"
      Me.appearOnTheColumnHeadersToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
      Me.appearOnTheColumnHeadersToolStripMenuItem.Text = "...appear on the column headers."
      ' 
      ' hotItemStyle1
      ' 
      Me.hotItemStyle1.BackColor = System.Drawing.Color.PeachPuff
      Me.hotItemStyle1.ForeColor = System.Drawing.Color.MediumBlue
      ' 
      ' tabPage2
      ' 
      Me.tabPage2.Controls.Add(Me.button16)
      Me.tabPage2.Controls.Add(Me.button17)
      Me.tabPage2.Controls.Add(Me.comboBox5)
      Me.tabPage2.Controls.Add(Me.label20)
      Me.tabPage2.Controls.Add(Me.label6)
      Me.tabPage2.Controls.Add(Me.comboBox1)
      Me.tabPage2.Controls.Add(Me.checkBox6)
      Me.tabPage2.Controls.Add(Me.button5)
      Me.tabPage2.Controls.Add(Me.button2)
      Me.tabPage2.Controls.Add(Me.label2)
      Me.tabPage2.Controls.Add(Me.checkBox2)
      Me.tabPage2.Controls.Add(Me.checkBox1)
      Me.tabPage2.Controls.Add(Me.listViewComplex)
      Me.tabPage2.Location = New System.Drawing.Point(4, 22)
      Me.tabPage2.Name = "tabPage2"
      Me.tabPage2.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage2.Size = New System.Drawing.Size(786, 485)
      Me.tabPage2.TabIndex = 1
      Me.tabPage2.Text = "Complex Example"
      Me.tabPage2.UseVisualStyleBackColor = True
      ' 
      ' button16
      ' 
      Me.button16.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button16.Location = New System.Drawing.Point(527, 455)
      Me.button16.Name = "button16"
      Me.button16.Size = New System.Drawing.Size(46, 23)
      Me.button16.TabIndex = 8
      Me.button16.Text = "Add"
      Me.button16.UseVisualStyleBackColor = True
      '			Me.button16.Click += New System.EventHandler(Me.button16_Click);
      ' 
      ' button17
      ' 
      Me.button17.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button17.Location = New System.Drawing.Point(575, 455)
      Me.button17.Name = "button17"
      Me.button17.Size = New System.Drawing.Size(57, 23)
      Me.button17.TabIndex = 9
      Me.button17.Text = "Remove"
      Me.button17.UseVisualStyleBackColor = True
      '			Me.button17.Click += New System.EventHandler(Me.button17_Click);
      ' 
      ' comboBox5
      ' 
      Me.comboBox5.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox5.FormattingEnabled = True
      Me.comboBox5.Items.AddRange(New Object() {"No", "Single Click", "Double Click", "F2 Only"})
      Me.comboBox5.Location = New System.Drawing.Point(331, 455)
      Me.comboBox5.Name = "comboBox5"
      Me.comboBox5.Size = New System.Drawing.Size(71, 21)
      Me.comboBox5.TabIndex = 5
      '			Me.comboBox5.SelectedIndexChanged += New System.EventHandler(Me.comboBox5_SelectedIndexChanged);
      ' 
      ' label20
      ' 
      Me.label20.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.label20.AutoSize = True
      Me.label20.Location = New System.Drawing.Point(277, 459)
      Me.label20.Name = "label20"
      Me.label20.Size = New System.Drawing.Size(48, 13)
      Me.label20.TabIndex = 4
      Me.label20.Text = "Editable:"
      ' 
      ' label6
      ' 
      Me.label6.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.label6.AutoSize = True
      Me.label6.Location = New System.Drawing.Point(408, 459)
      Me.label6.Name = "label6"
      Me.label6.Size = New System.Drawing.Size(33, 13)
      Me.label6.TabIndex = 6
      Me.label6.Text = "View:"
      ' 
      ' comboBox1
      ' 
      Me.comboBox1.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox1.FormattingEnabled = True
      Me.comboBox1.Items.AddRange(New Object() {"Small Icon", "Large Icon", "List", "Tile", "Details"})
      Me.comboBox1.Location = New System.Drawing.Point(447, 455)
      Me.comboBox1.Name = "comboBox1"
      Me.comboBox1.Size = New System.Drawing.Size(63, 21)
      Me.comboBox1.TabIndex = 7
      '			Me.comboBox1.SelectedIndexChanged += New System.EventHandler(Me.comboBox1_SelectedIndexChanged);
      ' 
      ' checkBox6
      ' 
      Me.checkBox6.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox6.Checked = True
      Me.checkBox6.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBox6.Location = New System.Drawing.Point(185, 455)
      Me.checkBox6.Name = "checkBox6"
      Me.checkBox6.Size = New System.Drawing.Size(86, 21)
      Me.checkBox6.TabIndex = 3
      Me.checkBox6.Text = "Owner &Draw"
      Me.checkBox6.UseVisualStyleBackColor = True
      '			Me.checkBox6.CheckedChanged += New System.EventHandler(Me.CheckBox6CheckedChanged);
      ' 
      ' button5
      ' 
      Me.button5.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button5.Location = New System.Drawing.Point(634, 455)
      Me.button5.Name = "button5"
      Me.button5.Size = New System.Drawing.Size(88, 23)
      Me.button5.TabIndex = 10
      Me.button5.Text = "Copy &Selection"
      Me.button5.UseVisualStyleBackColor = True
      '			Me.button5.Click += New System.EventHandler(Me.Button5Click);
      ' 
      ' button2
      ' 
      Me.button2.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button2.Location = New System.Drawing.Point(724, 455)
      Me.button2.Name = "button2"
      Me.button2.Size = New System.Drawing.Size(56, 23)
      Me.button2.TabIndex = 11
      Me.button2.Text = "&Rebuild"
      Me.button2.UseVisualStyleBackColor = True
      '			Me.button2.Click += New System.EventHandler(Me.Button2Click);
      ' 
      ' label2
      ' 
      Me.label2.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label2.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label2.Location = New System.Drawing.Point(6, 6)
      Me.label2.Name = "label2"
      Me.label2.Size = New System.Drawing.Size(774, 48)
      Me.label2.TabIndex = 3
      Me.label2.Text = resources.GetString("label2.Text")
      ' 
      ' checkBox2
      ' 
      Me.checkBox2.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox2.Location = New System.Drawing.Point(101, 455)
      Me.checkBox2.Name = "checkBox2"
      Me.checkBox2.Size = New System.Drawing.Size(78, 21)
      Me.checkBox2.TabIndex = 2
      Me.checkBox2.Text = "Item &Count"
      Me.checkBox2.UseVisualStyleBackColor = True
      '			Me.checkBox2.CheckedChanged += New System.EventHandler(Me.CheckBox2CheckedChanged);
      ' 
      ' checkBox1
      ' 
      Me.checkBox1.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox1.Location = New System.Drawing.Point(6, 455)
      Me.checkBox1.Name = "checkBox1"
      Me.checkBox1.Size = New System.Drawing.Size(92, 21)
      Me.checkBox1.TabIndex = 1
      Me.checkBox1.Text = "Show &Groups"
      Me.checkBox1.UseVisualStyleBackColor = True
      '			Me.checkBox1.CheckedChanged += New System.EventHandler(Me.CheckBox1CheckedChanged);
      ' 
      ' listViewComplex
      ' 
      Me.listViewComplex.AllColumns.Add(Me.personColumn)
      Me.listViewComplex.AllColumns.Add(Me.occupationColumn)
      Me.listViewComplex.AllColumns.Add(Me.columnCookingSkill)
      Me.listViewComplex.AllColumns.Add(Me.yearOfBirthColumn)
      Me.listViewComplex.AllColumns.Add(Me.birthdayColumn)
      Me.listViewComplex.AllColumns.Add(Me.hourlyRateColumn)
      Me.listViewComplex.AllColumns.Add(Me.moneyImageColumn)
      Me.listViewComplex.AllColumns.Add(Me.daysSinceBirthColumn)
      Me.listViewComplex.AllColumns.Add(Me.olvJokeColumn)
      Me.listViewComplex.AllColumns.Add(Me.olvColumn41)
      Me.listViewComplex.AllowColumnReorder = True
      Me.listViewComplex.AlternateRowBackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(192))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(192))))))
      Me.listViewComplex.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.listViewComplex.BackColor = System.Drawing.SystemColors.Window
      Me.listViewComplex.CheckBoxes = True
      Me.listViewComplex.CheckedAspectName = "IsActive"
      Me.listViewComplex.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.personColumn, Me.occupationColumn, Me.columnCookingSkill, Me.birthdayColumn, Me.hourlyRateColumn, Me.moneyImageColumn, Me.daysSinceBirthColumn, Me.olvJokeColumn, Me.olvColumn41})
      Me.listViewComplex.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.listViewComplex.FullRowSelect = True
      Me.listViewComplex.GroupWithItemCountFormat = "{0} ({1} people)"
      Me.listViewComplex.GroupWithItemCountSingularFormat = "{0} ({1} person)"
      Me.listViewComplex.HideSelection = False
      Me.listViewComplex.ItemRenderer = Nothing
      Me.listViewComplex.LargeImageList = Me.imageList2
      Me.listViewComplex.Location = New System.Drawing.Point(6, 57)
      Me.listViewComplex.Name = "listViewComplex"
      Me.listViewComplex.OwnerDraw = True
      Me.listViewComplex.ShowGroups = False
      Me.listViewComplex.ShowImagesOnSubItems = True
      Me.listViewComplex.ShowItemToolTips = True
      Me.listViewComplex.Size = New System.Drawing.Size(774, 391)
      Me.listViewComplex.SmallImageList = Me.imageList1
      Me.listViewComplex.TabIndex = 0
      Me.listViewComplex.UseAlternatingBackColors = True
      Me.listViewComplex.UseCompatibleStateImageBehavior = False
      Me.listViewComplex.UseHotItem = True
      Me.listViewComplex.UseSubItemCheckBoxes = True
      Me.listViewComplex.View = System.Windows.Forms.View.Details
      '			Me.listViewComplex.CellEditFinishing += New BrightIdeasSoftware.CellEditEventHandler(Me.listViewComplex_CellEditFinishing);
      '			Me.listViewComplex.CellEditStarting += New BrightIdeasSoftware.CellEditEventHandler(Me.listViewComplex_CellEditStarting);
      '			Me.listViewComplex.CellEditValidating += New BrightIdeasSoftware.CellEditEventHandler(Me.listViewComplex_CellEditValidating);
      '			Me.listViewComplex.MouseClick += New System.Windows.Forms.MouseEventHandler(Me.listViewComplex_MouseClick);
      '			Me.listViewComplex.SelectedIndexChanged += New System.EventHandler(Me.ListViewSelectedIndexChanged);
      ' 
      ' personColumn
      ' 
      Me.personColumn.AspectName = "Name"
      Me.personColumn.Text = "Person"
      Me.personColumn.UseInitialLetterForGroup = True
      Me.personColumn.Width = 114
      ' 
      ' occupationColumn
      ' 
      Me.occupationColumn.AspectName = "Occupation"
      Me.occupationColumn.IsTileViewColumn = True
      Me.occupationColumn.Text = "Occupation"
      Me.occupationColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.occupationColumn.Width = 92
      ' 
      ' columnCookingSkill
      ' 
      Me.columnCookingSkill.AspectName = "CulinaryRating"
      Me.columnCookingSkill.GroupWithItemCountFormat = "{0} ({1} candidates)"
      Me.columnCookingSkill.GroupWithItemCountSingularFormat = "{0} (only {1} candidate)"
      Me.columnCookingSkill.Renderer = Me.cookingSkillRenderer
      Me.columnCookingSkill.Text = "Cooking skill"
      Me.columnCookingSkill.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.columnCookingSkill.Width = 75
      ' 
      ' cookingSkillRenderer
      ' 
      Me.cookingSkillRenderer.ImageName = "star"
      Me.cookingSkillRenderer.MaximumValue = 40
      Me.cookingSkillRenderer.MaxNumberImages = 5
      ' 
      ' yearOfBirthColumn
      ' 
      Me.yearOfBirthColumn.AspectName = "YearOfBirth"
      Me.yearOfBirthColumn.DisplayIndex = 3
      Me.yearOfBirthColumn.IsVisible = False
      Me.yearOfBirthColumn.Text = "Year Of Birth"
      Me.yearOfBirthColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.yearOfBirthColumn.Width = 80
      ' 
      ' birthdayColumn
      ' 
      Me.birthdayColumn.AspectName = "BirthDate"
      Me.birthdayColumn.AspectToStringFormat = "{0:D}"
      Me.birthdayColumn.GroupWithItemCountFormat = "{0} has {1} birthdays"
      Me.birthdayColumn.GroupWithItemCountSingularFormat = "{0} has only {1} birthday"
      Me.birthdayColumn.IsTileViewColumn = True
      Me.birthdayColumn.Text = "Birthday"
      Me.birthdayColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.birthdayColumn.Width = 111
      ' 
      ' hourlyRateColumn
      ' 
      Me.hourlyRateColumn.AspectName = "GetRate"
      Me.hourlyRateColumn.AspectToStringFormat = "{0:C}"
      Me.hourlyRateColumn.IsTileViewColumn = True
      Me.hourlyRateColumn.Text = "Hourly Rate"
      Me.hourlyRateColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.hourlyRateColumn.Width = 71
      ' 
      ' moneyImageColumn
      ' 
      Me.moneyImageColumn.IsEditable = False
      Me.moneyImageColumn.Text = "Salary"
      Me.moneyImageColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.moneyImageColumn.Width = 55
      ' 
      ' daysSinceBirthColumn
      ' 
      Me.daysSinceBirthColumn.IsEditable = False
      Me.daysSinceBirthColumn.Text = "Days Since Birth"
      Me.daysSinceBirthColumn.Width = 81
      ' 
      ' olvJokeColumn
      ' 
      Me.olvJokeColumn.AspectName = "CanTellJokes"
      Me.olvJokeColumn.CheckBoxes = True
      Me.olvJokeColumn.Text = "Tells Jokes?"
      Me.olvJokeColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.olvJokeColumn.Width = 74
      ' 
      ' olvColumn41
      ' 
      Me.olvColumn41.AspectName = "MaritalStatus"
      Me.olvColumn41.Text = "Married?"
      Me.olvColumn41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      ' 
      ' imageList2
      ' 
      Me.imageList2.ImageStream = (CType(resources.GetObject("imageList2.ImageStream"), System.Windows.Forms.ImageListStreamer))
      Me.imageList2.TransparentColor = System.Drawing.Color.Transparent
      Me.imageList2.Images.SetKeyName(0, "user")
      Me.imageList2.Images.SetKeyName(1, "compass")
      Me.imageList2.Images.SetKeyName(2, "down")
      Me.imageList2.Images.SetKeyName(3, "find")
      Me.imageList2.Images.SetKeyName(4, "folder")
      Me.imageList2.Images.SetKeyName(5, "movie")
      Me.imageList2.Images.SetKeyName(6, "music")
      Me.imageList2.Images.SetKeyName(7, "no")
      Me.imageList2.Images.SetKeyName(8, "readonly")
      Me.imageList2.Images.SetKeyName(9, "public")
      Me.imageList2.Images.SetKeyName(10, "recycle")
      Me.imageList2.Images.SetKeyName(11, "spanner")
      Me.imageList2.Images.SetKeyName(12, "star")
      Me.imageList2.Images.SetKeyName(13, "tick")
      Me.imageList2.Images.SetKeyName(14, "archive")
      Me.imageList2.Images.SetKeyName(15, "system")
      Me.imageList2.Images.SetKeyName(16, "hidden")
      Me.imageList2.Images.SetKeyName(17, "temporary")
      ' 
      ' tabPage3
      ' 
      Me.tabPage3.Controls.Add(Me.groupBox3)
      Me.tabPage3.Controls.Add(Me.groupBox1)
      Me.tabPage3.Controls.Add(Me.label3)
      Me.tabPage3.Location = New System.Drawing.Point(4, 22)
      Me.tabPage3.Name = "tabPage3"
      Me.tabPage3.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage3.Size = New System.Drawing.Size(786, 485)
      Me.tabPage3.TabIndex = 2
      Me.tabPage3.Text = "DataSet Example"
      Me.tabPage3.UseVisualStyleBackColor = True
      ' 
      ' groupBox3
      ' 
      Me.groupBox3.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.groupBox3.Controls.Add(Me.label22)
      Me.groupBox3.Controls.Add(Me.comboBox7)
      Me.groupBox3.Controls.Add(Me.checkBoxPause)
      Me.groupBox3.Controls.Add(Me.rowHeightUpDown)
      Me.groupBox3.Controls.Add(Me.label11)
      Me.groupBox3.Controls.Add(Me.label8)
      Me.groupBox3.Controls.Add(Me.comboBox3)
      Me.groupBox3.Controls.Add(Me.checkBox5)
      Me.groupBox3.Controls.Add(Me.listViewDataSet)
      Me.groupBox3.Controls.Add(Me.checkBox7)
      Me.groupBox3.Controls.Add(Me.checkBox8)
      Me.groupBox3.Location = New System.Drawing.Point(6, 263)
      Me.groupBox3.Name = "groupBox3"
      Me.groupBox3.Size = New System.Drawing.Size(774, 216)
      Me.groupBox3.TabIndex = 15
      Me.groupBox3.TabStop = False
      Me.groupBox3.Text = "Data List View"
      ' 
      ' label22
      ' 
      Me.label22.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label22.AutoSize = True
      Me.label22.Location = New System.Drawing.Point(664, 133)
      Me.label22.Name = "label22"
      Me.label22.Size = New System.Drawing.Size(48, 13)
      Me.label22.TabIndex = 7
      Me.label22.Text = "&Editable:"
      ' 
      ' comboBox7
      ' 
      Me.comboBox7.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox7.FormattingEnabled = True
      Me.comboBox7.Items.AddRange(New Object() {"No", "Single Click", "Double Click", "F2 Only"})
      Me.comboBox7.Location = New System.Drawing.Point(664, 148)
      Me.comboBox7.Name = "comboBox7"
      Me.comboBox7.Size = New System.Drawing.Size(104, 21)
      Me.comboBox7.TabIndex = 8
      '			Me.comboBox7.SelectedIndexChanged += New System.EventHandler(Me.comboBox7_SelectedIndexChanged);
      ' 
      ' checkBoxPause
      ' 
      Me.checkBoxPause.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.checkBoxPause.Checked = True
      Me.checkBoxPause.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBoxPause.Location = New System.Drawing.Point(664, 73)
      Me.checkBoxPause.Name = "checkBoxPause"
      Me.checkBoxPause.Size = New System.Drawing.Size(113, 19)
      Me.checkBoxPause.TabIndex = 4
      Me.checkBoxPause.Text = "Pause &Animations"
      Me.checkBoxPause.UseVisualStyleBackColor = True
      '			Me.checkBoxPause.CheckedChanged += New System.EventHandler(Me.checkBoxPause_CheckedChanged);
      ' 
      ' rowHeightUpDown
      ' 
      Me.rowHeightUpDown.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.rowHeightUpDown.Location = New System.Drawing.Point(664, 187)
      Me.rowHeightUpDown.Maximum = New Decimal(New Integer() {128, 0, 0, 0})
      Me.rowHeightUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
      Me.rowHeightUpDown.Name = "rowHeightUpDown"
      Me.rowHeightUpDown.Size = New System.Drawing.Size(101, 20)
      Me.rowHeightUpDown.TabIndex = 10
      Me.rowHeightUpDown.Value = New Decimal(New Integer() {16, 0, 0, 0})
      '			Me.rowHeightUpDown.ValueChanged += New System.EventHandler(Me.rowHeightUpDown_ValueChanged);
      ' 
      ' label11
      ' 
      Me.label11.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label11.AutoSize = True
      Me.label11.Location = New System.Drawing.Point(664, 172)
      Me.label11.Name = "label11"
      Me.label11.Size = New System.Drawing.Size(66, 13)
      Me.label11.TabIndex = 9
      Me.label11.Text = "Row &Height:"
      ' 
      ' label8
      ' 
      Me.label8.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label8.AutoSize = True
      Me.label8.Location = New System.Drawing.Point(664, 93)
      Me.label8.Name = "label8"
      Me.label8.Size = New System.Drawing.Size(33, 13)
      Me.label8.TabIndex = 5
      Me.label8.Text = "&View:"
      ' 
      ' comboBox3
      ' 
      Me.comboBox3.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox3.FormattingEnabled = True
      Me.comboBox3.Items.AddRange(New Object() {"Small Icon", "Large Icon", "List", "Tile", "Details"})
      Me.comboBox3.Location = New System.Drawing.Point(664, 108)
      Me.comboBox3.Name = "comboBox3"
      Me.comboBox3.Size = New System.Drawing.Size(104, 21)
      Me.comboBox3.TabIndex = 6
      '			Me.comboBox3.SelectedIndexChanged += New System.EventHandler(Me.comboBox3_SelectedIndexChanged);
      ' 
      ' checkBox5
      ' 
      Me.checkBox5.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.checkBox5.Checked = True
      Me.checkBox5.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBox5.Location = New System.Drawing.Point(664, 55)
      Me.checkBox5.Name = "checkBox5"
      Me.checkBox5.Size = New System.Drawing.Size(113, 19)
      Me.checkBox5.TabIndex = 3
      Me.checkBox5.Text = "Owner &Draw"
      Me.checkBox5.UseVisualStyleBackColor = True
      '			Me.checkBox5.CheckedChanged += New System.EventHandler(Me.CheckBox5CheckedChanged);
      ' 
      ' listViewDataSet
      ' 
      Me.listViewDataSet.AllColumns.Add(Me.olvColumn1)
      Me.listViewDataSet.AllColumns.Add(Me.olvColumn2)
      Me.listViewDataSet.AllColumns.Add(Me.olvColumn3)
      Me.listViewDataSet.AllColumns.Add(Me.salaryColumn)
      Me.listViewDataSet.AllColumns.Add(Me.heightColumn)
      Me.listViewDataSet.AllColumns.Add(Me.olvColumn42)
      Me.listViewDataSet.AllColumns.Add(Me.olvColumnGif)
      Me.listViewDataSet.AllowColumnReorder = True
      Me.listViewDataSet.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.listViewDataSet.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClick
      Me.listViewDataSet.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.olvColumn1, Me.olvColumn2, Me.olvColumn3, Me.salaryColumn, Me.heightColumn, Me.olvColumn42, Me.olvColumnGif})
      Me.listViewDataSet.DataSource = Nothing
      Me.listViewDataSet.EmptyListMsg = "Add rows to the above table to see them here"
      Me.listViewDataSet.EmptyListMsgFont = New System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.listViewDataSet.GridLines = True
      Me.listViewDataSet.GroupWithItemCountFormat = "{0} ({1} people)"
      Me.listViewDataSet.GroupWithItemCountSingularFormat = "{0} (1 person)"
      Me.listViewDataSet.HideSelection = False
      Me.listViewDataSet.HighlightBackgroundColor = System.Drawing.Color.Crimson
      Me.listViewDataSet.HighlightForegroundColor = System.Drawing.Color.DarkGreen
      Me.listViewDataSet.ItemRenderer = Nothing
      Me.listViewDataSet.LargeImageList = Me.imageList2
      Me.listViewDataSet.Location = New System.Drawing.Point(6, 19)
      Me.listViewDataSet.Name = "listViewDataSet"
      Me.listViewDataSet.OwnerDraw = True
      Me.listViewDataSet.ShowGroups = False
      Me.listViewDataSet.ShowImagesOnSubItems = True
      Me.listViewDataSet.ShowItemToolTips = True
      Me.listViewDataSet.Size = New System.Drawing.Size(652, 191)
      Me.listViewDataSet.SmallImageList = Me.imageList1
      Me.listViewDataSet.TabIndex = 0
      Me.listViewDataSet.UseCompatibleStateImageBehavior = False
      Me.listViewDataSet.UseHotItem = True
      Me.listViewDataSet.View = System.Windows.Forms.View.Details
      '			Me.listViewDataSet.SelectedIndexChanged += New System.EventHandler(Me.ListViewDataSetSelectedIndexChanged);
      ' 
      ' olvColumn1
      ' 
      Me.olvColumn1.AspectName = "Name"
      Me.olvColumn1.IsTileViewColumn = True
      Me.olvColumn1.Text = "Name"
      Me.olvColumn1.UseInitialLetterForGroup = True
      Me.olvColumn1.Width = 112
      ' 
      ' olvColumn2
      ' 
      Me.olvColumn2.AspectName = "Company"
      Me.olvColumn2.IsTileViewColumn = True
      Me.olvColumn2.Text = "Company"
      Me.olvColumn2.Width = 104
      ' 
      ' olvColumn3
      ' 
      Me.olvColumn3.AspectName = "Occupation"
      Me.olvColumn3.IsTileViewColumn = True
      Me.olvColumn3.Text = "Occupation"
      Me.olvColumn3.Width = 109
      ' 
      ' salaryColumn
      ' 
      Me.salaryColumn.AspectName = "Salary"
      Me.salaryColumn.AspectToStringFormat = "{0:C}"
      Me.salaryColumn.Renderer = Me.salaryRenderer
      Me.salaryColumn.Text = "Salary"
      Me.salaryColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.salaryColumn.Width = 84
      ' 
      ' salaryRenderer
      ' 
      Me.salaryRenderer.ImageName = "tick"
      Me.salaryRenderer.MaximumValue = 500000
      Me.salaryRenderer.MaxNumberImages = 5
      Me.salaryRenderer.MinimumValue = 10000
      ' 
      ' heightColumn
      ' 
      Me.heightColumn.AspectName = "Height"
      Me.heightColumn.Renderer = Me.heightRenderer
      Me.heightColumn.Text = "Height (m)"
      Me.heightColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.heightColumn.Width = 72
      ' 
      ' heightRenderer
      ' 
      Me.heightRenderer.BackgroundColor = System.Drawing.Color.Green
      Me.heightRenderer.MaximumValue = 2
      Me.heightRenderer.UseStandardBar = False
      ' 
      ' olvColumn42
      ' 
      Me.olvColumn42.AspectName = "TellsJokes"
      Me.olvColumn42.CheckBoxes = True
      Me.olvColumn42.Text = "Joker?"
      Me.olvColumn42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.olvColumn42.Width = 48
      ' 
      ' olvColumnGif
      ' 
      Me.olvColumnGif.AspectName = "GifFileName"
      Me.olvColumnGif.Renderer = Me.imageRenderer1
      Me.olvColumnGif.Text = "Animated GIF"
      Me.olvColumnGif.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.olvColumnGif.Width = 96
      ' 
      ' checkBox7
      ' 
      Me.checkBox7.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.checkBox7.Location = New System.Drawing.Point(664, 18)
      Me.checkBox7.Name = "checkBox7"
      Me.checkBox7.Size = New System.Drawing.Size(104, 20)
      Me.checkBox7.TabIndex = 1
      Me.checkBox7.Text = "Show &Groups"
      Me.checkBox7.UseVisualStyleBackColor = True
      '			Me.checkBox7.CheckedChanged += New System.EventHandler(Me.CheckBox7CheckedChanged);
      ' 
      ' checkBox8
      ' 
      Me.checkBox8.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.checkBox8.Location = New System.Drawing.Point(664, 37)
      Me.checkBox8.Name = "checkBox8"
      Me.checkBox8.Size = New System.Drawing.Size(113, 19)
      Me.checkBox8.TabIndex = 2
      Me.checkBox8.Text = "Show Item &Counts"
      Me.checkBox8.UseVisualStyleBackColor = True
      '			Me.checkBox8.CheckedChanged += New System.EventHandler(Me.CheckBox8CheckedChanged);
      ' 
      ' groupBox1
      ' 
      Me.groupBox1.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.groupBox1.Controls.Add(Me.dataGridView1)
      Me.groupBox1.Controls.Add(Me.button3)
      Me.groupBox1.Location = New System.Drawing.Point(6, 56)
      Me.groupBox1.Name = "groupBox1"
      Me.groupBox1.Size = New System.Drawing.Size(774, 201)
      Me.groupBox1.TabIndex = 13
      Me.groupBox1.TabStop = False
      Me.groupBox1.Text = "Source Data Table"
      ' 
      ' dataGridView1
      ' 
      Me.dataGridView1.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
      Me.dataGridView1.Location = New System.Drawing.Point(6, 19)
      Me.dataGridView1.Name = "dataGridView1"
      Me.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
      Me.dataGridView1.Size = New System.Drawing.Size(652, 176)
      Me.dataGridView1.TabIndex = 0
      ' 
      ' button3
      ' 
      Me.button3.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button3.Location = New System.Drawing.Point(664, 19)
      Me.button3.Name = "button3"
      Me.button3.Size = New System.Drawing.Size(104, 23)
      Me.button3.TabIndex = 1
      Me.button3.Text = "&Reset Data"
      Me.button3.UseVisualStyleBackColor = True
      '			Me.button3.Click += New System.EventHandler(Me.Button3Click);
      ' 
      ' label3
      ' 
      Me.label3.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label3.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label3.Location = New System.Drawing.Point(6, 6)
      Me.label3.Name = "label3"
      Me.label3.Size = New System.Drawing.Size(774, 46)
      Me.label3.TabIndex = 9
      Me.label3.Text = resources.GetString("label3.Text")
      ' 
      ' tabPage4
      ' 
      Me.tabPage4.Controls.Add(Me.comboBox8)
      Me.tabPage4.Controls.Add(Me.label23)
      Me.tabPage4.Controls.Add(Me.button9)
      Me.tabPage4.Controls.Add(Me.button8)
      Me.tabPage4.Controls.Add(Me.label7)
      Me.tabPage4.Controls.Add(Me.comboBox2)
      Me.tabPage4.Controls.Add(Me.checkBox9)
      Me.tabPage4.Controls.Add(Me.label4)
      Me.tabPage4.Controls.Add(Me.listViewVirtual)
      Me.tabPage4.Location = New System.Drawing.Point(4, 22)
      Me.tabPage4.Name = "tabPage4"
      Me.tabPage4.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage4.Size = New System.Drawing.Size(786, 485)
      Me.tabPage4.TabIndex = 3
      Me.tabPage4.Text = "Virtual List"
      Me.tabPage4.UseVisualStyleBackColor = True
      ' 
      ' comboBox8
      ' 
      Me.comboBox8.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox8.FormattingEnabled = True
      Me.comboBox8.Items.AddRange(New Object() {"No", "Single Click", "Double Click", "F2 Only"})
      Me.comboBox8.Location = New System.Drawing.Point(360, 454)
      Me.comboBox8.Name = "comboBox8"
      Me.comboBox8.Size = New System.Drawing.Size(83, 21)
      Me.comboBox8.TabIndex = 3
      '			Me.comboBox8.SelectedIndexChanged += New System.EventHandler(Me.comboBox8_SelectedIndexChanged);
      ' 
      ' label23
      ' 
      Me.label23.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.label23.AutoSize = True
      Me.label23.Location = New System.Drawing.Point(306, 458)
      Me.label23.Name = "label23"
      Me.label23.Size = New System.Drawing.Size(48, 13)
      Me.label23.TabIndex = 2
      Me.label23.Text = "Editable:"
      ' 
      ' button9
      ' 
      Me.button9.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button9.Location = New System.Drawing.Point(624, 453)
      Me.button9.Name = "button9"
      Me.button9.Size = New System.Drawing.Size(75, 23)
      Me.button9.TabIndex = 6
      Me.button9.Text = "Deselect All"
      Me.button9.UseVisualStyleBackColor = True
      '			Me.button9.Click += New System.EventHandler(Me.button9_Click);
      ' 
      ' button8
      ' 
      Me.button8.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button8.Location = New System.Drawing.Point(705, 453)
      Me.button8.Name = "button8"
      Me.button8.Size = New System.Drawing.Size(75, 23)
      Me.button8.TabIndex = 7
      Me.button8.Text = "Select All"
      Me.button8.UseVisualStyleBackColor = True
      '			Me.button8.Click += New System.EventHandler(Me.button8_Click);
      ' 
      ' label7
      ' 
      Me.label7.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.label7.AutoSize = True
      Me.label7.Location = New System.Drawing.Point(453, 458)
      Me.label7.Name = "label7"
      Me.label7.Size = New System.Drawing.Size(33, 13)
      Me.label7.TabIndex = 4
      Me.label7.Text = "View:"
      ' 
      ' comboBox2
      ' 
      Me.comboBox2.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox2.FormattingEnabled = True
      Me.comboBox2.Items.AddRange(New Object() {"Small Icon", "Large Icon", "List", "Tile", "Details"})
      Me.comboBox2.Location = New System.Drawing.Point(493, 454)
      Me.comboBox2.Name = "comboBox2"
      Me.comboBox2.Size = New System.Drawing.Size(121, 21)
      Me.comboBox2.TabIndex = 5
      '			Me.comboBox2.SelectedIndexChanged += New System.EventHandler(Me.comboBox2_SelectedIndexChanged);
      ' 
      ' checkBox9
      ' 
      Me.checkBox9.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox9.Checked = True
      Me.checkBox9.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBox9.Location = New System.Drawing.Point(6, 454)
      Me.checkBox9.Name = "checkBox9"
      Me.checkBox9.Size = New System.Drawing.Size(113, 21)
      Me.checkBox9.TabIndex = 1
      Me.checkBox9.Text = "Owner &Draw"
      Me.checkBox9.UseVisualStyleBackColor = True
      '			Me.checkBox9.CheckedChanged += New System.EventHandler(Me.CheckBox9CheckedChanged);
      ' 
      ' label4
      ' 
      Me.label4.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label4.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label4.Location = New System.Drawing.Point(6, 6)
      Me.label4.Name = "label4"
      Me.label4.Size = New System.Drawing.Size(774, 38)
      Me.label4.TabIndex = 5
      Me.label4.Text = resources.GetString("label4.Text")
      ' 
      ' listViewVirtual
      ' 
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn4)
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn12)
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn5)
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn7)
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn8)
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn9)
      Me.listViewVirtual.AllColumns.Add(Me.olvColumn10)
      Me.listViewVirtual.AllowColumnReorder = True
      Me.listViewVirtual.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.listViewVirtual.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick
      Me.listViewVirtual.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.olvColumn4, Me.olvColumn12, Me.olvColumn5, Me.olvColumn7, Me.olvColumn8, Me.olvColumn9, Me.olvColumn10})
      Me.listViewVirtual.EmptyListMsg = "Will this work?"
      Me.listViewVirtual.EmptyListMsgFont = New System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.listViewVirtual.FullRowSelect = True
      Me.listViewVirtual.GridLines = True
      Me.listViewVirtual.HideSelection = False
      Me.listViewVirtual.ItemRenderer = Nothing
      Me.listViewVirtual.LargeImageList = Me.imageList2
      Me.listViewVirtual.Location = New System.Drawing.Point(6, 48)
      Me.listViewVirtual.Name = "listViewVirtual"
      Me.listViewVirtual.OwnerDraw = True
      Me.listViewVirtual.ShowGroups = False
      Me.listViewVirtual.ShowImagesOnSubItems = True
      Me.listViewVirtual.ShowItemToolTips = True
      Me.listViewVirtual.Size = New System.Drawing.Size(774, 399)
      Me.listViewVirtual.SmallImageList = Me.imageList1
      Me.listViewVirtual.TabIndex = 0
      Me.listViewVirtual.UseAlternatingBackColors = True
      Me.listViewVirtual.UseCompatibleStateImageBehavior = False
      Me.listViewVirtual.UseHotItem = True
      Me.listViewVirtual.View = System.Windows.Forms.View.Details
      Me.listViewVirtual.VirtualListSize = 10000000
      Me.listViewVirtual.VirtualMode = True
      '			Me.listViewVirtual.SelectionChanged += New System.EventHandler(Me.listViewVirtual_SelectionChanged);
      ' 
      ' olvColumn4
      ' 
      Me.olvColumn4.AspectName = "Name"
      Me.olvColumn4.Text = "Person"
      Me.olvColumn4.UseInitialLetterForGroup = True
      Me.olvColumn4.Width = 130
      ' 
      ' olvColumn12
      ' 
      Me.olvColumn12.AspectName = "serialNumber"
      Me.olvColumn12.IsEditable = False
      Me.olvColumn12.Text = "Serial #"
      ' 
      ' olvColumn5
      ' 
      Me.olvColumn5.AspectName = "Occupation"
      Me.olvColumn5.Text = "Occupation"
      Me.olvColumn5.Width = 100
      ' 
      ' olvColumn7
      ' 
      Me.olvColumn7.AspectName = "CulinaryRating"
      Me.olvColumn7.Text = "Cooking skill"
      Me.olvColumn7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.olvColumn7.Width = 80
      ' 
      ' olvColumn8
      ' 
      Me.olvColumn8.AspectName = "YearOfBirth"
      Me.olvColumn8.Text = "Year Of Birth"
      Me.olvColumn8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.olvColumn8.Width = 80
      ' 
      ' olvColumn9
      ' 
      Me.olvColumn9.AspectName = "BirthDate"
      Me.olvColumn9.AspectToStringFormat = "{0:D}"
      Me.olvColumn9.Text = "Birthday"
      Me.olvColumn9.Width = 120
      ' 
      ' olvColumn10
      ' 
      Me.olvColumn10.AspectName = "GetRate"
      Me.olvColumn10.AspectToStringFormat = "{0:C}"
      Me.olvColumn10.Text = "Hourly Rate"
      Me.olvColumn10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn10.Width = 80
      ' 
      ' tabPage5
      ' 
      Me.tabPage5.Controls.Add(Me.buttonSaveState)
      Me.tabPage5.Controls.Add(Me.buttonRestoreState)
      Me.tabPage5.Controls.Add(Me.button13)
      Me.tabPage5.Controls.Add(Me.buttonUp)
      Me.tabPage5.Controls.Add(Me.buttonGo)
      Me.tabPage5.Controls.Add(Me.textBoxFolderPath)
      Me.tabPage5.Controls.Add(Me.label10)
      Me.tabPage5.Controls.Add(Me.label9)
      Me.tabPage5.Controls.Add(Me.comboBox4)
      Me.tabPage5.Controls.Add(Me.checkBox10)
      Me.tabPage5.Controls.Add(Me.checkBox11)
      Me.tabPage5.Controls.Add(Me.checkBox12)
      Me.tabPage5.Controls.Add(Me.label5)
      Me.tabPage5.Controls.Add(Me.listViewFiles)
      Me.tabPage5.Location = New System.Drawing.Point(4, 22)
      Me.tabPage5.Name = "tabPage5"
      Me.tabPage5.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage5.Size = New System.Drawing.Size(786, 485)
      Me.tabPage5.TabIndex = 4
      Me.tabPage5.Text = "File Explorer"
      Me.tabPage5.UseVisualStyleBackColor = True
      ' 
      ' buttonSaveState
      ' 
      Me.buttonSaveState.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.buttonSaveState.Location = New System.Drawing.Point(483, 456)
      Me.buttonSaveState.Name = "buttonSaveState"
      Me.buttonSaveState.Size = New System.Drawing.Size(87, 23)
      Me.buttonSaveState.TabIndex = 10
      Me.buttonSaveState.Text = "Save State"
      Me.buttonSaveState.UseVisualStyleBackColor = True
      '			Me.buttonSaveState.Click += New System.EventHandler(Me.buttonSaveState_Click);
      ' 
      ' buttonRestoreState
      ' 
      Me.buttonRestoreState.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.buttonRestoreState.Enabled = False
      Me.buttonRestoreState.Location = New System.Drawing.Point(576, 456)
      Me.buttonRestoreState.Name = "buttonRestoreState"
      Me.buttonRestoreState.Size = New System.Drawing.Size(83, 23)
      Me.buttonRestoreState.TabIndex = 11
      Me.buttonRestoreState.Text = "Restore State"
      Me.buttonRestoreState.UseVisualStyleBackColor = True
      '			Me.buttonRestoreState.Click += New System.EventHandler(Me.buttonRestoreState_Click);
      ' 
      ' button13
      ' 
      Me.button13.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button13.Location = New System.Drawing.Point(665, 456)
      Me.button13.Name = "button13"
      Me.button13.Size = New System.Drawing.Size(115, 23)
      Me.button13.TabIndex = 12
      Me.button13.Text = "&Choose Columns..."
      Me.button13.UseVisualStyleBackColor = True
      '			Me.button13.Click += New System.EventHandler(Me.button13_Click);
      ' 
      ' buttonUp
      ' 
      Me.buttonUp.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.buttonUp.Location = New System.Drawing.Point(705, 55)
      Me.buttonUp.Name = "buttonUp"
      Me.buttonUp.Size = New System.Drawing.Size(75, 23)
      Me.buttonUp.TabIndex = 3
      Me.buttonUp.Text = "&Up"
      Me.buttonUp.UseVisualStyleBackColor = True
      '			Me.buttonUp.Click += New System.EventHandler(Me.buttonUp_Click);
      ' 
      ' buttonGo
      ' 
      Me.buttonGo.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.buttonGo.Location = New System.Drawing.Point(624, 55)
      Me.buttonGo.Name = "buttonGo"
      Me.buttonGo.Size = New System.Drawing.Size(75, 23)
      Me.buttonGo.TabIndex = 2
      Me.buttonGo.Text = "&Go"
      Me.buttonGo.UseVisualStyleBackColor = True
      '			Me.buttonGo.Click += New System.EventHandler(Me.ButtonGoClick);
      ' 
      ' textBoxFolderPath
      ' 
      Me.textBoxFolderPath.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.textBoxFolderPath.Location = New System.Drawing.Point(56, 57)
      Me.textBoxFolderPath.Name = "textBoxFolderPath"
      Me.textBoxFolderPath.Size = New System.Drawing.Size(562, 20)
      Me.textBoxFolderPath.TabIndex = 1
      '			Me.textBoxFolderPath.TextChanged += New System.EventHandler(Me.TextBoxFolderPathTextChanged);
      '			Me.textBoxFolderPath.KeyPress += New System.Windows.Forms.KeyPressEventHandler(Me.textBoxFolderPath_KeyPress);
      ' 
      ' label10
      ' 
      Me.label10.AutoSize = True
      Me.label10.Location = New System.Drawing.Point(6, 60)
      Me.label10.Name = "label10"
      Me.label10.Size = New System.Drawing.Size(39, 13)
      Me.label10.TabIndex = 0
      Me.label10.Text = "&Folder:"
      ' 
      ' label9
      ' 
      Me.label9.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.label9.AutoSize = True
      Me.label9.Location = New System.Drawing.Point(303, 461)
      Me.label9.Name = "label9"
      Me.label9.Size = New System.Drawing.Size(33, 13)
      Me.label9.TabIndex = 8
      Me.label9.Text = "View:"
      ' 
      ' comboBox4
      ' 
      Me.comboBox4.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox4.FormattingEnabled = True
      Me.comboBox4.Items.AddRange(New Object() {"Small Icon", "Large Icon", "List", "Tile", "Details"})
      Me.comboBox4.Location = New System.Drawing.Point(337, 456)
      Me.comboBox4.Name = "comboBox4"
      Me.comboBox4.Size = New System.Drawing.Size(121, 21)
      Me.comboBox4.TabIndex = 9
      '			Me.comboBox4.SelectedIndexChanged += New System.EventHandler(Me.ComboBox4SelectedIndexChanged);
      ' 
      ' checkBox10
      ' 
      Me.checkBox10.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox10.Checked = True
      Me.checkBox10.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBox10.Location = New System.Drawing.Point(218, 459)
      Me.checkBox10.Name = "checkBox10"
      Me.checkBox10.Size = New System.Drawing.Size(90, 19)
      Me.checkBox10.TabIndex = 7
      Me.checkBox10.Text = "Owner &Draw"
      Me.checkBox10.UseVisualStyleBackColor = True
      '			Me.checkBox10.CheckedChanged += New System.EventHandler(Me.CheckBox10CheckedChanged);
      ' 
      ' checkBox11
      ' 
      Me.checkBox11.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox11.Location = New System.Drawing.Point(101, 456)
      Me.checkBox11.Name = "checkBox11"
      Me.checkBox11.Size = New System.Drawing.Size(111, 24)
      Me.checkBox11.TabIndex = 6
      Me.checkBox11.Text = "Show Item &Count"
      Me.checkBox11.UseVisualStyleBackColor = True
      '			Me.checkBox11.CheckedChanged += New System.EventHandler(Me.CheckBox11CheckedChanged);
      ' 
      ' checkBox12
      ' 
      Me.checkBox12.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox12.Location = New System.Drawing.Point(6, 456)
      Me.checkBox12.Name = "checkBox12"
      Me.checkBox12.Size = New System.Drawing.Size(104, 24)
      Me.checkBox12.TabIndex = 5
      Me.checkBox12.Text = "Show &Groups"
      Me.checkBox12.UseVisualStyleBackColor = True
      '			Me.checkBox12.CheckedChanged += New System.EventHandler(Me.CheckBox12CheckedChanged);
      ' 
      ' label5
      ' 
      Me.label5.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label5.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label5.Location = New System.Drawing.Point(6, 6)
      Me.label5.Name = "label5"
      Me.label5.Size = New System.Drawing.Size(774, 46)
      Me.label5.TabIndex = 6
      Me.label5.Text = resources.GetString("label5.Text")
      ' 
      ' listViewFiles
      ' 
      Me.listViewFiles.AllColumns.Add(Me.olvColumnFileName)
      Me.listViewFiles.AllColumns.Add(Me.olvColumnFileCreated)
      Me.listViewFiles.AllColumns.Add(Me.olvColumnFileModified)
      Me.listViewFiles.AllColumns.Add(Me.olvColumnSize)
      Me.listViewFiles.AllColumns.Add(Me.olvColumnFileType)
      Me.listViewFiles.AllColumns.Add(Me.olvColumnAttributes)
      Me.listViewFiles.AllColumns.Add(Me.treeColumnFileExtension)
      Me.listViewFiles.AllowColumnReorder = True
      Me.listViewFiles.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.listViewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.olvColumnFileName, Me.olvColumnFileCreated, Me.olvColumnFileModified, Me.olvColumnSize, Me.olvColumnFileType, Me.olvColumnAttributes})
      Me.listViewFiles.EmptyListMsg = "This folder is completely empty!"
      Me.listViewFiles.EmptyListMsgFont = New System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.listViewFiles.HideSelection = False
      Me.listViewFiles.ItemRenderer = Nothing
      Me.listViewFiles.LargeImageList = Me.imageList2
      Me.listViewFiles.Location = New System.Drawing.Point(6, 83)
      Me.listViewFiles.Name = "listViewFiles"
      Me.listViewFiles.OwnerDraw = True
      Me.listViewFiles.RowHeight = 20
      Me.listViewFiles.ShowGroups = False
      Me.listViewFiles.Size = New System.Drawing.Size(774, 367)
      Me.listViewFiles.SmallImageList = Me.imageList1
      Me.listViewFiles.TabIndex = 13
      Me.listViewFiles.UseCompatibleStateImageBehavior = False
      Me.listViewFiles.View = System.Windows.Forms.View.Details
      '			Me.listViewFiles.ItemActivate += New System.EventHandler(Me.listViewFiles_ItemActivate);
      ' 
      ' olvColumnFileName
      ' 
      Me.olvColumnFileName.AspectName = "Name"
      Me.olvColumnFileName.IsTileViewColumn = True
      Me.olvColumnFileName.Text = "Name"
      Me.olvColumnFileName.UseInitialLetterForGroup = True
      Me.olvColumnFileName.Width = 180
      ' 
      ' olvColumnFileCreated
      ' 
      Me.olvColumnFileCreated.AspectName = "CreationTime"
      Me.olvColumnFileCreated.DisplayIndex = 4
      Me.olvColumnFileCreated.Text = "Created"
      Me.olvColumnFileCreated.Width = 131
      ' 
      ' olvColumnFileModified
      ' 
      Me.olvColumnFileModified.AspectName = "LastWriteTime"
      Me.olvColumnFileModified.DisplayIndex = 1
      Me.olvColumnFileModified.IsTileViewColumn = True
      Me.olvColumnFileModified.Text = "Modified"
      Me.olvColumnFileModified.Width = 145
      ' 
      ' olvColumnSize
      ' 
      Me.olvColumnSize.AspectName = "Extension"
      Me.olvColumnSize.DisplayIndex = 2
      Me.olvColumnSize.Text = "Size"
      Me.olvColumnSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumnSize.Width = 80
      ' 
      ' olvColumnFileType
      ' 
      Me.olvColumnFileType.DisplayIndex = 3
      Me.olvColumnFileType.IsTileViewColumn = True
      Me.olvColumnFileType.Text = "File Type"
      Me.olvColumnFileType.Width = 148
      ' 
      ' olvColumnAttributes
      ' 
      Me.olvColumnAttributes.FillsFreeSpace = True
      Me.olvColumnAttributes.IsEditable = False
      Me.olvColumnAttributes.MinimumWidth = 20
      Me.olvColumnAttributes.Text = "Attributes"
      ' 
      ' treeColumnFileExtension
      ' 
      Me.treeColumnFileExtension.AspectName = "Extension"
      Me.treeColumnFileExtension.IsVisible = False
      Me.treeColumnFileExtension.Text = "Extension"
      ' 
      ' tabPage6
      ' 
      Me.tabPage6.Controls.Add(Me.groupBox5)
      Me.tabPage6.Controls.Add(Me.groupBox4)
      Me.tabPage6.Controls.Add(Me.groupBox2)
      Me.tabPage6.Controls.Add(Me.label12)
      Me.tabPage6.Controls.Add(Me.button12)
      Me.tabPage6.Controls.Add(Me.button11)
      Me.tabPage6.Controls.Add(Me.button10)
      Me.tabPage6.Controls.Add(Me.printPreviewControl1)
      Me.tabPage6.Location = New System.Drawing.Point(4, 22)
      Me.tabPage6.Name = "tabPage6"
      Me.tabPage6.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage6.Size = New System.Drawing.Size(786, 485)
      Me.tabPage6.TabIndex = 5
      Me.tabPage6.Text = "Printing ListViews"
      Me.tabPage6.UseVisualStyleBackColor = True
      ' 
      ' groupBox5
      ' 
      Me.groupBox5.Controls.Add(Me.numericUpDown2)
      Me.groupBox5.Controls.Add(Me.label19)
      Me.groupBox5.Controls.Add(Me.numericUpDown1)
      Me.groupBox5.Controls.Add(Me.label18)
      Me.groupBox5.Controls.Add(Me.cbCellGridLines)
      Me.groupBox5.Controls.Add(Me.label17)
      Me.groupBox5.Controls.Add(Me.rbStyleTooMuch)
      Me.groupBox5.Controls.Add(Me.cbPrintOnlySelection)
      Me.groupBox5.Controls.Add(Me.rbStyleModern)
      Me.groupBox5.Controls.Add(Me.cbShrinkToFit)
      Me.groupBox5.Controls.Add(Me.rbStyleMinimal)
      Me.groupBox5.Controls.Add(Me.cbIncludeImages)
      Me.groupBox5.Location = New System.Drawing.Point(144, 39)
      Me.groupBox5.Name = "groupBox5"
      Me.groupBox5.Size = New System.Drawing.Size(301, 109)
      Me.groupBox5.TabIndex = 1
      Me.groupBox5.TabStop = False
      Me.groupBox5.Text = "Format"
      ' 
      ' numericUpDown2
      ' 
      Me.numericUpDown2.Location = New System.Drawing.Point(200, 84)
      Me.numericUpDown2.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
      Me.numericUpDown2.Name = "numericUpDown2"
      Me.numericUpDown2.Size = New System.Drawing.Size(46, 20)
      Me.numericUpDown2.TabIndex = 11
      Me.numericUpDown2.Value = New Decimal(New Integer() {10, 0, 0, 0})
      '			Me.numericUpDown2.ValueChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' label19
      ' 
      Me.label19.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label19.Location = New System.Drawing.Point(163, 85)
      Me.label19.Name = "label19"
      Me.label19.Size = New System.Drawing.Size(28, 19)
      Me.label19.TabIndex = 10
      Me.label19.Text = "to:"
      ' 
      ' numericUpDown1
      ' 
      Me.numericUpDown1.Location = New System.Drawing.Point(99, 83)
      Me.numericUpDown1.Name = "numericUpDown1"
      Me.numericUpDown1.Size = New System.Drawing.Size(46, 20)
      Me.numericUpDown1.TabIndex = 9
      Me.numericUpDown1.Value = New Decimal(New Integer() {1, 0, 0, 0})
      '			Me.numericUpDown1.ValueChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' label18
      ' 
      Me.label18.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label18.Location = New System.Drawing.Point(18, 86)
      Me.label18.Name = "label18"
      Me.label18.Size = New System.Drawing.Size(74, 19)
      Me.label18.TabIndex = 8
      Me.label18.Text = "Page range:"
      ' 
      ' cbCellGridLines
      ' 
      Me.cbCellGridLines.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.cbCellGridLines.AutoSize = True
      Me.cbCellGridLines.Checked = True
      Me.cbCellGridLines.CheckState = System.Windows.Forms.CheckState.Checked
      Me.cbCellGridLines.Location = New System.Drawing.Point(200, 62)
      Me.cbCellGridLines.Name = "cbCellGridLines"
      Me.cbCellGridLines.Size = New System.Drawing.Size(87, 17)
      Me.cbCellGridLines.TabIndex = 7
      Me.cbCellGridLines.Text = "Cell grid lines"
      Me.cbCellGridLines.UseVisualStyleBackColor = True
      '			Me.cbCellGridLines.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' label17
      ' 
      Me.label17.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label17.Location = New System.Drawing.Point(19, 19)
      Me.label17.Name = "label17"
      Me.label17.Size = New System.Drawing.Size(41, 19)
      Me.label17.TabIndex = 0
      Me.label17.Text = "Style:"
      ' 
      ' rbStyleTooMuch
      ' 
      Me.rbStyleTooMuch.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.rbStyleTooMuch.Location = New System.Drawing.Point(200, 15)
      Me.rbStyleTooMuch.Name = "rbStyleTooMuch"
      Me.rbStyleTooMuch.Size = New System.Drawing.Size(78, 24)
      Me.rbStyleTooMuch.TabIndex = 3
      Me.rbStyleTooMuch.Text = "Too much!"
      Me.rbStyleTooMuch.UseVisualStyleBackColor = True
      '			Me.rbStyleTooMuch.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' cbPrintOnlySelection
      ' 
      Me.cbPrintOnlySelection.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.cbPrintOnlySelection.AutoSize = True
      Me.cbPrintOnlySelection.Location = New System.Drawing.Point(21, 62)
      Me.cbPrintOnlySelection.Name = "cbPrintOnlySelection"
      Me.cbPrintOnlySelection.Size = New System.Drawing.Size(146, 17)
      Me.cbPrintOnlySelection.TabIndex = 6
      Me.cbPrintOnlySelection.Text = "Print Only Selected Rows"
      Me.cbPrintOnlySelection.UseVisualStyleBackColor = True
      '			Me.cbPrintOnlySelection.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' rbStyleModern
      ' 
      Me.rbStyleModern.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.rbStyleModern.Checked = True
      Me.rbStyleModern.Location = New System.Drawing.Point(133, 15)
      Me.rbStyleModern.Name = "rbStyleModern"
      Me.rbStyleModern.Size = New System.Drawing.Size(61, 24)
      Me.rbStyleModern.TabIndex = 2
      Me.rbStyleModern.TabStop = True
      Me.rbStyleModern.Text = "Modern"
      Me.rbStyleModern.UseVisualStyleBackColor = True
      '			Me.rbStyleModern.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' cbShrinkToFit
      ' 
      Me.cbShrinkToFit.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.cbShrinkToFit.AutoSize = True
      Me.cbShrinkToFit.Location = New System.Drawing.Point(200, 41)
      Me.cbShrinkToFit.Name = "cbShrinkToFit"
      Me.cbShrinkToFit.Size = New System.Drawing.Size(86, 17)
      Me.cbShrinkToFit.TabIndex = 5
      Me.cbShrinkToFit.Text = "Shrink To Fit"
      Me.cbShrinkToFit.UseVisualStyleBackColor = True
      '			Me.cbShrinkToFit.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' rbStyleMinimal
      ' 
      Me.rbStyleMinimal.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.rbStyleMinimal.Location = New System.Drawing.Point(66, 15)
      Me.rbStyleMinimal.Name = "rbStyleMinimal"
      Me.rbStyleMinimal.Size = New System.Drawing.Size(61, 24)
      Me.rbStyleMinimal.TabIndex = 1
      Me.rbStyleMinimal.Text = "Minimal"
      Me.rbStyleMinimal.UseVisualStyleBackColor = True
      '			Me.rbStyleMinimal.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' cbIncludeImages
      ' 
      Me.cbIncludeImages.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.cbIncludeImages.AutoSize = True
      Me.cbIncludeImages.Checked = True
      Me.cbIncludeImages.CheckState = System.Windows.Forms.CheckState.Checked
      Me.cbIncludeImages.Location = New System.Drawing.Point(22, 41)
      Me.cbIncludeImages.Name = "cbIncludeImages"
      Me.cbIncludeImages.Size = New System.Drawing.Size(145, 17)
      Me.cbIncludeImages.TabIndex = 4
      Me.cbIncludeImages.Text = "Include Images In Report"
      Me.cbIncludeImages.UseVisualStyleBackColor = True
      '			Me.cbIncludeImages.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' groupBox4
      ' 
      Me.groupBox4.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.groupBox4.Controls.Add(Me.tbFooter)
      Me.groupBox4.Controls.Add(Me.label15)
      Me.groupBox4.Controls.Add(Me.tbHeader)
      Me.groupBox4.Controls.Add(Me.label16)
      Me.groupBox4.Controls.Add(Me.tbWatermark)
      Me.groupBox4.Controls.Add(Me.label14)
      Me.groupBox4.Controls.Add(Me.tbTitle)
      Me.groupBox4.Controls.Add(Me.label13)
      Me.groupBox4.Location = New System.Drawing.Point(451, 39)
      Me.groupBox4.Name = "groupBox4"
      Me.groupBox4.Size = New System.Drawing.Size(329, 109)
      Me.groupBox4.TabIndex = 2
      Me.groupBox4.TabStop = False
      Me.groupBox4.Text = "Print Settings"
      ' 
      ' tbFooter
      ' 
      Me.tbFooter.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.tbFooter.Location = New System.Drawing.Point(73, 61)
      Me.tbFooter.Name = "tbFooter"
      Me.tbFooter.Size = New System.Drawing.Size(250, 20)
      Me.tbFooter.TabIndex = 5
      Me.tbFooter.Text = "{1:F}\t\tPage: {0}"
      ' 
      ' label15
      ' 
      Me.label15.Location = New System.Drawing.Point(6, 64)
      Me.label15.Name = "label15"
      Me.label15.Size = New System.Drawing.Size(70, 19)
      Me.label15.TabIndex = 4
      Me.label15.Text = "Footer:"
      ' 
      ' tbHeader
      ' 
      Me.tbHeader.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.tbHeader.Location = New System.Drawing.Point(73, 39)
      Me.tbHeader.Name = "tbHeader"
      Me.tbHeader.Size = New System.Drawing.Size(250, 20)
      Me.tbHeader.TabIndex = 3
      Me.tbHeader.Text = "Easy Printing ListView"
      ' 
      ' label16
      ' 
      Me.label16.Location = New System.Drawing.Point(7, 41)
      Me.label16.Name = "label16"
      Me.label16.Size = New System.Drawing.Size(54, 19)
      Me.label16.TabIndex = 2
      Me.label16.Text = "Header:"
      ' 
      ' tbWatermark
      ' 
      Me.tbWatermark.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.tbWatermark.Location = New System.Drawing.Point(73, 83)
      Me.tbWatermark.Name = "tbWatermark"
      Me.tbWatermark.Size = New System.Drawing.Size(250, 20)
      Me.tbWatermark.TabIndex = 7
      Me.tbWatermark.Text = "SLOTHFUL!"
      ' 
      ' label14
      ' 
      Me.label14.Location = New System.Drawing.Point(6, 86)
      Me.label14.Name = "label14"
      Me.label14.Size = New System.Drawing.Size(70, 19)
      Me.label14.TabIndex = 6
      Me.label14.Text = "Watermark:"
      ' 
      ' tbTitle
      ' 
      Me.tbTitle.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.tbTitle.Location = New System.Drawing.Point(73, 17)
      Me.tbTitle.Name = "tbTitle"
      Me.tbTitle.Size = New System.Drawing.Size(250, 20)
      Me.tbTitle.TabIndex = 1
      Me.tbTitle.Text = "List View printer demo"
      ' 
      ' label13
      ' 
      Me.label13.Location = New System.Drawing.Point(7, 19)
      Me.label13.Name = "label13"
      Me.label13.Size = New System.Drawing.Size(54, 19)
      Me.label13.TabIndex = 0
      Me.label13.Text = "Job title:"
      ' 
      ' groupBox2
      ' 
      Me.groupBox2.Controls.Add(Me.rbShowVirtual)
      Me.groupBox2.Controls.Add(Me.rbShowFileExplorer)
      Me.groupBox2.Controls.Add(Me.rbShowDataset)
      Me.groupBox2.Controls.Add(Me.rbShowComplex)
      Me.groupBox2.Controls.Add(Me.rbShowSimple)
      Me.groupBox2.Location = New System.Drawing.Point(7, 40)
      Me.groupBox2.Name = "groupBox2"
      Me.groupBox2.Size = New System.Drawing.Size(131, 108)
      Me.groupBox2.TabIndex = 0
      Me.groupBox2.TabStop = False
      Me.groupBox2.Text = "List view to print"
      ' 
      ' rbShowVirtual
      ' 
      Me.rbShowVirtual.Location = New System.Drawing.Point(6, 67)
      Me.rbShowVirtual.Name = "rbShowVirtual"
      Me.rbShowVirtual.Size = New System.Drawing.Size(104, 23)
      Me.rbShowVirtual.TabIndex = 3
      Me.rbShowVirtual.Text = "Virtual list"
      Me.rbShowVirtual.UseVisualStyleBackColor = True
      ' 
      ' rbShowFileExplorer
      ' 
      Me.rbShowFileExplorer.Location = New System.Drawing.Point(6, 86)
      Me.rbShowFileExplorer.Name = "rbShowFileExplorer"
      Me.rbShowFileExplorer.Size = New System.Drawing.Size(104, 23)
      Me.rbShowFileExplorer.TabIndex = 4
      Me.rbShowFileExplorer.Text = "File explorer list"
      Me.rbShowFileExplorer.UseVisualStyleBackColor = True
      '			Me.rbShowFileExplorer.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' rbShowDataset
      ' 
      Me.rbShowDataset.Location = New System.Drawing.Point(6, 48)
      Me.rbShowDataset.Name = "rbShowDataset"
      Me.rbShowDataset.Size = New System.Drawing.Size(104, 23)
      Me.rbShowDataset.TabIndex = 2
      Me.rbShowDataset.Text = "Dataset list"
      Me.rbShowDataset.UseVisualStyleBackColor = True
      '			Me.rbShowDataset.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' rbShowComplex
      ' 
      Me.rbShowComplex.Checked = True
      Me.rbShowComplex.Location = New System.Drawing.Point(6, 29)
      Me.rbShowComplex.Name = "rbShowComplex"
      Me.rbShowComplex.Size = New System.Drawing.Size(104, 23)
      Me.rbShowComplex.TabIndex = 1
      Me.rbShowComplex.TabStop = True
      Me.rbShowComplex.Text = "Complex list"
      Me.rbShowComplex.UseVisualStyleBackColor = True
      '			Me.rbShowComplex.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' rbShowSimple
      ' 
      Me.rbShowSimple.Location = New System.Drawing.Point(6, 10)
      Me.rbShowSimple.Name = "rbShowSimple"
      Me.rbShowSimple.Size = New System.Drawing.Size(104, 23)
      Me.rbShowSimple.TabIndex = 0
      Me.rbShowSimple.Text = "Simple list"
      Me.rbShowSimple.UseVisualStyleBackColor = True
      '			Me.rbShowSimple.CheckedChanged += New System.EventHandler(Me.UpdatePreview);
      ' 
      ' label12
      ' 
      Me.label12.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label12.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label12.Location = New System.Drawing.Point(6, 6)
      Me.label12.Name = "label12"
      Me.label12.Size = New System.Drawing.Size(774, 30)
      Me.label12.TabIndex = 12
      Me.label12.Text = resources.GetString("label12.Text")
      ' 
      ' button12
      ' 
      Me.button12.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button12.Location = New System.Drawing.Point(685, 224)
      Me.button12.Name = "button12"
      Me.button12.Size = New System.Drawing.Size(93, 23)
      Me.button12.TabIndex = 5
      Me.button12.Text = "Print..."
      Me.button12.UseVisualStyleBackColor = True
      '			Me.button12.Click += New System.EventHandler(Me.button12_Click);
      ' 
      ' button11
      ' 
      Me.button11.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button11.Location = New System.Drawing.Point(685, 195)
      Me.button11.Name = "button11"
      Me.button11.Size = New System.Drawing.Size(93, 23)
      Me.button11.TabIndex = 4
      Me.button11.Text = "Print Preview..."
      Me.button11.UseVisualStyleBackColor = True
      '			Me.button11.Click += New System.EventHandler(Me.button11_Click);
      ' 
      ' button10
      ' 
      Me.button10.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button10.Location = New System.Drawing.Point(685, 166)
      Me.button10.Name = "button10"
      Me.button10.Size = New System.Drawing.Size(95, 23)
      Me.button10.TabIndex = 3
      Me.button10.Text = "Page Setup..."
      Me.button10.UseVisualStyleBackColor = True
      '			Me.button10.Click += New System.EventHandler(Me.button10_Click_1);
      ' 
      ' printPreviewControl1
      ' 
      Me.printPreviewControl1.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.printPreviewControl1.AutoZoom = False
      Me.printPreviewControl1.Columns = 2
      Me.printPreviewControl1.Document = Me.listViewPrinter1
      Me.printPreviewControl1.Location = New System.Drawing.Point(7, 154)
      Me.printPreviewControl1.Name = "printPreviewControl1"
      Me.printPreviewControl1.Size = New System.Drawing.Size(664, 310)
      Me.printPreviewControl1.TabIndex = 6
      Me.printPreviewControl1.UseAntiAlias = True
      Me.printPreviewControl1.Zoom = 0.25834046193327631
      ' 
      ' listViewPrinter1
      ' 
      ' 
      ' 
      ' 
      Me.listViewPrinter1.CellFormat.CanWrap = True
      Me.listViewPrinter1.CellFormat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.listViewPrinter1.Footer = "This is the footers"
      ' 
      ' 
      ' 
      Me.listViewPrinter1.FooterFormat.Font = New System.Drawing.Font("Verdana", 10.0F, System.Drawing.FontStyle.Italic)
      ' 
      ' 
      ' 
      Me.listViewPrinter1.GroupHeaderFormat.Font = New System.Drawing.Font("Verdana", 10.0F, System.Drawing.FontStyle.Bold)
      Me.listViewPrinter1.Header = "This is the header" & Constants.vbTab + Constants.vbTab & "Right"
      ' 
      ' 
      ' 
      Me.listViewPrinter1.HeaderFormat.Font = New System.Drawing.Font("Verdana", 24.0F)
      Me.listViewPrinter1.IsListHeaderOnEachPage = False
      ' 
      ' 
      ' 
      Me.listViewPrinter1.ListHeaderFormat.CanWrap = True
      Me.listViewPrinter1.ListHeaderFormat.Font = New System.Drawing.Font("Verdana", 12.0F)
      Me.listViewPrinter1.ListView = Me.listViewComplex
      Me.listViewPrinter1.Watermark = "TOP SECRET!"
      Me.listViewPrinter1.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 36.0F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      '			Me.listViewPrinter1.PrintPage += New System.Drawing.Printing.PrintPageEventHandler(Me.listViewPrinter1_PrintPage);
      '			Me.listViewPrinter1.EndPrint += New System.Drawing.Printing.PrintEventHandler(Me.listViewPrinter1_EndPrint);
      ' 
      ' tabPage7
      ' 
      Me.tabPage7.Controls.Add(Me.button19)
      Me.tabPage7.Controls.Add(Me.button18)
      Me.tabPage7.Controls.Add(Me.label26)
      Me.tabPage7.Controls.Add(Me.comboBox9)
      Me.tabPage7.Controls.Add(Me.label24)
      Me.tabPage7.Controls.Add(Me.label25)
      Me.tabPage7.Controls.Add(Me.comboBox10)
      Me.tabPage7.Controls.Add(Me.checkBox13)
      Me.tabPage7.Controls.Add(Me.button15)
      Me.tabPage7.Controls.Add(Me.button14)
      Me.tabPage7.Controls.Add(Me.olvFastList)
      Me.tabPage7.Location = New System.Drawing.Point(4, 22)
      Me.tabPage7.Name = "tabPage7"
      Me.tabPage7.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage7.Size = New System.Drawing.Size(786, 485)
      Me.tabPage7.TabIndex = 6
      Me.tabPage7.Text = "Fast List"
      Me.tabPage7.UseVisualStyleBackColor = True
      ' 
      ' button19
      ' 
      Me.button19.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button19.Location = New System.Drawing.Point(426, 454)
      Me.button19.Name = "button19"
      Me.button19.Size = New System.Drawing.Size(90, 23)
      Me.button19.TabIndex = 17
      Me.button19.Text = "&Copy Checked"
      Me.button19.UseVisualStyleBackColor = True
      '			Me.button19.Click += New System.EventHandler(Me.button19_Click);
      ' 
      ' button18
      ' 
      Me.button18.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button18.Location = New System.Drawing.Point(597, 454)
      Me.button18.Name = "button18"
      Me.button18.Size = New System.Drawing.Size(101, 23)
      Me.button18.TabIndex = 7
      Me.button18.Text = "Remove Selected"
      Me.button18.UseVisualStyleBackColor = True
      '			Me.button18.Click += New System.EventHandler(Me.Button18Click);
      ' 
      ' label26
      ' 
      Me.label26.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label26.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label26.Location = New System.Drawing.Point(6, 5)
      Me.label26.Name = "label26"
      Me.label26.Size = New System.Drawing.Size(774, 38)
      Me.label26.TabIndex = 16
      Me.label26.Text = resources.GetString("label26.Text")
      ' 
      ' comboBox9
      ' 
      Me.comboBox9.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.comboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox9.FormattingEnabled = True
      Me.comboBox9.Items.AddRange(New Object() {"No", "Single Click", "Double Click", "F2 Only"})
      Me.comboBox9.Location = New System.Drawing.Point(215, 456)
      Me.comboBox9.Name = "comboBox9"
      Me.comboBox9.Size = New System.Drawing.Size(83, 21)
      Me.comboBox9.TabIndex = 3
      '			Me.comboBox9.SelectedIndexChanged += New System.EventHandler(Me.comboBox9_SelectedIndexChanged);
      ' 
      ' label24
      ' 
      Me.label24.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label24.AutoSize = True
      Me.label24.Location = New System.Drawing.Point(166, 460)
      Me.label24.Name = "label24"
      Me.label24.Size = New System.Drawing.Size(48, 13)
      Me.label24.TabIndex = 2
      Me.label24.Text = "Editable:"
      ' 
      ' label25
      ' 
      Me.label25.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label25.AutoSize = True
      Me.label25.Location = New System.Drawing.Point(303, 460)
      Me.label25.Name = "label25"
      Me.label25.Size = New System.Drawing.Size(33, 13)
      Me.label25.TabIndex = 4
      Me.label25.Text = "View:"
      ' 
      ' comboBox10
      ' 
      Me.comboBox10.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.comboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox10.FormattingEnabled = True
      Me.comboBox10.Items.AddRange(New Object() {"Small Icon", "Large Icon", "List", "Tile", "Details"})
      Me.comboBox10.Location = New System.Drawing.Point(337, 456)
      Me.comboBox10.Name = "comboBox10"
      Me.comboBox10.Size = New System.Drawing.Size(83, 21)
      Me.comboBox10.TabIndex = 5
      '			Me.comboBox10.SelectedIndexChanged += New System.EventHandler(Me.comboBox10_SelectedIndexChanged);
      ' 
      ' checkBox13
      ' 
      Me.checkBox13.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox13.Checked = True
      Me.checkBox13.CheckState = System.Windows.Forms.CheckState.Checked
      Me.checkBox13.Location = New System.Drawing.Point(6, 456)
      Me.checkBox13.Name = "checkBox13"
      Me.checkBox13.Size = New System.Drawing.Size(113, 21)
      Me.checkBox13.TabIndex = 1
      Me.checkBox13.Text = "Owner &Draw"
      Me.checkBox13.UseVisualStyleBackColor = True
      '			Me.checkBox13.CheckedChanged += New System.EventHandler(Me.checkBox13_CheckedChanged);
      ' 
      ' button15
      ' 
      Me.button15.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button15.Location = New System.Drawing.Point(522, 454)
      Me.button15.Name = "button15"
      Me.button15.Size = New System.Drawing.Size(69, 23)
      Me.button15.TabIndex = 6
      Me.button15.Text = "&Clear List"
      Me.button15.UseVisualStyleBackColor = True
      '			Me.button15.Click += New System.EventHandler(Me.button15_Click);
      ' 
      ' button14
      ' 
      Me.button14.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button14.Location = New System.Drawing.Point(704, 454)
      Me.button14.Name = "button14"
      Me.button14.Size = New System.Drawing.Size(77, 23)
      Me.button14.TabIndex = 8
      Me.button14.Text = "&Add 1000"
      Me.button14.UseVisualStyleBackColor = True
      '			Me.button14.Click += New System.EventHandler(Me.button14_Click);
      ' 
      ' olvFastList
      ' 
      Me.olvFastList.AllColumns.Add(Me.olvColumn18)
      Me.olvFastList.AllColumns.Add(Me.olvColumn19)
      Me.olvFastList.AllColumns.Add(Me.olvColumn26)
      Me.olvFastList.AllColumns.Add(Me.olvColumn27)
      Me.olvFastList.AllColumns.Add(Me.olvColumn28)
      Me.olvFastList.AllColumns.Add(Me.olvColumn29)
      Me.olvFastList.AllColumns.Add(Me.olvColumn31)
      Me.olvFastList.AllColumns.Add(Me.olvColumn32)
      Me.olvFastList.AllColumns.Add(Me.olvColumn33)
      Me.olvFastList.AllowColumnReorder = True
      Me.olvFastList.AlternateRowBackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(192))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(192))))))
      Me.olvFastList.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.olvFastList.BackgroundImageTiled = True
      Me.olvFastList.CheckBoxes = True
      Me.olvFastList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.olvColumn18, Me.olvColumn19, Me.olvColumn26, Me.olvColumn27, Me.olvColumn28, Me.olvColumn29, Me.olvColumn31, Me.olvColumn32, Me.olvColumn33})
      Me.olvFastList.EmptyListMsg = "This fast list is empty"
      Me.olvFastList.FullRowSelect = True
      Me.olvFastList.GridLines = True
      Me.olvFastList.HideSelection = False
      Me.olvFastList.ItemRenderer = Nothing
      Me.olvFastList.LargeImageList = Me.imageList2
      Me.olvFastList.Location = New System.Drawing.Point(6, 47)
      Me.olvFastList.Name = "olvFastList"
      Me.olvFastList.OwnerDraw = True
      Me.olvFastList.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.olvFastList.ShowGroups = False
      Me.olvFastList.ShowImagesOnSubItems = True
      Me.olvFastList.ShowItemToolTips = True
      Me.olvFastList.Size = New System.Drawing.Size(774, 401)
      Me.olvFastList.SmallImageList = Me.imageList1
      Me.olvFastList.TabIndex = 0
      Me.olvFastList.UseAlternatingBackColors = True
      Me.olvFastList.UseCompatibleStateImageBehavior = False
      Me.olvFastList.UseHotItem = True
      Me.olvFastList.View = System.Windows.Forms.View.Details
      Me.olvFastList.VirtualMode = True
      '			Me.olvFastList.ItemCheck += New System.Windows.Forms.ItemCheckEventHandler(Me.olvFastList_ItemCheck);
      '			Me.olvFastList.SelectionChanged += New System.EventHandler(Me.olvFastList_SelectionChanged);
      '			Me.olvFastList.ItemChecked += New System.Windows.Forms.ItemCheckedEventHandler(Me.olvFastList_ItemChecked);
      ' 
      ' olvColumn18
      ' 
      Me.olvColumn18.AspectName = "Name"
      Me.olvColumn18.Text = "Person"
      Me.olvColumn18.UseInitialLetterForGroup = True
      Me.olvColumn18.Width = 114
      ' 
      ' olvColumn19
      ' 
      Me.olvColumn19.AspectName = "Occupation"
      Me.olvColumn19.IsTileViewColumn = True
      Me.olvColumn19.Text = "Occupation"
      Me.olvColumn19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn19.Width = 92
      ' 
      ' olvColumn26
      ' 
      Me.olvColumn26.AspectName = "CulinaryRating"
      Me.olvColumn26.GroupWithItemCountFormat = "{0} ({1} candidates)"
      Me.olvColumn26.GroupWithItemCountSingularFormat = "{0} (only {1} candidate)"
      Me.olvColumn26.Text = "Cooking skill"
      Me.olvColumn26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn26.Width = 75
      ' 
      ' olvColumn27
      ' 
      Me.olvColumn27.AspectName = "YearOfBirth"
      Me.olvColumn27.Text = "Year Of Birth"
      Me.olvColumn27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn27.Width = 80
      ' 
      ' olvColumn28
      ' 
      Me.olvColumn28.AspectName = "BirthDate"
      Me.olvColumn28.AspectToStringFormat = "{0:D}"
      Me.olvColumn28.FillsFreeSpace = True
      Me.olvColumn28.GroupWithItemCountFormat = "{0} has {1} birthdays"
      Me.olvColumn28.GroupWithItemCountSingularFormat = "{0} has only {1} birthday"
      Me.olvColumn28.IsTileViewColumn = True
      Me.olvColumn28.Text = "Birthday"
      Me.olvColumn28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn28.Width = 111
      ' 
      ' olvColumn29
      ' 
      Me.olvColumn29.AspectName = "GetRate"
      Me.olvColumn29.AspectToStringFormat = "{0:C}"
      Me.olvColumn29.IsTileViewColumn = True
      Me.olvColumn29.Text = "Hourly Rate"
      Me.olvColumn29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn29.Width = 71
      ' 
      ' olvColumn31
      ' 
      Me.olvColumn31.IsEditable = False
      Me.olvColumn31.Text = "Salary"
      Me.olvColumn31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn31.Width = 55
      ' 
      ' olvColumn32
      ' 
      Me.olvColumn32.IsEditable = False
      Me.olvColumn32.Text = "Days Since Birth"
      Me.olvColumn32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn32.Width = 81
      ' 
      ' olvColumn33
      ' 
      Me.olvColumn33.AspectName = "CanTellJokes"
      Me.olvColumn33.CheckBoxes = True
      Me.olvColumn33.Text = "Tells Jokes?"
      Me.olvColumn33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      Me.olvColumn33.Width = 74
      ' 
      ' tabPage9
      ' 
      Me.tabPage9.Controls.Add(Me.button25)
      Me.tabPage9.Controls.Add(Me.button26)
      Me.tabPage9.Controls.Add(Me.button27)
      Me.tabPage9.Controls.Add(Me.label32)
      Me.tabPage9.Controls.Add(Me.treeListView)
      Me.tabPage9.Location = New System.Drawing.Point(4, 22)
      Me.tabPage9.Name = "tabPage9"
      Me.tabPage9.Padding = New System.Windows.Forms.Padding(3)
      Me.tabPage9.Size = New System.Drawing.Size(786, 485)
      Me.tabPage9.TabIndex = 8
      Me.tabPage9.Text = "TreeListView"
      Me.tabPage9.UseVisualStyleBackColor = True
      ' 
      ' button25
      ' 
      Me.button25.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button25.Location = New System.Drawing.Point(483, 456)
      Me.button25.Name = "button25"
      Me.button25.Size = New System.Drawing.Size(87, 23)
      Me.button25.TabIndex = 10
      Me.button25.Text = "Save State"
      Me.button25.UseVisualStyleBackColor = True
      '			Me.button25.Click += New System.EventHandler(Me.button25_Click);
      ' 
      ' button26
      ' 
      Me.button26.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button26.Enabled = False
      Me.button26.Location = New System.Drawing.Point(576, 456)
      Me.button26.Name = "button26"
      Me.button26.Size = New System.Drawing.Size(83, 23)
      Me.button26.TabIndex = 11
      Me.button26.Text = "Restore State"
      Me.button26.UseVisualStyleBackColor = True
      '			Me.button26.Click += New System.EventHandler(Me.button26_Click);
      ' 
      ' button27
      ' 
      Me.button27.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button27.Location = New System.Drawing.Point(665, 456)
      Me.button27.Name = "button27"
      Me.button27.Size = New System.Drawing.Size(115, 23)
      Me.button27.TabIndex = 12
      Me.button27.Text = "&Choose Columns..."
      Me.button27.UseVisualStyleBackColor = True
      '			Me.button27.Click += New System.EventHandler(Me.button27_Click);
      ' 
      ' label32
      ' 
      Me.label32.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label32.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label32.Location = New System.Drawing.Point(6, 6)
      Me.label32.Name = "label32"
      Me.label32.Size = New System.Drawing.Size(774, 46)
      Me.label32.TabIndex = 6
      Me.label32.Text = "This is like the File Explorer tab, except that it shows the directory structure," & " rooted on the available disks."
      ' 
      ' treeListView
      ' 
      Me.treeListView.AllColumns.Add(Me.treeColumnName)
      Me.treeListView.AllColumns.Add(Me.treeColumnCreated)
      Me.treeListView.AllColumns.Add(Me.treeColumnModified)
      Me.treeListView.AllColumns.Add(Me.treeColumnSize)
      Me.treeListView.AllColumns.Add(Me.treeColumnFileType)
      Me.treeListView.AllColumns.Add(Me.treeColumnAttributes)
      Me.treeListView.AllColumns.Add(Me.treeColumnFileExtension)
      Me.treeListView.AllowColumnReorder = True
      Me.treeListView.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.treeListView.CheckBoxes = True
      Me.treeListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.treeColumnName, Me.treeColumnCreated, Me.treeColumnModified, Me.treeColumnSize, Me.treeColumnFileType, Me.treeColumnAttributes})
      Me.treeListView.EmptyListMsg = "This folder is completely empty!"
      Me.treeListView.EmptyListMsgFont = New System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.treeListView.HideSelection = False
      Me.treeListView.ItemRenderer = Nothing
      Me.treeListView.Location = New System.Drawing.Point(6, 55)
      Me.treeListView.Name = "treeListView"
      Me.treeListView.OwnerDraw = True
      Me.treeListView.ShowGroups = False
      Me.treeListView.Size = New System.Drawing.Size(774, 395)
      Me.treeListView.TabIndex = 13
      Me.treeListView.UseCompatibleStateImageBehavior = False
      Me.treeListView.UseHotItem = True
      Me.treeListView.View = System.Windows.Forms.View.Details
      Me.treeListView.VirtualMode = True
      '			Me.treeListView.ItemCheck += New System.Windows.Forms.ItemCheckEventHandler(Me.treeListView_ItemCheck);
      '			Me.treeListView.ItemActivate += New System.EventHandler(Me.treeListView_ItemActivate);
      '			Me.treeListView.ItemChecked += New System.Windows.Forms.ItemCheckedEventHandler(Me.treeListView_ItemChecked);
      ' 
      ' treeColumnName
      ' 
      Me.treeColumnName.AspectName = "Name"
      Me.treeColumnName.IsTileViewColumn = True
      Me.treeColumnName.Text = "Name"
      Me.treeColumnName.UseInitialLetterForGroup = True
      Me.treeColumnName.Width = 180
      ' 
      ' treeColumnCreated
      ' 
      Me.treeColumnCreated.AspectName = "CreationTime"
      Me.treeColumnCreated.DisplayIndex = 4
      Me.treeColumnCreated.Text = "Created"
      Me.treeColumnCreated.Width = 131
      ' 
      ' treeColumnModified
      ' 
      Me.treeColumnModified.AspectName = "LastWriteTime"
      Me.treeColumnModified.DisplayIndex = 1
      Me.treeColumnModified.IsTileViewColumn = True
      Me.treeColumnModified.Text = "Modified"
      Me.treeColumnModified.Width = 145
      ' 
      ' treeColumnSize
      ' 
      Me.treeColumnSize.AspectName = "Extension"
      Me.treeColumnSize.DisplayIndex = 2
      Me.treeColumnSize.Text = "Size"
      Me.treeColumnSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.treeColumnSize.Width = 80
      ' 
      ' treeColumnFileType
      ' 
      Me.treeColumnFileType.DisplayIndex = 3
      Me.treeColumnFileType.IsTileViewColumn = True
      Me.treeColumnFileType.Text = "File Type"
      Me.treeColumnFileType.Width = 148
      ' 
      ' treeColumnAttributes
      ' 
      Me.treeColumnAttributes.FillsFreeSpace = True
      Me.treeColumnAttributes.IsEditable = False
      Me.treeColumnAttributes.MinimumWidth = 20
      Me.treeColumnAttributes.Text = "Attributes"
      ' 
      ' statusStrip1
      ' 
      Me.statusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripStatusLabel1})
      Me.statusStrip1.Location = New System.Drawing.Point(0, 526)
      Me.statusStrip1.Name = "statusStrip1"
      Me.statusStrip1.Size = New System.Drawing.Size(819, 22)
      Me.statusStrip1.TabIndex = 3
      Me.statusStrip1.Text = "statusStrip1"
      ' 
      ' toolStripStatusLabel1
      ' 
      Me.toolStripStatusLabel1.Name = "toolStripStatusLabel1"
      Me.toolStripStatusLabel1.Size = New System.Drawing.Size(0, 17)
      ' 
      ' button20
      ' 
      Me.button20.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button20.Location = New System.Drawing.Point(483, 456)
      Me.button20.Name = "button20"
      Me.button20.Size = New System.Drawing.Size(87, 23)
      Me.button20.TabIndex = 10
      Me.button20.Text = "Save State"
      Me.button20.UseVisualStyleBackColor = True
      ' 
      ' button21
      ' 
      Me.button21.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button21.Enabled = False
      Me.button21.Location = New System.Drawing.Point(576, 456)
      Me.button21.Name = "button21"
      Me.button21.Size = New System.Drawing.Size(83, 23)
      Me.button21.TabIndex = 11
      Me.button21.Text = "Restore State"
      Me.button21.UseVisualStyleBackColor = True
      ' 
      ' button22
      ' 
      Me.button22.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button22.Location = New System.Drawing.Point(665, 456)
      Me.button22.Name = "button22"
      Me.button22.Size = New System.Drawing.Size(115, 23)
      Me.button22.TabIndex = 12
      Me.button22.Text = "&Choose Columns..."
      Me.button22.UseVisualStyleBackColor = True
      ' 
      ' button23
      ' 
      Me.button23.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button23.Location = New System.Drawing.Point(705, 55)
      Me.button23.Name = "button23"
      Me.button23.Size = New System.Drawing.Size(75, 23)
      Me.button23.TabIndex = 3
      Me.button23.Text = "&Up"
      Me.button23.UseVisualStyleBackColor = True
      ' 
      ' button24
      ' 
      Me.button24.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.button24.Location = New System.Drawing.Point(624, 55)
      Me.button24.Name = "button24"
      Me.button24.Size = New System.Drawing.Size(75, 23)
      Me.button24.TabIndex = 2
      Me.button24.Text = "&Go"
      Me.button24.UseVisualStyleBackColor = True
      ' 
      ' textBox1
      ' 
      Me.textBox1.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.textBox1.Location = New System.Drawing.Point(56, 57)
      Me.textBox1.Name = "textBox1"
      Me.textBox1.Size = New System.Drawing.Size(562, 20)
      Me.textBox1.TabIndex = 1
      ' 
      ' label27
      ' 
      Me.label27.AutoSize = True
      Me.label27.Location = New System.Drawing.Point(6, 60)
      Me.label27.Name = "label27"
      Me.label27.Size = New System.Drawing.Size(39, 13)
      Me.label27.TabIndex = 0
      Me.label27.Text = "&Folder:"
      ' 
      ' label28
      ' 
      Me.label28.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.label28.AutoSize = True
      Me.label28.Location = New System.Drawing.Point(303, 461)
      Me.label28.Name = "label28"
      Me.label28.Size = New System.Drawing.Size(33, 13)
      Me.label28.TabIndex = 8
      Me.label28.Text = "View:"
      ' 
      ' comboBox11
      ' 
      Me.comboBox11.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.comboBox11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
      Me.comboBox11.FormattingEnabled = True
      Me.comboBox11.Items.AddRange(New Object() {"Small Icon", "Large Icon", "List", "Tile", "Details"})
      Me.comboBox11.Location = New System.Drawing.Point(337, 456)
      Me.comboBox11.Name = "comboBox11"
      Me.comboBox11.Size = New System.Drawing.Size(121, 21)
      Me.comboBox11.TabIndex = 9
      ' 
      ' checkBox14
      ' 
      Me.checkBox14.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox14.Location = New System.Drawing.Point(218, 459)
      Me.checkBox14.Name = "checkBox14"
      Me.checkBox14.Size = New System.Drawing.Size(90, 19)
      Me.checkBox14.TabIndex = 7
      Me.checkBox14.Text = "Owner &Draw"
      Me.checkBox14.UseVisualStyleBackColor = True
      ' 
      ' checkBox16
      ' 
      Me.checkBox16.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox16.Location = New System.Drawing.Point(101, 456)
      Me.checkBox16.Name = "checkBox16"
      Me.checkBox16.Size = New System.Drawing.Size(111, 24)
      Me.checkBox16.TabIndex = 6
      Me.checkBox16.Text = "Show Item &Count"
      Me.checkBox16.UseVisualStyleBackColor = True
      ' 
      ' checkBox17
      ' 
      Me.checkBox17.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
      Me.checkBox17.Location = New System.Drawing.Point(6, 456)
      Me.checkBox17.Name = "checkBox17"
      Me.checkBox17.Size = New System.Drawing.Size(104, 24)
      Me.checkBox17.TabIndex = 5
      Me.checkBox17.Text = "Show &Groups"
      Me.checkBox17.UseVisualStyleBackColor = True
      ' 
      ' label29
      ' 
      Me.label29.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.label29.BackColor = System.Drawing.Color.FromArgb((CInt(Fix((CByte(255))))), (CInt(Fix((CByte(255))))), (CInt(Fix((CByte(128))))))
      Me.label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      Me.label29.Location = New System.Drawing.Point(6, 6)
      Me.label29.Name = "label29"
      Me.label29.Size = New System.Drawing.Size(774, 46)
      Me.label29.TabIndex = 6
      Me.label29.Text = resources.GetString("label29.Text")
      ' 
      ' olvColumn21
      ' 
      Me.olvColumn21.AspectName = "StartTime"
      Me.olvColumn21.DisplayIndex = 2
      Me.olvColumn21.IsVisible = False
      Me.olvColumn21.Text = "Start Time"
      ' 
      ' olvColumn22
      ' 
      Me.olvColumn22.AspectName = "Threads.Count"
      Me.olvColumn22.DisplayIndex = 3
      Me.olvColumn22.IsVisible = False
      Me.olvColumn22.Text = "Thread Count"
      ' 
      ' olvColumn23
      ' 
      Me.olvColumn23.AspectName = "TotalProcessorTime"
      Me.olvColumn23.DisplayIndex = 4
      Me.olvColumn23.IsVisible = False
      Me.olvColumn23.Text = "Processor Time"
      ' 
      ' olvColumn30
      ' 
      Me.olvColumn30.AspectName = "PriorityClass"
      Me.olvColumn30.DisplayIndex = 9
      Me.olvColumn30.IsVisible = False
      Me.olvColumn30.Text = "Priority Class"
      ' 
      ' olvColumn24
      ' 
      Me.olvColumn24.DisplayIndex = 5
      Me.olvColumn24.IsVisible = False
      ' 
      ' olvColumn25
      ' 
      Me.olvColumn25.DisplayIndex = 6
      Me.olvColumn25.IsVisible = False
      ' 
      ' olvColumn20
      ' 
      Me.olvColumn20.DisplayIndex = 2
      Me.olvColumn20.IsVisible = False
      ' 
      ' olvColumn17
      ' 
      Me.olvColumn17.DisplayIndex = 0
      Me.olvColumn17.Text = "Zero"
      ' 
      ' olvColumn13
      ' 
      Me.olvColumn13.DisplayIndex = 1
      Me.olvColumn13.Text = "Two"
      ' 
      ' olvColumn14
      ' 
      Me.olvColumn14.DisplayIndex = 2
      Me.olvColumn14.Text = "Three"
      ' 
      ' olvColumn15
      ' 
      Me.olvColumn15.DisplayIndex = 3
      Me.olvColumn15.Text = "Four"
      ' 
      ' olvColumn6
      ' 
      Me.olvColumn6.DisplayIndex = 0
      ' 
      ' olvColumn11
      ' 
      Me.olvColumn11.DisplayIndex = 1
      Me.olvColumn11.IsVisible = False
      Me.olvColumn11.Text = "One"
      ' 
      ' olvColumn16
      ' 
      Me.olvColumn16.DisplayIndex = 4
      Me.olvColumn16.IsVisible = False
      Me.olvColumn16.Text = "Five"
      ' 
      ' objectListView1
      ' 
      Me.objectListView1.AllColumns.Add(Me.olvColumn35)
      Me.objectListView1.AllColumns.Add(Me.olvColumn36)
      Me.objectListView1.AllColumns.Add(Me.olvColumn37)
      Me.objectListView1.AllColumns.Add(Me.olvColumn38)
      Me.objectListView1.AllColumns.Add(Me.olvColumn39)
      Me.objectListView1.AllColumns.Add(Me.olvColumn40)
      Me.objectListView1.AllColumns.Add(Me.treeColumnFileExtension)
      Me.objectListView1.AllowColumnReorder = True
      Me.objectListView1.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
      Me.objectListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.olvColumn35, Me.olvColumn36, Me.olvColumn37, Me.olvColumn38, Me.olvColumn39, Me.olvColumn40})
      Me.objectListView1.EmptyListMsg = "This folder is completely empty!"
      Me.objectListView1.EmptyListMsgFont = New System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
      Me.objectListView1.HideSelection = False
      Me.objectListView1.ItemRenderer = Nothing
      Me.objectListView1.LargeImageList = Me.imageList2
      Me.objectListView1.Location = New System.Drawing.Point(6, 83)
      Me.objectListView1.Name = "objectListView1"
      Me.objectListView1.OwnerDraw = True
      Me.objectListView1.ShowGroups = False
      Me.objectListView1.Size = New System.Drawing.Size(774, 367)
      Me.objectListView1.SmallImageList = Me.imageList1
      Me.objectListView1.TabIndex = 13
      Me.objectListView1.UseCompatibleStateImageBehavior = False
      Me.objectListView1.UseHotItem = True
      Me.objectListView1.View = System.Windows.Forms.View.Details
      ' 
      ' olvColumn35
      ' 
      Me.olvColumn35.AspectName = "Name"
      Me.olvColumn35.IsTileViewColumn = True
      Me.olvColumn35.Text = "Name"
      Me.olvColumn35.UseInitialLetterForGroup = True
      Me.olvColumn35.Width = 180
      ' 
      ' olvColumn36
      ' 
      Me.olvColumn36.AspectName = "CreationTime"
      Me.olvColumn36.DisplayIndex = 4
      Me.olvColumn36.Text = "Created"
      Me.olvColumn36.Width = 131
      ' 
      ' olvColumn37
      ' 
      Me.olvColumn37.AspectName = "LastWriteTime"
      Me.olvColumn37.DisplayIndex = 1
      Me.olvColumn37.IsTileViewColumn = True
      Me.olvColumn37.Text = "Modified"
      Me.olvColumn37.Width = 145
      ' 
      ' olvColumn38
      ' 
      Me.olvColumn38.AspectName = "Extension"
      Me.olvColumn38.DisplayIndex = 2
      Me.olvColumn38.Text = "Size"
      Me.olvColumn38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
      Me.olvColumn38.Width = 80
      ' 
      ' olvColumn39
      ' 
      Me.olvColumn39.DisplayIndex = 3
      Me.olvColumn39.IsTileViewColumn = True
      Me.olvColumn39.Text = "File Type"
      Me.olvColumn39.Width = 148
      ' 
      ' olvColumn40
      ' 
      Me.olvColumn40.FillsFreeSpace = True
      Me.olvColumn40.IsEditable = False
      Me.olvColumn40.MinimumWidth = 20
      Me.olvColumn40.Text = "Attributes"
      ' 
      ' MainForm
      ' 
      Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.ClientSize = New System.Drawing.Size(819, 548)
      Me.Controls.Add(Me.statusStrip1)
      Me.Controls.Add(Me.tabControl1)
      Me.Name = "MainForm"
      Me.Text = "ObjectListView Demo"
      '			Me.Load += New System.EventHandler(Me.MainForm_Load);
      Me.tabControl1.ResumeLayout(False)
      Me.tabPage1.ResumeLayout(False)
      Me.tabPage1.PerformLayout()
      CType(Me.listViewSimple, System.ComponentModel.ISupportInitialize).EndInit()
      Me.contextMenuStrip1.ResumeLayout(False)
      Me.tabPage2.ResumeLayout(False)
      Me.tabPage2.PerformLayout()
      CType(Me.listViewComplex, System.ComponentModel.ISupportInitialize).EndInit()
      Me.tabPage3.ResumeLayout(False)
      Me.groupBox3.ResumeLayout(False)
      Me.groupBox3.PerformLayout()
      CType(Me.rowHeightUpDown, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.listViewDataSet, System.ComponentModel.ISupportInitialize).EndInit()
      Me.groupBox1.ResumeLayout(False)
      CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
      Me.tabPage4.ResumeLayout(False)
      Me.tabPage4.PerformLayout()
      CType(Me.listViewVirtual, System.ComponentModel.ISupportInitialize).EndInit()
      Me.tabPage5.ResumeLayout(False)
      Me.tabPage5.PerformLayout()
      CType(Me.listViewFiles, System.ComponentModel.ISupportInitialize).EndInit()
      Me.tabPage6.ResumeLayout(False)
      Me.groupBox5.ResumeLayout(False)
      Me.groupBox5.PerformLayout()
      CType(Me.numericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.numericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
      Me.groupBox4.ResumeLayout(False)
      Me.groupBox4.PerformLayout()
      Me.groupBox2.ResumeLayout(False)
      Me.tabPage7.ResumeLayout(False)
      Me.tabPage7.PerformLayout()
      CType(Me.olvFastList, System.ComponentModel.ISupportInitialize).EndInit()
      Me.tabPage9.ResumeLayout(False)
      CType(Me.treeListView, System.ComponentModel.ISupportInitialize).EndInit()
      Me.statusStrip1.ResumeLayout(False)
      Me.statusStrip1.PerformLayout()
      CType(Me.objectListView1, System.ComponentModel.ISupportInitialize).EndInit()
      Me.ResumeLayout(False)
      Me.PerformLayout()

    End Sub
    Private WithEvents Button18 As System.Windows.Forms.Button
    Private WithEvents RbShowSimple As System.Windows.Forms.RadioButton
    Private WithEvents RbShowComplex As System.Windows.Forms.RadioButton
    Private WithEvents RbShowDataset As System.Windows.Forms.RadioButton
    Private WithEvents RbShowFileExplorer As System.Windows.Forms.RadioButton
    Private tbTitle As System.Windows.Forms.TextBox
    Private tbWatermark As System.Windows.Forms.TextBox
    Private tbHeader As System.Windows.Forms.TextBox
    Private tbFooter As System.Windows.Forms.TextBox
    Private WithEvents CbIncludeImages As System.Windows.Forms.CheckBox
    Private label17 As System.Windows.Forms.Label
    Private WithEvents RbStyleMinimal As System.Windows.Forms.RadioButton
    Private WithEvents RbStyleModern As System.Windows.Forms.RadioButton
    Private WithEvents RbStyleTooMuch As System.Windows.Forms.RadioButton
    Private WithEvents CbPrintOnlySelection As System.Windows.Forms.CheckBox
    Private WithEvents CbShrinkToFit As System.Windows.Forms.CheckBox
    Private label12 As System.Windows.Forms.Label
    Private groupBox2 As System.Windows.Forms.GroupBox
    Private label13 As System.Windows.Forms.Label
    Private label14 As System.Windows.Forms.Label
    Private label16 As System.Windows.Forms.Label
    Private label15 As System.Windows.Forms.Label
    Private groupBox4 As System.Windows.Forms.GroupBox
    Private printPreviewControl1 As System.Windows.Forms.PrintPreviewControl
    Private WithEvents ListViewPrinter1 As BrightIdeasSoftware.ListViewPrinter
    Private olvColumnFileName As BrightIdeasSoftware.OLVColumn
    Private olvColumnFileModified As BrightIdeasSoftware.OLVColumn
    Private olvColumnFileCreated As BrightIdeasSoftware.OLVColumn
    Private olvColumnSize As BrightIdeasSoftware.OLVColumn
    Private WithEvents ListViewFiles As BrightIdeasSoftware.ObjectListView
    Private WithEvents ButtonGo As System.Windows.Forms.Button
    Private WithEvents TextBoxFolderPath As System.Windows.Forms.TextBox
    Private label5 As System.Windows.Forms.Label
    Private WithEvents CheckBox12 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox11 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox10 As System.Windows.Forms.CheckBox
    Private WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Private label9 As System.Windows.Forms.Label
    Private label10 As System.Windows.Forms.Label
    Private tabPage5 As System.Windows.Forms.TabPage
    Private WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Private groupBox3 As System.Windows.Forms.GroupBox
    Private dataGridView1 As System.Windows.Forms.DataGridView
    Private olvColumn12 As BrightIdeasSoftware.OLVColumn
    Private WithEvents ListViewSimple As BrightIdeasSoftware.ObjectListView
    Private olvColumn10 As BrightIdeasSoftware.OLVColumn
    Private olvColumn9 As BrightIdeasSoftware.OLVColumn
    Private olvColumn8 As BrightIdeasSoftware.OLVColumn
    Private olvColumn7 As BrightIdeasSoftware.OLVColumn
    Private olvColumn5 As BrightIdeasSoftware.OLVColumn
    Private olvColumn4 As BrightIdeasSoftware.OLVColumn
    Private label4 As System.Windows.Forms.Label
    Private tabPage4 As System.Windows.Forms.TabPage
    Private WithEvents ListViewVirtual As BrightIdeasSoftware.VirtualObjectListView
    Private yearOfBirthColumn As BrightIdeasSoftware.OLVColumn
    Private occupationColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents Button5 As System.Windows.Forms.Button
    Private WithEvents Button4 As System.Windows.Forms.Button
    Private daysSinceBirthColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Private groupBox1 As GroupBox
    Private WithEvents ListViewDataSet As BrightIdeasSoftware.DataListView
    Private salaryColumn As BrightIdeasSoftware.OLVColumn
    Private olvColumn2 As BrightIdeasSoftware.OLVColumn
    Private olvColumn3 As BrightIdeasSoftware.OLVColumn
    Private olvColumn1 As BrightIdeasSoftware.OLVColumn
    Private label3 As System.Windows.Forms.Label
    Private WithEvents Button3 As System.Windows.Forms.Button
    Private tabPage3 As System.Windows.Forms.TabPage
    Private WithEvents Button2 As System.Windows.Forms.Button
    Private WithEvents Button1 As System.Windows.Forms.Button
    Private toolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Private statusStrip1 As System.Windows.Forms.StatusStrip
    Private label2 As System.Windows.Forms.Label
    Private label1 As System.Windows.Forms.Label
    Private WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Private tabPage2 As System.Windows.Forms.TabPage
    Private tabPage1 As System.Windows.Forms.TabPage
    Private WithEvents TabControl1 As System.Windows.Forms.TabControl
    Private imageList1 As System.Windows.Forms.ImageList
    Private personColumn As BrightIdeasSoftware.OLVColumn
    Private columnCookingSkill As BrightIdeasSoftware.OLVColumn
    Private birthdayColumn As BrightIdeasSoftware.OLVColumn
    Private hourlyRateColumn As BrightIdeasSoftware.OLVColumn
    Private columnHeader12 As BrightIdeasSoftware.OLVColumn
    Private columnHeader11 As BrightIdeasSoftware.OLVColumn
    Private columnHeader13 As BrightIdeasSoftware.OLVColumn
    Private columnHeader14 As BrightIdeasSoftware.OLVColumn
    Private columnHeader15 As BrightIdeasSoftware.OLVColumn
    Private columnHeader16 As BrightIdeasSoftware.OLVColumn
    Private WithEvents ListViewComplex As ObjectListView
    Private moneyImageColumn As BrightIdeasSoftware.OLVColumn
    Private imageList2 As ImageList
    Private heightColumn As BrightIdeasSoftware.OLVColumn
    Private label6 As Label
    Private WithEvents ComboBox1 As ComboBox
    Private label7 As Label
    Private WithEvents ComboBox2 As ComboBox
    Private label8 As Label
    Private WithEvents ComboBox3 As ComboBox
    Private olvColumnFileType As OLVColumn
    Private WithEvents ButtonUp As Button
    Private olvColumnGif As OLVColumn
    Private WithEvents RowHeightUpDown As NumericUpDown
    Private label11 As Label
    Private WithEvents CheckBoxPause As CheckBox
    Private WithEvents Button6 As Button
    Private WithEvents Button7 As Button
    Private WithEvents Button8 As Button
    Private WithEvents Button9 As Button
    Private tabPage6 As TabPage
    Private WithEvents Button12 As Button
    Private WithEvents Button11 As Button
    Private WithEvents Button10 As Button
    Private WithEvents CbCellGridLines As CheckBox
    Private groupBox5 As GroupBox
    Private WithEvents NumericUpDown2 As NumericUpDown
    Private label19 As Label
    Private WithEvents NumericUpDown1 As NumericUpDown
    Private label18 As Label
    Private rbShowVirtual As RadioButton
    Private olvJokeColumn As OLVColumn
    Private WithEvents ComboBox5 As ComboBox
    Private label20 As Label
    Private WithEvents ComboBox6 As ComboBox
    Private label21 As Label
    Private label22 As Label
    Private WithEvents ComboBox7 As ComboBox
    Private WithEvents ComboBox8 As ComboBox
    Private label23 As Label
    Private olvColumn6 As OLVColumn
    Private olvColumn17 As OLVColumn
    Private olvColumn13 As OLVColumn
    Private olvColumn14 As OLVColumn
    Private olvColumn15 As OLVColumn
    Private olvColumn11 As OLVColumn
    Private olvColumn16 As OLVColumn
    Private olvColumn20 As OLVColumn
    Private olvColumn24 As OLVColumn
    Private olvColumn25 As OLVColumn
    Private WithEvents Button13 As Button
    Private treeColumnFileExtension As OLVColumn
    Private olvColumnAttributes As OLVColumn
    Private olvColumn21 As OLVColumn
    Private olvColumn22 As OLVColumn
    Private olvColumn23 As OLVColumn
    Private olvColumn30 As OLVColumn
    Private tabPage7 As TabPage
    Private WithEvents OlvFastList As FastObjectListView
    Private olvColumn18 As OLVColumn
    Private olvColumn19 As OLVColumn
    Private olvColumn26 As OLVColumn
    Private olvColumn27 As OLVColumn
    Private olvColumn28 As OLVColumn
    Private olvColumn29 As OLVColumn
    Private olvColumn31 As OLVColumn
    Private olvColumn32 As OLVColumn
    Private olvColumn33 As OLVColumn
    Private WithEvents Button14 As Button
    Private WithEvents ComboBox9 As ComboBox
    Private label24 As Label
    Private label25 As Label
    Private WithEvents ComboBox10 As ComboBox
    Private WithEvents CheckBox13 As CheckBox
    Private WithEvents Button15 As Button
    Private label26 As Label
    Private olvColumn34 As OLVColumn
    Private WithEvents ButtonSaveState As Button
    Private WithEvents ButtonRestoreState As Button
    Private WithEvents Button16 As Button
    Private WithEvents Button17 As Button
    Private contextMenuStrip1 As ContextMenuStrip
    Private command1ToolStripMenuItem As ToolStripMenuItem
    Private command2ToolStripMenuItem As ToolStripMenuItem
    Private command3ToolStripMenuItem As ToolStripMenuItem
    Private appearOnTheColumnHeadersToolStripMenuItem As ToolStripMenuItem
    Private WithEvents CheckBox15 As CheckBox
    Private WithEvents Button19 As Button
    Private tabPage9 As TabPage
    Private WithEvents Button25 As Button
    Private WithEvents Button26 As Button
    Private WithEvents Button27 As Button
    Private label32 As Label
    Private WithEvents TreeListView As TreeListView
    Private treeColumnName As OLVColumn
    Private treeColumnCreated As OLVColumn
    Private treeColumnModified As OLVColumn
    Private treeColumnSize As OLVColumn
    Private treeColumnFileType As OLVColumn
    Private treeColumnAttributes As OLVColumn
    Private button20 As Button
    Private button21 As Button
    Private button22 As Button
    Private button23 As Button
    Private button24 As Button
    Private textBox1 As TextBox
    Private label27 As Label
    Private label28 As Label
    Private comboBox11 As ComboBox
    Private checkBox14 As CheckBox
    Private checkBox16 As CheckBox
    Private checkBox17 As CheckBox
    Private label29 As Label
    Private objectListView1 As ObjectListView
    Private olvColumn35 As OLVColumn
    Private olvColumn36 As OLVColumn
    Private olvColumn37 As OLVColumn
    Private olvColumn38 As OLVColumn
    Private olvColumn39 As OLVColumn
    Private olvColumn40 As OLVColumn
    Private blockFormat1 As BlockFormat
    Private olvColumn41 As OLVColumn
    Private WithEvents CheckBox18 As CheckBox
    Private hotItemStyle1 As HotItemStyle
    Private heightRenderer As BarRenderer
    Private imageRenderer1 As ImageRenderer
    Private salaryRenderer As MultiImageRenderer
    Private olvColumn42 As OLVColumn
    Private cookingSkillRenderer As MultiImageRenderer

  End Class
End Namespace
