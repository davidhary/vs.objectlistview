
'
'* ObjectListViewDemo - A simple demo to show the ObjectListView control
'*
'* User: Phillip Piper
'* Date: 15/10/2006 11:15 AM
'*
'* Change log:
'* 2006-10-20  JPP  Added DataSet tab page
'* 2006-10-15  JPP  Initial version
'


Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Collections
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Diagnostics
Imports System.Threading
Imports System.Drawing.Imaging

Imports BrightIdeasSoftware

Namespace ObjectListViewDemo
	''' <summary>
	''' Description of MainForm.
	''' </summary>
	Public Partial Class MainForm
		''' <summary>
		'''
		''' </summary>
		''' <param name="args"></param>
		<STAThread> 
		Public Shared Sub Main(ByVal args As String())
			Application.EnableVisualStyles()
			Application.SetCompatibleTextRenderingDefault(False)
      Application.Run(New MainForm())
		End Sub

		''' <summary>
		'''
		''' </summary>
		Public Sub New()
			'
			' The InitializeComponent() call is required for Windows Forms designer support.
			'
			InitializeComponent()
			InitializeExamples()
		End Sub

		Private masterList As List(Of Person)
		Private Sub InitializeExamples()
			masterList = New List(Of Person)()
			masterList.Add(New Person("Wilhelm Frat", "Gymnast", 19, New DateTime(1984, 9, 23), 45.67, False, "ak", "Aggressive, belligerent "))
			masterList.Add(New Person("Alana Roderick", "Gymnast", 21, New DateTime(1974, 9, 23), 245.67, False, "gp", "Beautiful, exquisite"))
			masterList.Add(New Person("Frank Price", "Dancer", 30, New DateTime(1965, 11, 1), 75.5, False, "ns", "Competitive, spirited"))
			masterList.Add(New Person("Eric", "Half-a-bee", 1, New DateTime(1966, 10, 12), 12.25, True, "cp", "Diminutive, vertically challenged"))
			masterList.Add(New Person("Madalene Alright", "School Teacher", 21, New DateTime(1964, 9, 23), 145.67, False, "jr", "Extensive, dimensionally challenged"))
			masterList.Add(New Person("Ned Peirce", "School Teacher", 21, New DateTime(1960, 1, 23), 145.67, False, "gab", "Fulsome, effusive"))
			masterList.Add(New Person("Felicity Brown", "Economist", 30, New DateTime(1975, 1, 12), 175.5, False, "sp", "Gifted, exceptional"))
			masterList.Add(New Person("Urny Unmin", "Economist", 41, New DateTime(1956, 9, 24), 212.25, True, "cr", "Heinous, aesthetically challenged"))
			masterList.Add(New Person("Terrance Darby", "Singer", 35, New DateTime(1970, 9, 29), 1145, False, "mb", "Introverted, relationally challenged"))
			masterList.Add(New Person("Phillip Nottingham", "Programmer", 27, New DateTime(1974, 8, 28), 245.7, False, "sj", "Jocular, gregarious"))
			masterList.Add(New Person("Mister Null"))

			Dim list As List(Of Person) = New List(Of Person)()
			For Each p As Person In masterList
				list.Add(New Person(p))
			Next p

			' Change this value to see the performance on bigger lists.
			' Each list builds about 1000 rows per second.
			'while (list.Count < 200) {
			'    foreach (Person p in masterList)
			'        list.Add(new Person(p));
			'}

			InitializeSimpleExample(list)
			InitializeComplexExample(list)
			InitializeDataSetExample()
			InitializeVirtualListExample()
			InitializeExplorerExample()
			InitializeTreeListExample()
			InitializeListPrinting()
			InitializeFastListExample(list)
#If MONO Then
			' As of 2008-03-23, grid lines on virtual lists on Windows Mono crashes the program
			Me.listViewVirtual.GridLines = False
			Me.olvFastList.GridLines = False
#End If
		End Sub

		Private Sub TimedRebuildList(ByVal olv As ObjectListView)
			Dim stopWatch As Stopwatch = New Stopwatch()

			Try
				Me.Cursor = Cursors.WaitCursor
				stopWatch.Start()
				olv.BuildList()
			Finally
				stopWatch.Stop()
				Me.Cursor = Cursors.Default
			End Try

			Me.toolStripStatusLabel1.Text = String.Format("Build time: {0} items in {1}ms, average per item: {2:F}ms", olv.Items.Count, stopWatch.ElapsedMilliseconds, CSng(stopWatch.ElapsedMilliseconds) / olv.Items.Count)
		End Sub

		Private Sub InitializeSimpleExample(ByVal list As List(Of Person))
			Me.comboBox6.SelectedIndex = 0

			' Give this column an aspect putter, since it fetches its value using a method rather than a property
			Dim tcol As TypedColumn(Of Person) = New TypedColumn(Of Person)(Me.columnHeader16)
			tcol.AspectPutter = AddressOf AnonymousMethod1

			' Just one line of code make everything happen.
			Me.listViewSimple.SetObjects(list)
		End Sub
		Private Sub AnonymousMethod1(ByVal x As Person, ByVal newValue As Object)
			x.SetRate(CDbl(newValue))
		End Sub

		Private Sub InitializeComplexExample(ByVal list As List(Of Person))
			' The following line makes getting aspect about 10x faster. Since getting the aspect is
			' the slowest part of building the ListView, it is worthwhile BUT NOT NECESSARY to do.
			Dim tlist As TypedObjectListView(Of Person) = New TypedObjectListView(Of Person)(Me.listViewComplex)
			tlist.GenerateAspectGetters()
			' The line above the equivilent to typing the following:
			'tlist.GetColumn(0).AspectGetter = delegate(Person x) { return x.Name; };
			'tlist.GetColumn(1).AspectGetter = delegate(Person x) { return x.Occupation; };
			'tlist.GetColumn(2).AspectGetter = delegate(Person x) { return x.CulinaryRating; };
			'tlist.GetColumn(3).AspectGetter = delegate(Person x) { return x.YearOfBirth; };
			'tlist.GetColumn(4).AspectGetter = delegate(Person x) { return x.BirthDate; };
			'tlist.GetColumn(5).AspectGetter = delegate(Person x) { return x.GetRate(); };
			'tlist.GetColumn(6).AspectGetter = delegate(Person x) { return x.Comments; };
			'

			Me.personColumn.AspectToStringConverter = AddressOf AnonymousMethod2
			Me.personColumn.ImageGetter = AddressOf AnonymousMethod3

			' Cooking skill columns
			Me.columnCookingSkill.MakeGroupies(New Int32(){10, 20, 30, 40}, New String() {"Pay to eat out", "Suggest take-away", "Passable", "Seek dinner invitation", "Hire as chef"})

			' Hourly rate column
			Me.hourlyRateColumn.MakeGroupies(New Double() { 100, 1000 }, New String() { "Less than $100", "$100-$1000", "Megabucks" })
			Me.hourlyRateColumn.AspectPutter = AddressOf AnonymousMethod4

			' Salary indicator column
			Me.moneyImageColumn.AspectGetter = AddressOf AnonymousMethod5
			Me.moneyImageColumn.Renderer = New MappedImageRenderer(New Object() { "Little", Resource1.down16, "Medium", Resource1.tick16, "Lots", Resource1.star16 })

			' Birthday column
			Me.birthdayColumn.GroupKeyGetter = AddressOf AnonymousMethod6
			Me.birthdayColumn.GroupKeyToTitleConverter = AddressOf AnonymousMethod7
			Me.birthdayColumn.ImageGetter = AddressOf AnonymousMethod8

			' Use this column to test sorting and group on TimeSpan objects
			Me.daysSinceBirthColumn.AspectGetter = AddressOf AnonymousMethod9
			Me.daysSinceBirthColumn.AspectToStringConverter = AddressOf AnonymousMethod10

			' Show a long tooltip over cells when the control key is down
			Me.listViewComplex.CellToolTipGetter = AddressOf AnonymousMethod11

			comboBox1.SelectedIndex = 4
			comboBox5.SelectedIndex = 0
			listViewComplex.SetObjects(list)

			Me.listViewComplex.ItemRenderer = New BusinessCardRenderer()
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod2(ByVal cellValue As Object) As String
      Return (CType(cellValue, String)).ToUpperInvariant()
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod3(ByVal row As Object) As Object
			Dim name As String = (CType(row, Person)).Name
			If name.Length > 0 AndAlso "AEIOU".Contains(name.Substring(0, 1)) Then
			Return 0 ' star
			ElseIf name.CompareTo("N") < 0 Then
			Return 1 ' heart
			Else
			Return 2 ' music
			End If
		End Function
		Private Sub AnonymousMethod4(ByVal x As Object, ByVal newValue As Object)
			CType(x, Person).SetRate(CDbl(newValue))
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod5(ByVal row As Object) As Object
			If (CType(row, Person)).GetRate() < 100 Then
			Return "Little"
			End If
			If (CType(row, Person)).GetRate() > 1000 Then
			Return "Lots"
			End If
			Return "Medium"
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod6(ByVal row As Object) As Object
			Return (CType(row, Person)).BirthDate.Month
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod7(ByVal key As Object) As String
      Return (New DateTime(1, CInt(Fix(key)), 1)).ToString("MMMM")
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod8(ByVal row As Object) As Object
			Dim p As Person = CType(row, Person)
      If p IsNot Nothing AndAlso (p.BirthDate.Year Mod 10) = 4 Then
        Return 3
      Else
        Return -1 ' no image
      End If
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod9(ByVal row As Object) As Object
			Return DateTime.Now - (CType(row, Person)).BirthDate
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod10(ByVal aspect As Object) As String
      Return (CType(aspect, TimeSpan)).Days.ToString()
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod11(ByVal col As OLVColumn, ByVal x As Object) As String
      If Control.ModifierKeys = Keys.Control Then
        Return String.Format("Tool tip for '{0}', column '{1}'" & Constants.vbCrLf & "Value shown: '{2}'", (CType(x, Person)).Name, col.Text, col.GetStringValue(x))
      Else
        Return Nothing
      End If
    End Function

		''' <summary>
		''' Hackish renderer that draw a fancy version of a person for a Tile view.
		''' </summary>
		''' <remarks>This is not the way to write a professional level renderer.
		''' It is hideously inefficient (we should at least cache the images),
		''' but it is obvious</remarks>
		Friend Class BusinessCardRenderer
			Inherits AbstractRenderer
			Public Overrides Function RenderItem(ByVal e As DrawListViewItemEventArgs, ByVal g As Graphics, ByVal itemBounds As Rectangle, ByVal rowObject As Object) As Boolean
				' If we're in any other view than Tile, return false to say that we haven't done 
				' the rendereing and the default process should do it's stuff
				Dim olv As ObjectListView = TryCast(e.Item.ListView, ObjectListView)
				If olv Is Nothing OrElse olv.View <> View.Tile Then
					Return False
				End If

				Const spacing As Integer = 8

				' Use buffered graphics to kill flickers
				Dim buffered As BufferedGraphics = BufferedGraphicsManager.Current.Allocate(g, itemBounds)
				g = buffered.Graphics
				g.Clear(olv.BackColor)

				g.SmoothingMode = SmoothingMode.AntiAlias
				Dim grey13Pen As Pen = New Pen(Color.FromArgb(&H33, &H33, &H33))
				Dim grey13Brush As SolidBrush = New SolidBrush(Color.FromArgb(&H33, &H33, &H33))

				' Allow a border around the card
				itemBounds.Inflate(-2, -2)

				' Draw card background
				Const rounding As Integer = 20
				Dim path As GraphicsPath = Me.GetRoundedRect(itemBounds, rounding)
				g.FillPath(Brushes.LemonChiffon, path)
				If e.Item.Selected Then
					g.DrawPath(Pens.Blue, path)
				Else
					g.DrawPath(grey13Pen, path)
				End If

				g.Clip = New Region(itemBounds)

				' Draw the photo
				Dim photoRect As Rectangle = itemBounds
				photoRect.Inflate(-spacing, -spacing)
				Dim person As Person = TryCast(rowObject, Person)
				If Not person Is Nothing Then
					Try
						photoRect.Width = 75
						Dim photo As Image = Image.FromFile(String.Format(".\Photos\{0}.png", person.Photo))
						If photo.Width > photoRect.Width Then
							photoRect.Height = CInt(Fix(photo.Height * (CSng(photoRect.Width) / photo.Width)))
						Else
							photoRect.Height = photo.Height
						End If
						g.DrawImage(photo, photoRect)
					Catch e1 As FileNotFoundException
						g.DrawRectangle(Pens.DarkGray, photoRect)
					End Try
				End If

				' Now draw the text portion
				Dim textBoxRect As RectangleF = photoRect
				textBoxRect.X += (photoRect.Width + spacing)
				textBoxRect.Width = itemBounds.Right - textBoxRect.X - spacing

				' Measure the height of the title
				Dim fmt As StringFormat = New StringFormat(StringFormatFlags.NoWrap)
				fmt.Trimming = StringTrimming.EllipsisCharacter
				fmt.Alignment = StringAlignment.Center
				fmt.LineAlignment = StringAlignment.Near
				Dim font As Font = New Font("Tahoma", 11)
				Dim txt As String = e.Item.Text
				Dim size As SizeF = g.MeasureString(txt, font, CInt(Fix(textBoxRect.Width)), fmt)

				' Draw the title
				Dim r3 As RectangleF = textBoxRect
				r3.Height = size.Height
				path = Me.GetRoundedRect(r3, 15)
				If e.Item.Selected Then
					g.FillPath(New SolidBrush(olv.HighlightBackgroundColorOrDefault), path)
				Else
					g.FillPath(grey13Brush, path)
				End If
				g.DrawString(txt, font, Brushes.AliceBlue, textBoxRect, fmt)
				textBoxRect.Y += size.Height + spacing

				' Draw the other bits of information
				font = New Font("Tahoma", 8)
				size = g.MeasureString("Wj", font, itemBounds.Width, fmt)
				textBoxRect.Height = size.Height
				fmt.Alignment = StringAlignment.Near
				For i As Integer = 0 To olv.Columns.Count - 1
					Dim column As OLVColumn = olv.GetColumn(i)
					If column.IsTileViewColumn Then
						txt = column.GetStringValue(rowObject)
						g.DrawString(txt, font, grey13Brush, textBoxRect, fmt)
						textBoxRect.Y += size.Height
					End If
				Next i

				' Finally render the buffered graphics
				buffered.Render()
				buffered.Dispose()

				' Return true to say that we've handled the drawing
				Return True
			End Function

			Private Function GetRoundedRect(ByVal rect As RectangleF, ByVal diameter As Single) As GraphicsPath
				Dim path As GraphicsPath = New GraphicsPath()

				Dim arc As RectangleF = New RectangleF(rect.X, rect.Y, diameter, diameter)
				path.AddArc(arc, 180, 90)
				arc.X = rect.Right - diameter
				path.AddArc(arc, 270, 90)
				arc.Y = rect.Bottom - diameter
				path.AddArc(arc, 0, 90)
				arc.X = rect.Left
				path.AddArc(arc, 90, 90)
				path.CloseFigure()

				Return path
			End Function
		End Class

		Private Sub InitializeDataSetExample()
			Me.olvColumn1.ImageGetter = AddressOf AnonymousMethod12

			Me.salaryColumn.MakeGroupies(New UInt32() { 20000, 100000 }, New String() { "Lowly worker", "Middle management", "Rarified elevation" })
			'this.salaryColumn.Renderer = new MultiImageRenderer(Resource1.tick16, 5, 10000, 500000);

			Me.heightColumn.MakeGroupies(New Double() { 1.50, 1.70, 1.85 }, New String() { "Shortie", "Normal", "Tall", "Really tall" })
			'this.heightColumn.Renderer = new BarRenderer(0, 2);

			'this.olvColumnGif.Renderer = new ImageRenderer(true);

			Me.comboBox3.SelectedIndex = 4
			Me.comboBox7.SelectedIndex = 0

			Me.listViewDataSet.RowFormatter = AddressOf AnonymousMethod13

			Me.rowHeightUpDown.Value = 32

			' Long values in the first column will wrap
			Dim renderer As BaseRenderer = New BaseRenderer()
			renderer.CanWrap = True
			Me.listViewDataSet.GetColumn(0).Renderer = renderer

			LoadXmlIntoList()
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod12(ByVal row As Object) As Object
			Return "user"
		End Function
		Private Sub AnonymousMethod13(ByVal olvi As OLVListItem)
			Dim colorNames As String() = New String() { "red", "green", "blue", "yellow" }
			For Each subItem As ListViewItem.ListViewSubItem In olvi.SubItems
			For Each name As String In colorNames
			If subItem.Text.ToLowerInvariant().Contains(name) Then
			olvi.UseItemStyleForSubItems = False
			If subItem.Text.ToLowerInvariant().Contains("bk-" & name) Then
			subItem.BackColor = Color.FromName(name)
			Else
			subItem.ForeColor = Color.FromName(name)
			End If
			End If
			Next name
			Dim style As FontStyle = FontStyle.Regular
			If subItem.Text.ToLowerInvariant().Contains("bold") Then
			style = style Or FontStyle.Bold
			End If
			If subItem.Text.ToLowerInvariant().Contains("italic") Then
			style = style Or FontStyle.Italic
			End If
			If subItem.Text.ToLowerInvariant().Contains("underline") Then
			style = style Or FontStyle.Underline
			End If
			If subItem.Text.ToLowerInvariant().Contains("strikeout") Then
			style = style Or FontStyle.Strikeout
			End If
			If style <> FontStyle.Regular Then
			olvi.UseItemStyleForSubItems = False
			subItem.Font = New Font(subItem.Font, style)
			End If
			Next subItem
		End Sub

		' A sorter to order Person objects according to a given column in a list view
		Friend Class MasterListSorter
			Inherits Comparer(Of Person)
			Public Sub New(ByVal col As OLVColumn, ByVal order As SortOrder)
				Me.column = col
				Me.sortOrder = order
			End Sub
			Private column As OLVColumn
			Private sortOrder As SortOrder

			Public Overrides Function Compare(ByVal x As Person, ByVal y As Person) As Integer
				Dim xValue As IComparable = TryCast(Me.column.GetValue(x), IComparable)
				Dim yValue As IComparable = TryCast(Me.column.GetValue(y), IComparable)

				Dim result As Integer = 0
				If xValue Is Nothing OrElse yValue Is Nothing Then
					If xValue Is Nothing AndAlso yValue Is Nothing Then
						result = 0
					Else
						If Not xValue Is Nothing Then
							result = 1
						Else
							result = -1
						End If
					End If
				Else
					result = xValue.CompareTo(yValue)
				End If

				If Me.sortOrder = SortOrder.Ascending Then
					Return result
				Else
					Return 0 - result
				End If
			End Function
		End Class

		Private Sub InitializeVirtualListExample()
			Me.listViewVirtual.BooleanCheckStateGetter = AddressOf AnonymousMethod14
			Me.listViewVirtual.BooleanCheckStatePutter = AddressOf AnonymousMethod15
			Me.listViewVirtual.VirtualListSize = 10000000
			Me.listViewVirtual.RowGetter = AddressOf AnonymousMethod16

			' Install a custom sorter, just to show how it could be done. We don't
			' have a backing store that can sort all 10 million items, so we have to be
			' content with sorting the master list and showing that sorted. It gives the idea.
			Me.listViewVirtual.CustomSorter = AddressOf AnonymousMethod17

			' Install aspect getters to optimize performance
			Me.olvColumn4.AspectGetter = AddressOf AnonymousMethod18
			Me.olvColumn5.AspectGetter = AddressOf AnonymousMethod19
			Me.olvColumn7.AspectGetter = AddressOf AnonymousMethod20
			Me.olvColumn8.AspectGetter = AddressOf AnonymousMethod21
			Me.olvColumn9.AspectGetter = AddressOf AnonymousMethod22
			Me.olvColumn10.AspectGetter = AddressOf AnonymousMethod23
			Me.olvColumn10.AspectPutter = AddressOf AnonymousMethod24

			' Install a RowFormatter to setup a tooltip on the item
			Me.listViewVirtual.RowFormatter = AddressOf AnonymousMethod25

			Me.olvColumn4.ImageGetter = AddressOf AnonymousMethod26
			Me.olvColumn5.ImageGetter = AddressOf AnonymousMethod27 ' user icon
			Me.olvColumn5.Renderer = New BaseRenderer()

			Me.olvColumn7.Renderer = New MultiImageRenderer("star", 5, 0, 50)


			Me.comboBox2.SelectedIndex = 4
			Me.comboBox8.SelectedIndex = 0
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod14(ByVal x As Object) As Boolean
      Return (CType(x, Person)).IsActive
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod15(ByVal x As Object, ByVal newValue As Boolean) As Boolean
      CType(x, Person).IsActive = newValue
      Return newValue
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod16(ByVal i As Integer) As Object
			Dim p As Person = masterList(i Mod masterList.Count)
			p.serialNumber = i
			Return p
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod17(ByVal col As OLVColumn, ByVal order As SortOrder) As Object
			masterList.Sort(New MasterListSorter(col, order))
      Me.listViewVirtual.BuildList()
      Return Nothing
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod18(ByVal x As Object) As Object
			Return (CType(x, Person)).Name
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod19(ByVal x As Object) As Object
			Return (CType(x, Person)).Occupation
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod20(ByVal x As Object) As Object
			Return (CType(x, Person)).CulinaryRating
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod21(ByVal x As Object) As Object
			Return (CType(x, Person)).YearOfBirth
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod22(ByVal x As Object) As Object
			Return (CType(x, Person)).BirthDate
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod23(ByVal x As Object) As Object
			Return (CType(x, Person)).GetRate()
		End Function
		Private Sub AnonymousMethod24(ByVal x As Object, ByVal newValue As Object)
			CType(x, Person).SetRate(CDbl(newValue))
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod25(ByVal lvi As OLVListItem) As Object
      lvi.ToolTipText = "This is a long tool tip for '" & lvi.Text & "' that does nothing except waste space."
      Return Nothing
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod26(ByVal row As Object) As Object
			If "AEIOU".Contains((CType(row, Person)).Name.Substring(0, 1)) Then
			Return 0 ' star
			ElseIf (CType(row, Person)).Name.CompareTo("N") < 0 Then
			Return 1 ' heart
			Else
			Return 2 ' music
			End If
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod27(ByVal row As Object) As Object
			Return "user"
		End Function

    Public Function ImageGetter11(ByVal row As Object) As Object
      Dim helper As SysImageListHelper = New SysImageListHelper(Me.listViewFiles)
      Return helper.GetImageIndex((CType(row, FileSystemInfo)).FullName)
    End Function

    Private Sub InitializeExplorerExample()
      ' Draw the system icon next to the name
#If (Not MONO) Then
      Me.olvColumnFileName.ImageGetter = AddressOf imageGetter11
#If False Then
      Dim helper As SysImageListHelper = New SysImageListHelper(Me.listViewFiles)
      'TO_DO: INSTANT VB TO_DO TASK: Anonymous methods are not converted by Instant VB if the parameter list is omitted or local variables of the outer method are referenced within the anonymous method:
			Me.olvColumnFileName.ImageGetter = delegate(Object x)
      Return helper.GetImageIndex((CType(x, FileSystemInfo)).FullName)
#End If
#End If
      ' Show the size of files as GB, MB and KBs. Also, group them by
      ' some meaningless divisions
      Me.olvColumnSize.AspectGetter = AddressOf AnonymousMethod28
      Me.olvColumnSize.AspectToStringConverter = AddressOf AnonymousMethod29
      Me.olvColumnSize.MakeGroupies(New Long() {0, 1024 * 1024, 512 * 1024 * 1024}, New String() {"Folders", "Small", "Big", "Disk space chewer"})

      ' Group by month-year, rather than date
      ' This code is duplicated for FileCreated and FileModified, so we really should
      ' create named methods rather than using anonymous delegates.
      Me.olvColumnFileCreated.GroupKeyGetter = AddressOf AnonymousMethod30
      Me.olvColumnFileCreated.GroupKeyToTitleConverter = AddressOf AnonymousMethod31

      ' Group by month-year, rather than date
      Me.olvColumnFileModified.GroupKeyGetter = AddressOf AnonymousMethod32
      Me.olvColumnFileModified.GroupKeyToTitleConverter = AddressOf AnonymousMethod33

      ' Show the system description for this object
      Me.olvColumnFileType.AspectGetter = AddressOf AnonymousMethod34

      ' Show the file attributes for this object
      Me.olvColumnAttributes.AspectGetter = AddressOf AnonymousMethod35
      Dim attributesRenderer As FlagRenderer = New FlagRenderer()
      attributesRenderer.Add(FileAttributes.Archive, "archive")
      attributesRenderer.Add(FileAttributes.ReadOnly, "readonly")
      attributesRenderer.Add(FileAttributes.System, "system")
      attributesRenderer.Add(FileAttributes.Hidden, "hidden")
      attributesRenderer.Add(FileAttributes.Temporary, "temporary")
      Me.olvColumnAttributes.Renderer = attributesRenderer

      Me.comboBox4.SelectedIndex = 4
      Me.textBoxFolderPath.Text = "c:\"
      Me.PopulateListFromPath(Me.textBoxFolderPath.Text)
    End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod28(ByVal x As Object) As Object
			If TypeOf x Is DirectoryInfo Then
			Return CLng(Fix(-1))
			End If
			Try
			Return (CType(x, FileInfo)).Length
			Catch e1 As System.IO.FileNotFoundException
			Return CLng(Fix(-2))
			End Try
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod29(ByVal x As Object) As String
      If CLng(Fix(x)) = -1 Then ' folder
        Return String.Empty
      Else
        Return Me.FormatFileSize(CLng(Fix(x)))
      End If
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod30(ByVal x As Object) As Object
			Dim dt As DateTime = (CType(x, FileSystemInfo)).CreationTime
			Return New DateTime(dt.Year, dt.Month, 1)
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod31(ByVal x As Object) As String
      Return (CDate(x)).ToString("MMMM yyyy")
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod32(ByVal x As Object) As Object
			Dim dt As DateTime = (CType(x, FileSystemInfo)).LastWriteTime
			Return New DateTime(dt.Year, dt.Month, 1)
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod33(ByVal x As Object) As String
      Return (CDate(x)).ToString("MMMM yyyy")
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod34(ByVal x As Object) As Object
			Return ShellUtilities.GetFileType((CType(x, FileSystemInfo)).FullName)
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod35(ByVal x As Object) As Object
			Return (CType(x, FileSystemInfo)).Attributes
		End Function


		Private Sub InitializeTreeListExample()
			Me.treeListView.CanExpandGetter = AddressOf AnonymousMethod36
			Me.treeListView.ChildrenGetter = AddressOf AnonymousMethod37

			Me.treeListView.CheckBoxes = True

			' You can change the way the connection lines are drawn by changing the pen
			'((TreeListView.TreeRenderer)this.treeListView.TreeColumnRenderer).LinePen = Pens.Firebrick;

			'-------------------------------------------------------------------
			' Eveything after this is the same as the Explorer example tab --
			' nothing specific to the TreeListView. It doesn't have the grouping
			' delegates, since TreeListViews can't show groups.

			' Draw the system icon next to the name
#If (Not MONO) Then
      Me.treeColumnName.ImageGetter = AddressOf imageGetter11
#If False Then
      Dim helper As SysImageListHelper = New SysImageListHelper(Me.treeListView)
      'TO_DO: INSTANT VB TO_DO TASK: Anonymous methods are not converted by Instant VB if the parameter list is omitted or local variables of the outer method are referenced within the anonymous method:
			Me.treeColumnName.ImageGetter = delegate(Object x)
      Return helper.GetImageIndex((CType(x, FileSystemInfo)).FullName)
#End If
#End If
			' Show the size of files as GB, MB and KBs. Also, group them by
			' some meaningless divisions
			Me.treeColumnSize.AspectGetter = AddressOf AnonymousMethod38
			Me.treeColumnSize.AspectToStringConverter = AddressOf AnonymousMethod39

			' Show the system description for this object
			Me.treeColumnFileType.AspectGetter = AddressOf AnonymousMethod40

			' Show the file attributes for this object
			Me.treeColumnAttributes.AspectGetter = AddressOf AnonymousMethod41
      Dim attributesRenderer As FlagRenderer = New FlagRenderer()
			attributesRenderer.Add(FileAttributes.Archive, "archive")
			attributesRenderer.Add(FileAttributes.ReadOnly, "readonly")
			attributesRenderer.Add(FileAttributes.System, "system")
			attributesRenderer.Add(FileAttributes.Hidden, "hidden")
			attributesRenderer.Add(FileAttributes.Temporary, "temporary")
			Me.treeColumnAttributes.Renderer = attributesRenderer

			' List all drives as the roots of the tree
			Dim roots As ArrayList = New ArrayList()
			For Each di As DriveInfo In DriveInfo.GetDrives()
				If di.IsReady Then
					roots.Add(New DirectoryInfo(di.Name))
				End If
			Next di
			Me.treeListView.Roots = roots
			Me.treeListView.CellEditActivation = ObjectListView.CellEditActivateMode.F2Only
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod36(ByVal x As Object) As Boolean
      Return (TypeOf x Is DirectoryInfo)
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod37(ByVal x As Object) As IEnumerable
      Dim dir As DirectoryInfo = CType(x, DirectoryInfo)
      Return New ArrayList(dir.GetFileSystemInfos())
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod38(ByVal x As Object) As Object
			If TypeOf x Is DirectoryInfo Then
			Return CLng(Fix(-1))
			End If
			Try
			Return (CType(x, FileInfo)).Length
			Catch e1 As System.IO.FileNotFoundException
			Return CLng(Fix(-2))
			End Try
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod39(ByVal x As Object) As String
      If CLng(Fix(x)) = -1 Then ' folder
        Return String.Empty
      Else
        Return Me.FormatFileSize(CLng(Fix(x)))
      End If
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod40(ByVal x As Object) As Object
			Return ShellUtilities.GetFileType((CType(x, FileSystemInfo)).FullName)
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod41(ByVal x As Object) As Object
			Return (CType(x, FileSystemInfo)).Attributes
		End Function

		Private Sub InitializeListPrinting()
			' For some reason the Form Designer loses these settings
			Me.printPreviewControl1.Zoom = 1
			Me.printPreviewControl1.AutoZoom = True

			Me.UpdatePrintPreview()
		End Sub

		Private Function FormatFileSize(ByVal size As Long) As String
			Dim limits As Integer() = New Integer() { 1024 * 1024 * 1024, 1024 * 1024, 1024 }
			Dim units As String() = New String() { "GB", "MB", "KB" }

			For i As Integer = 0 To limits.Length - 1
				If size >= limits(i) Then
					Return String.Format("{0:#,##0.##} " & units(i), (CDbl(size) / limits(i)))
				End If
			Next i

			Return String.Format("{0} bytes", size)

		End Function

		Private Sub LoadXmlIntoList()
			Dim ds As DataSet = LoadDatasetFromXml("Persons.xml")

			If ds.Tables.Count > 0 Then
#If (Not MONO) Then
				Me.dataGridView1.DataSource = ds
				Me.dataGridView1.DataMember = "Person"
#End If
				' Install this data source
#If MONO Then
				Dim personTable As DataTable = ds.Tables("Person")
				Me.listViewDataSet.DataSource = personTable
#Else
				Me.listViewDataSet.DataSource = New BindingSource(ds, "Person")
#End If
				' Test with BindingSource
				'this.listViewDataSet.DataSource = new BindingSource(ds, "Person");

				' Test with DataTable
				'DataTable personTable = ds.Tables["Person"];
				'this.listViewDataSet.DataSource = personTable;

				' Test with DataView
				'DataTable personTable = ds.Tables["Person"];
				'this.listViewDataSet.DataSource = new DataView(personTable);

				' Test with DataSet
				'this.listViewDataSet.DataMember = "Person";
				'this.listViewDataSet.DataSource = ds;

				' Test with DataViewManager
				'this.listViewDataSet.DataMember = "Person";
				'this.listViewDataSet.DataSource = new DataViewManager(ds);

				' Test with nulls
				'this.listViewDataSet.DataMember = null;
				'this.listViewDataSet.DataSource = null;
			End If
		End Sub

		Private Function LoadDatasetFromXml(ByVal fileName As String) As DataSet
			Dim ds As DataSet = New DataSet()
			Dim fs As FileStream = Nothing

			Try
				fs = New FileStream(fileName, FileMode.Open, FileAccess.Read)
				Dim reader As StreamReader = New StreamReader(fs)
				  ds.ReadXml(reader)
			Catch e As Exception
				MessageBox.Show(e.ToString())
			Finally
				If Not fs Is Nothing Then
					fs.Close()
				End If
			End Try

			Return ds
		End Function

		Private Sub TimedReloadXml()
			Dim stopWatch As Stopwatch = New Stopwatch()

			Try
				Me.Cursor = Cursors.WaitCursor
				stopWatch.Start()
				Me.LoadXmlIntoList()
			Finally
				stopWatch.Stop()
				Me.Cursor = Cursors.Default
			End Try

			Me.toolStripStatusLabel1.Text = String.Format("XML Load: {0} items in {1}ms, average per item: {2:F}ms", listViewDataSet.Items.Count, stopWatch.ElapsedMilliseconds, stopWatch.ElapsedMilliseconds / listViewDataSet.Items.Count)
		End Sub

		#Region "Form event handlers"

		Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
		End Sub

		#End Region

		#Region "Utilities"

		Private Sub ShowGroupsChecked(ByVal olv As ObjectListView, ByVal cb As CheckBox)
			olv.ShowGroups = cb.Checked
			olv.BuildList()
		End Sub

		Private Sub ShowLabelsOnGroupsChecked(ByVal olv As ObjectListView, ByVal cb As CheckBox)
			olv.ShowItemCountOnGroups = cb.Checked
			olv.BuildGroups()
		End Sub

		Private Sub HandleSelectionChanged(ByVal listView As ObjectListView)
			Dim msg As String
			Dim p As Person = CType(listView.GetSelectedObject(), Person)
			If p Is Nothing Then
				msg = listView.SelectedIndices.Count.ToString()
			Else
				msg = "'" & p.Name & "'"
			End If
			Me.toolStripStatusLabel1.Text = String.Format("Selected {0} of {1} items", msg, listView.GetItemCount())
		End Sub

		Private Sub ListViewSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listViewSimple.SelectedIndexChanged, listViewComplex.SelectedIndexChanged
			HandleSelectionChanged(CType(sender, ObjectListView))
		End Sub

		Private Sub ChangeView(ByVal listview As ObjectListView, ByVal comboBox As ComboBox)
			' Handle restrictions on Tile view
			If comboBox.SelectedIndex = 3 Then
				If listview.VirtualMode Then
					MessageBox.Show("Sorry, Microsoft says that virtual lists can't use Tile view.", "Object List View Demo", MessageBoxButtons.OK, MessageBoxIcon.Information)
					Return
				End If
				If listview.CheckBoxes Then
					MessageBox.Show("Microsoft says that Tile view can't have checkboxes, so CheckBoxes have been turned off on this list.", "Object List View Demo", MessageBoxButtons.OK, MessageBoxIcon.Information)
					listview.CheckBoxes = False
				End If
			End If

			Select Case comboBox.SelectedIndex
				Case 0
					listview.View = View.SmallIcon
				Case 1
					listview.View = View.LargeIcon
				Case 2
					listview.View = View.List
				Case 3
					listview.View = View.Tile
				Case 4
					listview.View = View.Details
			End Select
		End Sub

		Private Sub ChangeOwnerDrawn(ByVal listview As ObjectListView, ByVal cb As CheckBox)
			listview.OwnerDraw = cb.Checked
			listview.BuildList()
		End Sub

		#End Region

		#Region "Simple Tab Event Handlers"

		Private Sub CheckBox3CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBox3.CheckedChanged
			ShowGroupsChecked(Me.listViewSimple, CType(sender, CheckBox))
		End Sub

		Private Sub CheckBox4CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBox4.CheckedChanged
			ShowLabelsOnGroupsChecked(Me.listViewSimple, CType(sender, CheckBox))
		End Sub

		Private Sub Button1Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button1.Click
			Me.TimedRebuildList(Me.listViewSimple)
		End Sub

		Private Sub Button4Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button4.Click
			' Silly example just to make sure that object selection works

			listViewSimple.SelectedObjects = listViewComplex.SelectedObjects
			listViewSimple.Select()

			listViewSimple.CopyObjectsToClipboard(listViewSimple.CheckedObjects)
		End Sub

		Private Sub Button7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button7.Click
			Dim person As Person = New Person("Some One Else " & System.Environment.TickCount)
			Me.listViewSimple.AddObject(person)
			Me.listViewSimple.EnsureModelVisible(person)
		End Sub

		Private Sub Button6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button6.Click
			Me.listViewSimple.RemoveObject(Me.listViewSimple.SelectedObject)
		End Sub

		#End Region

		#Region "Complex Tab Event Handlers"

		Private Sub CheckBox1CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBox1.CheckedChanged
			ShowGroupsChecked(Me.listViewComplex, CType(sender, CheckBox))
		End Sub

		Private Sub CheckBox2CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBox2.CheckedChanged
			ShowLabelsOnGroupsChecked(Me.listViewComplex, CType(sender, CheckBox))
		End Sub

		Private Sub Button2Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button2.Click
			Me.TimedRebuildList(Me.listViewComplex)
		End Sub

		Private Sub Button5Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button5.Click
			Me.listViewComplex.CopySelectionToClipboard()
			listViewComplex.SelectedObjects = listViewSimple.SelectedObjects
			listViewComplex.Select()
		End Sub

		Private Sub CheckBox6CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox6.CheckedChanged
			If comboBox1.SelectedIndex = 3 AndAlso Me.checkBox6.Checked Then
				Me.listViewComplex.TileSize = New Size(250, 120)
			End If

			ChangeOwnerDrawn(Me.listViewComplex, CType(sender, CheckBox))
		End Sub

		Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox1.SelectedIndexChanged
			If comboBox1.SelectedIndex = 3 AndAlso Me.listViewComplex.OwnerDraw Then
				Me.listViewComplex.TileSize = New Size(250, 120)
			End If

			Me.ChangeView(Me.listViewComplex, CType(sender, ComboBox))
		End Sub
		#End Region

		#Region "Dataset Tab Event Handlers"

		Private Sub CheckBox7CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBox7.CheckedChanged
			ShowGroupsChecked(Me.listViewDataSet, CType(sender, CheckBox))
		End Sub

		Private Sub CheckBox8CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBox8.CheckedChanged
			ShowLabelsOnGroupsChecked(Me.listViewDataSet, CType(sender, CheckBox))
		End Sub

		Private Sub Button3Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button3.Click
			Me.TimedReloadXml()
		End Sub

		Private Sub CheckBox5CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox5.CheckedChanged
			ChangeOwnerDrawn(Me.listViewDataSet, CType(sender, CheckBox))
		End Sub

		Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox3.SelectedIndexChanged
			Me.ChangeView(Me.listViewDataSet, CType(sender, ComboBox))
		End Sub

		Private Sub ListViewDataSetSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listViewDataSet.SelectedIndexChanged
			Dim listView As ObjectListView = CType(sender, ObjectListView)
			Dim row As DataRowView = CType(listView.GetSelectedObject(), DataRowView)
			Dim msg As String
			If row Is Nothing Then
				msg = listView.SelectedIndices.Count.ToString()
			Else
        msg = "'" & CStr(row("Name")) & "'"
			End If
			Me.toolStripStatusLabel1.Text = String.Format("Selected {0} of {1} items", msg, listView.Items.Count)

		End Sub

		Private Sub RowHeightUpDown_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rowHeightUpDown.ValueChanged
			Me.listViewDataSet.RowHeight = Decimal.ToInt32(Me.rowHeightUpDown.Value)
		End Sub

		Private Sub CheckBoxPause_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBoxPause.CheckedChanged
			Me.listViewDataSet.PauseAnimations((CType(sender, CheckBox)).Checked)
		End Sub

		#End Region

		#Region "Virtual Tab Event Handlers"

		Private Sub CheckBox9CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox9.CheckedChanged
			Me.ChangeOwnerDrawn(Me.listViewVirtual, CType(sender, CheckBox))
		End Sub

		Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox2.SelectedIndexChanged
			Me.ChangeView(Me.listViewVirtual, CType(sender, ComboBox))
		End Sub

		Private Sub Button8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button8.Click
			Me.listViewVirtual.SelectAll()
		End Sub

		Private Sub Button9_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button9.Click
			Me.listViewVirtual.DeselectAll()
		End Sub

		#End Region

		#Region "Explorer Tab event handlers"

		Private Sub TextBoxFolderPathTextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles textBoxFolderPath.TextChanged
			If Directory.Exists(Me.textBoxFolderPath.Text) Then
				Me.textBoxFolderPath.ForeColor = Color.Black
				Me.buttonGo.Enabled = True
				Me.buttonUp.Enabled = True
			Else
				Me.textBoxFolderPath.ForeColor = Color.Red
				Me.buttonGo.Enabled = False
				Me.buttonUp.Enabled = False
			End If
		End Sub

		Private Sub ButtonGoClick(ByVal sender As Object, ByVal e As EventArgs) Handles buttonGo.Click
			Dim path As String = Me.textBoxFolderPath.Text
			Me.PopulateListFromPath(path)
		End Sub

		Private Sub PopulateListFromPath(ByVal path As String)
			Dim pathInfo As DirectoryInfo = New DirectoryInfo(path)
			If (Not pathInfo.Exists) Then
				Return
			End If

			Dim sw As Stopwatch = New Stopwatch()

			Cursor.Current = Cursors.WaitCursor
			sw.Start()
			Me.listViewFiles.SetObjects(pathInfo.GetFileSystemInfos())
			sw.Stop()
			Cursor.Current = Cursors.Default

			Dim msPerItem As Single
			If listViewFiles.Items.Count = 0 Then
				msPerItem = (0)
			Else
				msPerItem = (CSng(sw.ElapsedMilliseconds) / listViewFiles.Items.Count)
			End If
			Me.toolStripStatusLabel1.Text = String.Format("Timed build: {0} items in {1}ms ({2:F}ms per item)", listViewFiles.Items.Count, sw.ElapsedMilliseconds, msPerItem)
		End Sub

		Private Sub CheckBox12CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox12.CheckedChanged
			ShowGroupsChecked(Me.listViewFiles, CType(sender, CheckBox))
		End Sub

		Private Sub CheckBox11CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox11.CheckedChanged
			Me.ShowLabelsOnGroupsChecked(Me.listViewFiles, CType(sender, CheckBox))
		End Sub

		Private Sub CheckBox10CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox10.CheckedChanged
			Me.ChangeOwnerDrawn(Me.listViewFiles, CType(sender, CheckBox))
		End Sub

		Private Sub ComboBox4SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox4.SelectedIndexChanged
		   Me.ChangeView(Me.listViewFiles, CType(sender, ComboBox))
		   Me.button13.Enabled = (Me.listViewFiles.View = View.Details OrElse Me.listViewFiles.View = View.Tile)
		End Sub

		Private Sub ListViewFiles_ItemActivate(ByVal sender As Object, ByVal e As EventArgs) Handles listViewFiles.ItemActivate
			Dim rowObject As Object = Me.listViewFiles.SelectedObject
			If rowObject Is Nothing Then
				Return
			End If

			If TypeOf rowObject Is DirectoryInfo Then
				Me.textBoxFolderPath.Text = (CType(rowObject, DirectoryInfo)).FullName
				Me.buttonGo.PerformClick()
			Else
				ShellUtilities.Execute((CType(rowObject, FileInfo)).FullName)
			End If
		End Sub

		Private Sub TextBoxFolderPath_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles textBoxFolderPath.KeyPress
			If e.KeyChar = ChrW(13) Then
				Me.buttonGo.PerformClick()
				e.Handled = True
			End If
		End Sub

		Private Sub ButtonUp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonUp.Click
			Dim di As DirectoryInfo = Directory.GetParent(Me.textBoxFolderPath.Text)
			If di Is Nothing Then
				System.Media.SystemSounds.Asterisk.Play()
			Else
				Me.textBoxFolderPath.Text = di.FullName
				Me.buttonGo.PerformClick()
			End If
		End Sub

		Private fileListViewState As Byte()

		Private Sub ButtonSaveState_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSaveState.Click
			Me.fileListViewState = Me.listViewFiles.SaveState()
			Me.buttonRestoreState.Enabled = True
		End Sub

		Private Sub ButtonRestoreState_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonRestoreState.Click
			Me.listViewFiles.RestoreState(Me.fileListViewState)
		End Sub

		#End Region

		#Region "ListView printing"

		Private Sub Button10_Click_1(ByVal sender As Object, ByVal e As EventArgs) Handles button10.Click
			Me.listViewPrinter1.PageSetup()
			Me.UpdatePrintPreview()
		End Sub

		Private Sub Button11_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button11.Click
			Me.listViewPrinter1.PrintPreview()
		End Sub

		Private Sub Button12_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button12.Click
			If rbShowVirtual.Checked = True Then
				Dim msg As String = "Be careful when printing the virtual list." & Constants.vbLf + Constants.vbLf & "It contains 10 million rows. If you select to print all pages, it will try to print 250,000 pages! That seems little excessive for a demo." & Constants.vbLf + Constants.vbLf & "You have been warned."
				MessageBox.Show(msg, "Be careful", MessageBoxButtons.OK, MessageBoxIcon.Warning)
			End If
			Me.listViewPrinter1.PrintWithDialog()
		End Sub

		Private Sub Button13Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.UpdatePrintPreview()
		End Sub

		Private Sub UpdatePreview(ByVal sender As Object, ByVal e As EventArgs) Handles numericUpDown2.ValueChanged, numericUpDown1.ValueChanged, cbCellGridLines.CheckedChanged, rbStyleTooMuch.CheckedChanged, cbPrintOnlySelection.CheckedChanged, rbStyleModern.CheckedChanged, cbShrinkToFit.CheckedChanged, rbStyleMinimal.CheckedChanged, cbIncludeImages.CheckedChanged, rbShowFileExplorer.CheckedChanged, rbShowDataset.CheckedChanged, rbShowComplex.CheckedChanged, rbShowSimple.CheckedChanged
			Me.UpdatePrintPreview()
		End Sub

		Private Sub UpdatePrintPreview()
			If Me.rbShowSimple.Checked = True Then
				Me.listViewPrinter1.ListView = Me.listViewSimple
			ElseIf Me.rbShowComplex.Checked = True Then
				Me.listViewPrinter1.ListView = Me.listViewComplex
			ElseIf Me.rbShowDataset.Checked = True Then
				Me.listViewPrinter1.ListView = Me.listViewDataSet
			ElseIf Me.rbShowVirtual.Checked = True Then
				Me.listViewPrinter1.ListView = Me.listViewVirtual
			ElseIf Me.rbShowFileExplorer.Checked = True Then
				Me.listViewPrinter1.ListView = Me.listViewFiles
			End If

			Me.listViewPrinter1.DocumentName = Me.tbTitle.Text
			Me.listViewPrinter1.Header = Me.tbHeader.Text.Replace("\t", Constants.vbTab)
			Me.listViewPrinter1.Footer = Me.tbFooter.Text.Replace("\t", Constants.vbTab)
			Me.listViewPrinter1.Watermark = Me.tbWatermark.Text

			Me.listViewPrinter1.IsShrinkToFit = Me.cbShrinkToFit.Checked
			Me.listViewPrinter1.IsTextOnly = Not Me.cbIncludeImages.Checked
			Me.listViewPrinter1.IsPrintSelectionOnly = Me.cbPrintOnlySelection.Checked

			If Me.rbStyleMinimal.Checked = True Then
				Me.ApplyMinimalFormatting()
			ElseIf Me.rbStyleModern.Checked = True Then
				Me.ApplyModernFormatting()
			ElseIf Me.rbStyleTooMuch.Checked = True Then
				Me.ApplyOverTheTopFormatting()
			End If

			If Me.cbCellGridLines.Checked = False Then
				Me.listViewPrinter1.ListGridPen = Nothing
			End If

			Me.listViewPrinter1.FirstPage = CInt(Fix(Me.numericUpDown1.Value))
			Me.listViewPrinter1.LastPage = CInt(Fix(Me.numericUpDown2.Value))

			Me.printPreviewControl1.InvalidatePreview()
		End Sub

		''' <summary>
		''' Give the report a minimal set of default formatting values.
		''' </summary>
		Public Sub ApplyMinimalFormatting()
			Me.listViewPrinter1.CellFormat = Nothing
			Me.listViewPrinter1.ListFont = New Font("Tahoma", 9)

			Me.listViewPrinter1.HeaderFormat = BlockFormat.Header()
			Me.listViewPrinter1.HeaderFormat.TextBrush = Brushes.Black
			Me.listViewPrinter1.HeaderFormat.BackgroundBrush = Nothing
			Me.listViewPrinter1.HeaderFormat.SetBorderPen(Sides.Bottom, New Pen(Color.Black, 0.5f))

			Me.listViewPrinter1.FooterFormat = BlockFormat.Footer()
			Me.listViewPrinter1.GroupHeaderFormat = BlockFormat.GroupHeader()
			Dim brush As Brush = New LinearGradientBrush(New Point(0, 0), New Point(200, 0), Color.Gray, Color.White)
			Me.listViewPrinter1.GroupHeaderFormat.SetBorder(Sides.Bottom, 2, brush)

			Me.listViewPrinter1.ListHeaderFormat = BlockFormat.ListHeader()
			Me.listViewPrinter1.ListHeaderFormat.BackgroundBrush = Nothing

			Me.listViewPrinter1.WatermarkFont = Nothing
			Me.listViewPrinter1.WatermarkColor = Color.Empty
		End Sub

		''' <summary>
		''' Give the report a minimal set of default formatting values.
		''' </summary>
		Public Sub ApplyModernFormatting()
			Me.listViewPrinter1.CellFormat = Nothing
			Me.listViewPrinter1.ListFont = New Font("Ms Sans Serif", 9)
			Me.listViewPrinter1.ListGridPen = New Pen(Color.DarkGray, 0.5f)

			Me.listViewPrinter1.HeaderFormat = BlockFormat.Header(New Font("Verdana", 24, FontStyle.Bold))
			Me.listViewPrinter1.HeaderFormat.BackgroundBrush = New LinearGradientBrush(New Point(0, 0), New Point(200, 0), Color.DarkBlue, Color.White)

			Me.listViewPrinter1.FooterFormat = BlockFormat.Footer()
			Me.listViewPrinter1.FooterFormat.BackgroundBrush = New LinearGradientBrush(New Point(0, 0), New Point(200, 0), Color.White, Color.Blue)

			Me.listViewPrinter1.GroupHeaderFormat = BlockFormat.GroupHeader()
			Me.listViewPrinter1.ListHeaderFormat = BlockFormat.ListHeader(New Font("Verdana", 12))

			Me.listViewPrinter1.WatermarkFont = Nothing
			Me.listViewPrinter1.WatermarkColor = Color.Empty
		End Sub

		''' <summary>
		''' Give the report a minimal set of default formatting values.
		''' </summary>
		Public Sub ApplyOverTheTopFormatting()
			Me.listViewPrinter1.CellFormat = Nothing
			Me.listViewPrinter1.ListFont = New Font("Ms Sans Serif", 9)
			Me.listViewPrinter1.ListGridPen = New Pen(Color.Blue, 0.5f)

			Me.listViewPrinter1.HeaderFormat = BlockFormat.Header(New Font("Comic Sans MS", 36))
			Me.listViewPrinter1.HeaderFormat.TextBrush = New LinearGradientBrush(New Point(0, 0), New Point(900, 0), Color.Black, Color.Blue)
			Me.listViewPrinter1.HeaderFormat.BackgroundBrush = New TextureBrush(Resource1.star16, WrapMode.Tile)
			Me.listViewPrinter1.HeaderFormat.SetBorder(Sides.All, 10, New LinearGradientBrush(New Point(0, 0), New Point(300, 0), Color.Purple, Color.Pink))

			Me.listViewPrinter1.FooterFormat = BlockFormat.Footer(New Font("Comic Sans MS", 12))
			Me.listViewPrinter1.FooterFormat.TextBrush = Brushes.Blue
			Me.listViewPrinter1.FooterFormat.BackgroundBrush = New LinearGradientBrush(New Point(0, 0), New Point(200, 0), Color.Gold, Color.Green)
			Me.listViewPrinter1.FooterFormat.SetBorderPen(Sides.All, New Pen(Color.FromArgb(128, Color.Green), 5))

			Me.listViewPrinter1.GroupHeaderFormat = BlockFormat.GroupHeader()
			Dim brush As Brush = New HatchBrush(HatchStyle.LargeConfetti, Color.Blue, Color.Empty)
			Me.listViewPrinter1.GroupHeaderFormat.SetBorder(Sides.Bottom, 5, brush)

			Me.listViewPrinter1.ListHeaderFormat = BlockFormat.ListHeader(New Font("Comic Sans MS", 12))
			Me.listViewPrinter1.ListHeaderFormat.BackgroundBrush = Brushes.PowderBlue
			Me.listViewPrinter1.ListHeaderFormat.TextBrush = Brushes.Black

			Me.listViewPrinter1.WatermarkFont = New Font("Comic Sans MS", 72)
			Me.listViewPrinter1.WatermarkColor = Color.Red
		End Sub

		Private Sub ListViewPrinter1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles listViewPrinter1.PrintPage
			Me.toolStripStatusLabel1.Text = String.Format("Printing page #{0}...", Me.listViewPrinter1.PageNumber)
			Me.Update()
		End Sub

		Private Sub ListViewPrinter1_EndPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles listViewPrinter1.EndPrint
			Me.toolStripStatusLabel1.Text = "Printing done"
		End Sub

		Private Sub TabControl1_Selected(ByVal sender As Object, ByVal e As TabControlEventArgs) Handles tabControl1.Selected
			If e.TabPageIndex = 5 Then
				Me.UpdatePrintPreview()
			End If
		End Sub

		#End Region

		#Region "Cell editing example"

		Private Sub ListViewComplex_CellEditStarting(ByVal sender As Object, ByVal e As CellEditEventArgs) Handles listViewComplex.CellEditStarting
			' We only want to mess with the Cooking Skill column
			If e.Column.Text <> "Cooking skill" Then
				Return
			End If

			Dim cb As ComboBox = New ComboBox()
			cb.Bounds = e.CellBounds
			cb.Font = (CType(sender, ObjectListView)).Font
			cb.DropDownStyle = ComboBoxStyle.DropDownList
			cb.Items.AddRange(New String() { "Pay to eat out", "Suggest take-away", "Passable", "Seek dinner invitation", "Hire as chef" })
      cb.SelectedIndex = CInt(Math.Max(0, Math.Min(cb.Items.Count - 1, (CInt(Fix(e.Value))) / 10)))
			AddHandler cb.SelectedIndexChanged, AddressOf cb_SelectedIndexChanged
			cb.Tag = e.RowObject ' remember which person we are editing
			e.Control = cb
		End Sub

		Private Sub Cb_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
			Dim cb As ComboBox = CType(sender, ComboBox)
			CType(cb.Tag, Person).CulinaryRating = cb.SelectedIndex * 10
		End Sub

		Private Sub ListViewComplex_CellEditValidating(ByVal sender As Object, ByVal e As CellEditEventArgs) Handles listViewComplex.CellEditValidating
			' Disallow professions from starting with "a" or "z" -- just to be arbitrary
			If e.Column.Text = "Occupation" Then
				Dim newValue As String = (CType(e.Control, TextBox)).Text
				If newValue.ToLowerInvariant().StartsWith("a") OrElse newValue.ToLowerInvariant().StartsWith("z") Then
					e.Cancel = True
					MessageBox.Show( "Occupations cannot begin with 'a' or 'z' (just to show cell edit validation at work).", "ObjectListViewDemo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
				End If
			End If

			' Disallow birthdays from being on the 29th -- just to be arbitrary
			If e.Column.Text = "Birthday" Then
				Dim newValue As DateTime = (CType(e.Control, DateTimePicker)).Value
        If newValue.Day = 29 Then
          e.Cancel = True
          MessageBox.Show( "Sorry. Birthdays cannot be on 29th of any month (just to show cell edit validation at work).", "ObjectListViewDemo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If
			End If

		End Sub

		Private Sub ListViewComplex_CellEditFinishing(ByVal sender As Object, ByVal e As CellEditEventArgs) Handles listViewComplex.CellEditFinishing
			' We only want to mess with the Cooking Skill column
			If e.Column.Text <> "Cooking skill" Then
				Return
			End If

			' Stop listening for change events
			RemoveHandler (CType(e.Control, ComboBox)).SelectedIndexChanged, AddressOf cb_SelectedIndexChanged

			' Any updating will have been down in the SelectedIndexChanged event handler
			' Here we simply make the list redraw the involved ListViewItem
			CType(sender, ObjectListView).RefreshItem(e.ListViewItem)

			' We have updated the model object, so we cancel the auto update
			e.Cancel = True
		End Sub

		Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox5.SelectedIndexChanged
			Me.ChangeEditable(Me.listViewComplex, CType(sender, ComboBox))
		End Sub

		Private Sub ComboBox6_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox6.SelectedIndexChanged
			Me.ChangeEditable(Me.listViewSimple, CType(sender, ComboBox))
		End Sub

		Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox7.SelectedIndexChanged
			Me.ChangeEditable(Me.listViewDataSet, CType(sender, ComboBox))
		End Sub

		Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox8.SelectedIndexChanged
			Me.ChangeEditable(Me.listViewVirtual, CType(sender, ComboBox))
		End Sub

		Private Sub ChangeEditable(ByVal objectListView As ObjectListView, ByVal comboBox As ComboBox)
			If comboBox.Text = "No" Then
				objectListView.CellEditActivation = ObjectListView.CellEditActivateMode.None
			ElseIf comboBox.Text = "Single Click" Then
				objectListView.CellEditActivation = ObjectListView.CellEditActivateMode.SingleClick
			ElseIf comboBox.Text = "Double Click" Then
				objectListView.CellEditActivation = ObjectListView.CellEditActivateMode.DoubleClick
			Else
				objectListView.CellEditActivation = ObjectListView.CellEditActivateMode.F2Only
			End If
		End Sub

		#End Region

		Private Sub InitializeFastListExample(ByVal list As List(Of Person))
			Me.olvFastList.BooleanCheckStateGetter = AddressOf AnonymousMethod42
			Me.olvFastList.BooleanCheckStatePutter = AddressOf AnonymousMethod43
			Me.olvColumn18.AspectGetter = AddressOf AnonymousMethod44

			Me.olvColumn18.ImageGetter = AddressOf AnonymousMethod45

			Me.olvColumn19.AspectGetter = AddressOf AnonymousMethod46
			Me.olvColumn26.AspectGetter = AddressOf AnonymousMethod47
			Me.olvColumn26.Renderer = New MultiImageRenderer(Resource1.star16, 5, 0, 40)

			Me.olvColumn27.AspectGetter = AddressOf AnonymousMethod48

			Me.olvColumn28.AspectGetter = AddressOf AnonymousMethod49
			Me.olvColumn28.ImageGetter = AddressOf AnonymousMethod50
			Me.olvColumn28.Renderer = New BaseRenderer()

			Me.olvColumn29.AspectGetter = AddressOf AnonymousMethod51
			Me.olvColumn29.AspectPutter = AddressOf AnonymousMethod52

			Me.olvColumn31.AspectGetter = AddressOf AnonymousMethod53
			Me.olvColumn31.Renderer = New MappedImageRenderer(New Object() { "Little", Resource1.down16, "Medium", Resource1.tick16, "Lots", Resource1.star16 })
			Me.olvColumn32.AspectGetter = AddressOf AnonymousMethod54
			Me.olvColumn32.AspectToStringConverter = AddressOf AnonymousMethod55
			Me.olvColumn33.AspectGetter = AddressOf AnonymousMethod56

			comboBox9.SelectedIndex = 0
			comboBox10.SelectedIndex = 4

			Me.olvFastList.SetObjects(list)
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod42(ByVal x As Object) As Boolean
      Return (CType(x, Person)).IsActive
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod43(ByVal x As Object, ByVal newValue As Boolean) As Boolean
      CType(x, Person).IsActive = newValue
      Return newValue
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod44(ByVal x As Object) As Object
			Return (CType(x, Person)).Name
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod45(ByVal row As Object) As Object
			If "AEIOU".Contains((CType(row, Person)).Name.Substring(0, 1)) Then
			Return 0 ' star
			ElseIf (CType(row, Person)).Name.CompareTo("N") < 0 Then
			Return 1 ' heart
			Else
			Return 2 ' music
			End If
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod46(ByVal x As Object) As Object
			Return (CType(x, Person)).Occupation
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod47(ByVal x As Object) As Object
			Return (CType(x, Person)).CulinaryRating
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod48(ByVal x As Object) As Object
			Return (CType(x, Person)).YearOfBirth
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod49(ByVal x As Object) As Object
			Return (CType(x, Person)).BirthDate
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod50(ByVal row As Object) As Object
			Dim p As Person = CType(row, Person)
      If Not p Is Nothing AndAlso (p.BirthDate.Year Mod 10) = 4 Then
        Return 3
      Else
        Return -1 ' no image
      End If
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod51(ByVal x As Object) As Object
			Return (CType(x, Person)).GetRate()
		End Function
		Private Sub AnonymousMethod52(ByVal x As Object, ByVal newValue As Object)
			CType(x, Person).SetRate(CDbl(newValue))
		End Sub
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod53(ByVal row As Object) As Object
			If (CType(row, Person)).GetRate() < 100 Then
			Return "Little"
			End If
			If (CType(row, Person)).GetRate() > 1000 Then
			Return "Lots"
			End If
			Return "Medium"
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod54(ByVal row As Object) As Object
			Return DateTime.Now - (CType(row, Person)).BirthDate
		End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
    Private Function AnonymousMethod55(ByVal aspect As Object) As String
      Return (CType(aspect, TimeSpan)).Days.ToString()
    End Function
		'TO_DO: INSTANT VB TO_DO TASK: The return type of this anonymous method could not be determined by Instant VB:
		Private Function AnonymousMethod56(ByVal row As Object) As Object
			Return (CType(row, Person)).CanTellJokes
		End Function

		Private Sub Button14_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button14.Click
			Dim l As ArrayList = New ArrayList()
			Do While l.Count < 1000
				For Each x As Person In Me.masterList
					l.Add(New Person(x))
				Next x
			Loop

			Dim stopWatch As Stopwatch = New Stopwatch()
			Try
				Me.Cursor = Cursors.WaitCursor
				stopWatch.Start()
				Me.olvFastList.AddObjects(l)
			Finally
				stopWatch.Stop()
				Me.Cursor = Cursors.Default
			End Try

			Me.takeNoticeOfSelectionEvent = False
			Me.toolStripStatusLabel1.Text = String.Format("Build time: {0} items in {1}ms, average per item: {2:F}ms", Me.olvFastList.Items.Count, stopWatch.ElapsedMilliseconds, CSng(stopWatch.ElapsedMilliseconds) / Me.olvFastList.Items.Count)
		End Sub
		Private takeNoticeOfSelectionEvent As Boolean = True

		Private Sub CheckBox13_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox13.CheckedChanged
			ChangeOwnerDrawn(Me.olvFastList, CType(sender, CheckBox))
		End Sub

		Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox9.SelectedIndexChanged
			ChangeEditable(Me.olvFastList, CType(sender, ComboBox))
		End Sub

		Private Sub ComboBox10_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles comboBox10.SelectedIndexChanged
			ChangeView(Me.olvFastList, CType(sender, ComboBox))
		End Sub

		Private Sub Button15_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button15.Click
			Me.olvFastList.SetObjects(Nothing)
			Me.HandleSelectionChanged(Me.olvFastList)
		End Sub

		Private Sub Button13_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button13.Click
			Dim form As ColumnSelectionForm = New ColumnSelectionForm()
			form.OpenOn(Me.listViewFiles)
		End Sub

		Private Sub OlvFastList_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles olvFastList.SelectionChanged
			If Me.takeNoticeOfSelectionEvent Then
				Me.HandleSelectionChanged(CType(sender, ObjectListView))
			End If

			Me.takeNoticeOfSelectionEvent = True
		End Sub

		Private Sub ListViewVirtual_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles listViewVirtual.SelectionChanged
			Me.HandleSelectionChanged(CType(sender, ObjectListView))
		End Sub

		Private Sub Button16_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button16.Click
			Dim list As List(Of Person) = New List(Of Person)()
			list.Add(New Person("A New Person"))
			list.Add(New Person("Brave New Person"))
			list.Add(New Person("someone like e e cummings"))
			list.Add(New Person("Luis Nova Pessoa"))

			' Give him a birthday that will display an image to make sure the image appears.
			list(list.Count - 1).BirthDate = New DateTime(1984, 12, 25)

			Me.listViewComplex.AddObjects(list)
		End Sub

		Private Sub Button17_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button17.Click
			Me.listViewComplex.RemoveObjects(Me.listViewComplex.SelectedObjects)
		End Sub

		Private Sub Button18Click(ByVal sender As Object, ByVal e As EventArgs) Handles button18.Click
			Me.olvFastList.RemoveObjects(Me.olvFastList.SelectedObjects)
		End Sub

		Private Sub CheckBox15_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox15.CheckedChanged
			If (CType(sender, CheckBox)).Checked Then
				If Me.listViewSimple.ShowGroups Then
					Me.listViewSimple.AlwaysGroupByColumn = Me.listViewSimple.LastSortColumn
					Me.listViewSimple.AlwaysGroupBySortOrder = Me.listViewSimple.LastSortOrder
				End If
			Else
				Me.listViewSimple.AlwaysGroupByColumn = Nothing
				Me.listViewSimple.AlwaysGroupBySortOrder = SortOrder.None
			End If
		End Sub

		Private Sub Button19_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button19.Click
			Me.olvFastList.CopyObjectsToClipboard(Me.olvFastList.CheckedObjects)
		End Sub

		Private Sub Button20_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.olvFastList.EditSubItem(Me.olvFastList.GetItem(5), 1)
		End Sub

		Private Sub TreeListView_ItemActivate(ByVal sender As Object, ByVal e As EventArgs) Handles treeListView.ItemActivate
			Dim model As Object = Me.treeListView.SelectedObject
			If Not model Is Nothing Then
				Me.treeListView.ToggleExpansion(model)
			End If
		End Sub

		Private treeListViewState As Byte()

		Private Sub Button25_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button25.Click
			Me.treeListViewState = Me.treeListView.SaveState()
			Me.button26.Enabled = True
		End Sub

		Private Sub Button26_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button26.Click
			Me.treeListView.RestoreState(Me.treeListViewState)
		End Sub

		Private Sub Button27_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button27.Click
			Dim form As ColumnSelectionForm = New ColumnSelectionForm()
			form.OpenOn(Me.treeListView)
		End Sub

		Private Sub Button28_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.treeListView.RefreshObjects(Me.treeListView.SelectedObjects)
		End Sub

		Private Sub ListViewComplex_MouseClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles listViewComplex.MouseClick
			If e.Button <> MouseButtons.Right Then
				Return
			End If

			Dim ms As ContextMenuStrip = New ContextMenuStrip()
			AddHandler ms.ItemClicked, AddressOf ms_ItemClicked

			Dim olv As ObjectListView = CType(sender, ObjectListView)
			If olv.ShowGroups Then
				For Each lvg As ListViewGroup In olv.Groups
					Dim mi As ToolStripMenuItem = New ToolStripMenuItem(String.Format("Jump to group '{0}'", lvg.Header))
					mi.Tag = lvg
					ms.Items.Add(mi)
				Next lvg
			Else
				Dim mi As ToolStripMenuItem = New ToolStripMenuItem("Turn on 'Show Groups' to see this context menu in action")
				mi.Enabled = False
				ms.Items.Add(mi)
			End If

			ms.Show(CType(sender, Control), e.X, e.Y)
		End Sub

		Private Sub Ms_ItemClicked(ByVal sender As Object, ByVal e As ToolStripItemClickedEventArgs)
			Dim mi As ToolStripMenuItem = CType(e.ClickedItem, ToolStripMenuItem)
			Dim lvg As ListViewGroup = CType(mi.Tag, ListViewGroup)
			Dim olv As ObjectListView = CType(lvg.ListView, ObjectListView)
			olv.EnsureGroupVisible(lvg)
		End Sub
		'
		'private static void BlendBitmaps(Graphics g, Bitmap b1, Bitmap b2, float transition)
		'{
			'float[][] colorMatrixElements = {
   'new float[] {1,  0,  0,  0, 0},        // red scaling factor of 2
   'new float[] {0,  1,  0,  0, 0},        // green scaling factor of 1
   'new float[] {0,  0,  1,  0, 0},        // blue scaling factor of 1
   'new float[] {0,  0,  0,  transition, 0},        // alpha scaling factor of 1
   'new float[] {0,  0,  0,  0, 1}};    // three translations of 0.2

			'ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);
			'ImageAttributes imageAttributes = new ImageAttributes();
			'imageAttributes.SetColorMatrix(colorMatrix);

			'g.DrawImage(
			   'b1,
			   'new Rectangle(0, 0, b1.Size.Width, b1.Size.Height),  // destination rectangle
			   '0, 0,        // upper-left corner of source rectangle
			   'b1.Size.Width,       // width of source rectangle
			   'b1.Size.Height,      // height of source rectangle
			   'GraphicsUnit.Pixel,
			   'imageAttributes);

			'colorMatrix.Matrix33 = 1.0f - transition;
			'imageAttributes.SetColorMatrix(colorMatrix);

			'g.DrawImage(
			   'b2,
			   'new Rectangle(0, 0, b2.Size.Width, b2.Size.Height),  // destination rectangle
			   '0, 0,        // upper-left corner of source rectangle
			   'b2.Size.Width,       // width of source rectangle
			   'b2.Size.Height,      // height of source rectangle
			   'GraphicsUnit.Pixel,
			   'imageAttributes);
		'}
		'
		Private Sub CheckBox18_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox18.CheckedChanged
			Me.listViewSimple.UseHotItem = (CType(sender, CheckBox)).Checked
		End Sub

		Private Sub TreeListView_ItemChecked(ByVal sender As Object, ByVal e As ItemCheckedEventArgs) Handles treeListView.ItemChecked
			System.Diagnostics.Debug.WriteLine("tree checked")
		End Sub

		Private Sub TreeListView_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles treeListView.ItemCheck
			System.Diagnostics.Debug.WriteLine("tree check")
		End Sub

		Private Sub ListViewSimple_ItemChecked(ByVal sender As Object, ByVal e As ItemCheckedEventArgs) Handles listViewSimple.ItemChecked
			System.Diagnostics.Debug.WriteLine("simple checked")
		End Sub

		Private Sub OlvFastList_ItemChecked(ByVal sender As Object, ByVal e As ItemCheckedEventArgs) Handles olvFastList.ItemChecked
			System.Diagnostics.Debug.WriteLine("fast checked")
		End Sub

		Private Sub ListViewSimple_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles listViewSimple.ItemCheck
			System.Diagnostics.Debug.WriteLine("simple check")
		End Sub

		Private Sub OlvFastList_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles olvFastList.ItemCheck
			System.Diagnostics.Debug.WriteLine("fast check")
		End Sub
	End Class

	Friend Enum MaritalStatus
		[Single]
		Married
		Divorced
		Partnered
	End Enum

	Friend Class Person
		Public IsActive As Boolean = True

		Public Sub New(ByVal name As String)
			Me.name_Renamed = name
		End Sub

		Public Sub New(ByVal name As String, ByVal occupation As String, ByVal culinaryRating As Integer, ByVal birthDate As DateTime, ByVal hourlyRate As Double, ByVal canTellJokes As Boolean, ByVal photo As String, ByVal comments As String)
			Me.name_Renamed = name
			Me.Occupation = occupation
			Me.culinaryRating_Renamed = culinaryRating
			Me.birthDate_Renamed = birthDate
			Me.hourlyRate = hourlyRate
			Me.CanTellJokes = canTellJokes
			Me.Comments = comments
			Me.Photo = photo
		End Sub

		Public Sub New(ByVal other As Person)
			Me.name_Renamed = other.Name
			Me.Occupation = other.Occupation
			Me.culinaryRating_Renamed = other.CulinaryRating
			Me.birthDate_Renamed = other.BirthDate
			Me.hourlyRate = other.GetRate()
			Me.CanTellJokes = other.CanTellJokes
			Me.Photo = other.Photo
			Me.Comments = other.Comments
			Me.MaritalStatus = other.MaritalStatus
		End Sub

		' Allows tests for properties.
		Public Property Name() As String
			Get
				Return name_Renamed
			End Get
			Set(ByVal value As String)
				name_Renamed = value
			End Set
		End Property
		Private name_Renamed As String

		Public Property Occupation() As String
			Get
				Return occupation_Renamed
			End Get
			Set(ByVal value As String)
				occupation_Renamed = value
			End Set
		End Property
		Private occupation_Renamed As String

		Public Property CulinaryRating() As Integer
			Get
				Return culinaryRating_Renamed
			End Get
			Set(ByVal value As Integer)
				culinaryRating_Renamed = value
			End Set
		End Property
		Private culinaryRating_Renamed As Integer

		Public Property BirthDate() As DateTime
			Get
				Return birthDate_Renamed
			End Get
			Set(ByVal value As DateTime)
				birthDate_Renamed = value
			End Set
		End Property
		Private birthDate_Renamed As DateTime

		Public Property YearOfBirth() As Integer
			Get
				Return Me.BirthDate.Year
			End Get
			Set(ByVal value As Integer)
				Me.BirthDate = New DateTime(value, birthDate_Renamed.Month, birthDate_Renamed.Day)
			End Set
		End Property

		' Allow tests for methods
		Public Function GetRate() As Double
			Return hourlyRate
		End Function
		Private hourlyRate As Double

		Public Sub SetRate(ByVal value As Double)
			hourlyRate = value
		End Sub

		' Allows tests for fields.
		Public Photo As String
		Public Comments As String
		Public serialNumber As Integer
		Public CanTellJokes As Nullable(Of Boolean)

		' Allow tests for enums
		Public MaritalStatus As MaritalStatus = MaritalStatus.Single
	End Class

End Namespace
