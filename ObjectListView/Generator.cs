﻿/*
 * Generator - Utility methods that generate columns or methods
 *
 * Author: Phillip Piper
 * Date: 15/08/2009 22:37
 *
 * Change log:
 * v2.4
 * 2010-04-14  JPP  - Allow Name property to be set
 *                  - Don't double set the Text property
 * v2.3
 * 2009-08-15  JPP  - Initial version
 *
 * To do:
 * 
 * Copyright (C) 2009-2010 Phillip Piper
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Core., either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you wish to use this code in a closed source application, please contact phillip_piper@bigfoot.com.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace BrightIdeasSoftware
{
    public static class Generator
    {
        static public void GenerateColumns(ObjectListView olv, IEnumerable enumerable) {
            // Generate columns based on the type of the first model in the collection and then quit
            foreach (object model in enumerable) {
                Generator.GenerateColumns(olv, model.GetType());
                return;
            }

            // If we reach here, the collection was empty, so we clear the list
            Generator.ReplaceColumns(olv, new List<OLVColumn>());
        }

        static public void GenerateColumns(ObjectListView olv, Type type) {
            IList<OLVColumn> columns = Generator.GenerateColumns(type);
            Generator.ReplaceColumns(olv, columns);
        }

        static private void ReplaceColumns(ObjectListView olv, IList<OLVColumn> columns) {
            olv.Clear();
            olv.AllColumns.Clear();
            if (columns.Count > 0) {
                olv.AllColumns.AddRange(columns);
                olv.RebuildColumns();
            }
        }

        static public IList<OLVColumn> GenerateColumns(Type type) {
            List<OLVColumn> columns = new List<OLVColumn>();
            
            // Iterate all public properties in the class and build columns from those that have
            // an OLVColumn attribute.
            foreach (PropertyInfo pinfo in type.GetProperties()) {
                OLVColumnAttribute attr = Attribute.GetCustomAttribute(pinfo, typeof(OLVColumnAttribute)) as OLVColumnAttribute;
                if (attr != null)
                    columns.Add(Generator.MakeColumnFromAttribute(pinfo.Name, attr, pinfo.CanWrite));
            }

            columns.Sort(delegate(OLVColumn x, OLVColumn y) {
                return x.DisplayIndex.CompareTo(y.DisplayIndex);
            });

            return columns;
        }

        private static OLVColumn MakeColumnFromAttribute(string aspectName, OLVColumnAttribute attr, bool editable) {
            string title = String.IsNullOrEmpty(attr.Title) ? aspectName : attr.Title;
            OLVColumn column = new OLVColumn(title, aspectName);
            column.AspectToStringFormat = attr.AspectToStringFormat;
            column.CheckBoxes = attr.CheckBoxes;
            column.DisplayIndex = attr.DisplayIndex;
            column.FillsFreeSpace = attr.FillsFreeSpace;
            if (attr.FreeSpaceProportion.HasValue)
                column.FreeSpaceProportion = attr.FreeSpaceProportion.Value;
            column.GroupWithItemCountFormat = attr.GroupWithItemCountFormat;
            column.GroupWithItemCountSingularFormat = attr.GroupWithItemCountSingularFormat;
            column.Hyperlink = attr.Hyperlink;
            column.ImageAspectName = attr.ImageAspectName;
            if (attr.IsEditableSet)
                column.IsEditable = attr.IsEditable;
            else
                column.IsEditable = editable;
            column.IsTileViewColumn = attr.IsTileViewColumn;
            column.IsVisible = attr.IsVisible;
            column.MaximumWidth = attr.MaximumWidth;
            column.MinimumWidth = attr.MinimumWidth;
            column.Name = String.IsNullOrEmpty(attr.Name) ? aspectName : attr.Name;
            column.Tag = attr.Tag;
            column.TextAlign = attr.TextAlign;
            column.ToolTipText = attr.ToolTipText;
            column.TriStateCheckBoxes = attr.TriStateCheckBoxes;
            column.UseInitialLetterForGroup = attr.UseInitialLetterForGroup;
            column.Width = attr.Width;
            if (attr.GroupCutoffs != null && attr.GroupDescriptions != null)
                column.MakeGroupies(attr.GroupCutoffs, attr.GroupDescriptions);
            return column;
        }

    }
}
