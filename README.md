# Object List View Project

A library of object list view classes.

## Getting Started

Clone the project to its respective relative path.

### Prerequisites


```
git clone git@bitbucket.org:davidhary/vs.objectlistview.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Controls\ObjectListView
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **Phillip Piper** -- [Object List View](objectlistview.sourceforge.net/cs)

## License

This repository is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.objectlistview/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)
* [Phillip Piper](objectlistview.sourceforge.net/cs) -- Object list view

